% ==============================================================================
% PG - Vitor Araujo Chane
% Capítulo 2 - Referencial Teórico
% ==============================================================================


\chapter{Referencial Teórico}
\label{ch-referencial}

Este capítulo apresenta a base de conceitos que fundamentaram e tornaram possíveis o desenvolvimento do módulo \emph{\imprimirtitulo}, e está organizado da seguinte forma: a Seção~\ref{sec-referencial-engsoftware} apresenta os conceitos da Engenharia de Software, divididos em duas subseções; a Seção~\ref{sec-referencial-engweb} apresenta os conceitos da Engenharia Web; a Seção~\ref{sec-referencial-tecnologias} apresenta uma breve descrição das principais tecnologias e ferramentas utilizadas para a produção deste trabalho; por fim, a Seção~\ref{sec-referencial-frameweb} apresenta o método FrameWeb.


%%% Início de seção. %%%
\section{Engenharia de Software}
\label{sec-referencial-engsoftware}

O desenvolvimento de software é uma atividade muito importante nos dias de hoje. Todos nós, desenvolvedores, queremos construir softwares que atendam às necessidades dos usuários, operando perfeitamente durante um longo período de tempo, com um alto grau de manutenibilidade e que seja intuitivo ao uso. Porém, frequentemente, nos deparamos com softwares ruins, propensos a muitos erros, usuários insatisfeitos e uma dificuldade imensa na manutenção e utilização dos mesmos.

Para evitar a falta de qualidade dos softwares e aumentar a produtividade no processo de desenvolvimento dos mesmos, surgiu a Engenharia de Software. De acordo com \citeonline{falbo2014notasaula}, a Engenharia de Software é a área da Ciência da Computação voltada à especificação, desenvolvimento e manutenção de sistemas de software, com aplicação de tecnologias e práticas de gerência de projetos e outras disciplinas, visando organização, produtividade e qualidade no processo de software. A Engenharia de Software trata de aspectos relacionados ao estabelecimento de processos, métodos, técnicas, ferramentas e ambientes de suporte ao desenvolvimento de software~\cite{falbo2014notasaula}.

A Engenharia de Software determina que se tenha fases de processo e desenvolvimento do produto, nas quais se deve dividir o problema em partes menores, e que são determinadas por algum modelo de processo estabelecido, como por exemplos os modelos sequenciais, evolutivos, incrementais, etc. Não existe um modelo de processo ideal, sendo que cada projeto deve ter uma análise prévia para se estabelecer o modelo que melhor pode atendê-lo, mas pode-se observar que todos esses modelos possuem algumas fases básicas, sendo elas: Análise e Especificação de Requisitos, Projeto e Implementação, Testes e Entrega~\cite{sommer2011engsoft}.

Esta seção sobre Engenharia de Software é uma introdução e possui as seguintes subseções referentes ao processo de desenvolvimento de software: A Seção~\ref{sec-referencial-engsoftware-analise} apresenta o processo de Análise e Especificação de Requisitos; e a Seção~\ref{sec-referencial-engsoftware-projeto} apresenta o conceito de Projeto e Implementação de Sistemas.


%%% Início de sub-seção. %%%
\subsection{Análise e Especificação de Requisitos}
\label{sec-referencial-engsoftware-analise}

Desenvolver um software, por mais simples que seja, não é uma tarefa fácil. Essa atividade requer calma e paciência para se lidar com pessoas que estão envolvidas no desenvolvimento, desde o cliente que solicita as funcionalidades que o sistema deve prover, até a equipe de desenvolvimento, que nem sempre concorda com a maneira que certa funcionalidade está sendo desenvolvida. É de suma importância estabelecer uma base de concordância entre o cliente e o fornecedor, para que as funcionalidades do software fiquem bem claras, e isso requer um processo bem definido de levantamento e análise de requisitos.

Os requisitos nada mais são do que as necessidades explicitadas pelos clientes e as restrições que o sistema deve ter para satisfazê-las. Em outras palavras, os requisitos definem o que o sistema deve fazer e as circunstâncias sob as quais deve operar \cite{sommer2011engsoft}.

Requisitos podem ser divididos em três grupos: requisitos funcionais, que são as funcionalidades que o sistema deve prover; requisitos não-funcionais, que são restrições sobre os requisitos funcionais, que o sistema deve ter para funcionar corretamente; e os requisitos de domínio (também conhecidos como regras de negócio) que são provenientes do domínio de aplicação do sistema e refletem características e restrições desse domínio~\cite{falbo2014notasaula}.

O ciclo de vida do processo de Engenharia de Requisitos, de acordo com a Figura~\ref{fig-referencial-engrequisitos-analise}, inicia-se com a fase de levantamento de requisitos, que combina elementos de solução de problemas, elaboração, negociação e especificação~\cite{pressman2016engsoft}. Se deve compreender o domínio da aplicação, os processos de negócio a serem apoiados pelo sistema, os problemas que se pretende resolver e as necessidades e restrições dos envolvidos~\cite{falbo2014notasaula}. No fim, espera-se ter o problema identificado, propostas de soluções para o problema e um conjunto preliminar de requisitos da solução documentados em um documento de requisitos.

\begin{figure}[h]
    \centering
    \includegraphics[width=1\textwidth]{figuras/proc-eng-req.jpg}
    \caption{O Processo da Engenharia de Requisitos~\cite{kotsom1998engreq}.}
    \label{fig-referencial-engrequisitos-analise}
\end{figure}
\pagebreak

Após os requisitos serem levantados, é preciso garantir que o sistema tenha condições de tratá-los a partir de uma série de pontos de vista. A modelagem de sistemas possibilita o estudo, de uma forma abstrata, dos pontos fortes e fracos, fazendo com que previamente se levante a necessidade de modificações ou correções para o funcionamento correto do sistema. De acordo com \citeonline{falbo2014notasaula}, a fase de análise de requisitos é, em ultima instância, uma fase de construção de modelos.

Para cada situação, diferentes tipos de modelos são necessários e, de acordo com \citeonline{pressman2016engsoft}, a modelagem de requisitos resulta em um ou mais dos seguintes tipos de modelos:

\begin{itemize}
   \item Modelos baseados em cenários de requisitos, como os modelos de casos de uso, nos quais o objetivo é representar um requisito de software sob a ótica de um usuário (ator);
   \item Modelos orientados a classes que representam classes orientadas a objetos (atributos e operações) e a maneira como as classes colaboram para atender aos requisitos do sistema;
   \item Modelos comportamentais e baseados em padrões que representam como o software se comporta em consequência de “eventos” externos;
   \item Modelos de dados que representam o domínio de informações para o problema;
   \item Modelos orientados a fluxos que representam os elementos funcionais do sistema e como eles transformam os dados à medida que se movem pelo sistema.
\end{itemize}

Por fim, os modelos de requisitos e a especificação de requisitos dão ao projetista de software informações que podem ser transformadas em projetos de arquitetura, de interface e de componentes~\cite{pressman2016engsoft}.


%%% Início de sub-seção. %%%
\subsection{Projeto e Implementação de Sistemas}
\label{sec-referencial-engsoftware-projeto}

Com os requisitos levantados, analisados e modelados, começamos a pensar na melhor maneira de implementar o sistema. Para isso, é necessário identificar os componentes de software e seus relacionamentos, respeitando os requisitos especificados, e utilizar as tecnologias mais adequadas. A partir disso, são gerados modelos de projeto que servirão de guia para o que será codificado e implementado.

É importante que esses guias proporcionem um quadro completo do software, e que sejam de fácil entendimento para as pessoas que irão codificá-lo, testá-lo e mantê-lo~\cite{falbo2014notasaula}.

De acordo com \citeonline{falbo2014notasaula}, independente do ciclo de vida e dos paradigmas adotados, a partir dos modelos de requisitos, o projeto deve produzir:

\begin{itemize}
   \item \textbf{Projeto de Arquitetura de Software}: definição da relação entre os elementos estruturais, assim como o estilo da arquitetura e os padrões que o projeto irá atender;
   
   \item \textbf{Projeto de Dados}: transforma os modelos de classes em realizações de classes de projeto e nas estruturas de dados dos requisitos necessárias para implementar o software, que servirão de base para a realização do projeto~\cite{pressman2016engsoft};
   
   \item \textbf{Projeto de Interfaces}: descreve como deverá ser a comunicação do software com outros sistemas (interfaces externas), com os usuários que irão o utlizar (interface com o usuário), e com ele mesmo (interfaces internas);
   
   \item \textbf{Projeto Detalhado}: tem por objetivo refinar e detalhar a descrição dos componentes estruturais da arquitetura do software.
\end{itemize}

Ao final da fase de projeto, espera-se que tenhamos um padrão arquitetônico bem definido, assim como os componentes, que devem estar refinados e detalhados para que possam posteriormente ser codificados e testados. Essas camadas são mostradas com mais detalhes na Seção~\ref{sec-referencial-frameweb}.  

Na etapa de implementação, o projeto é codificado de acordo com a descrição do projeto a partir de uma linguagem de programação. Eventualmente é gerado o primeiro executável onde o projeto pode ser compilado e testado.


%%% Início de seção. %%%
\section{Engenharia Web}
\label{sec-referencial-engweb}
    
Graças aos avanços das tecnologias Web, da Internet e da própria \textit{World Wide Web}, hoje vivemos em um mundo estritamente conectado, onde coisas que há poucos anos pareciam inimagináveis, atualmente são comuns aos nossos olhos. Novas aplicações baseadas na Web são lançadas todos os dias, em diferentes setores da economia, com o intuito de criar respostas para as necessidades presentes no mercado e na sociedade contemporânea. 
O que se viu por muitos anos era que o desenvolvimento de sistema baseado na Web careciam de rigor, abordagens sistemáticas e garantia de qualidade~\cite{san2000webeng}.

À medida que a complexidade e a sofisticação das aplicações baseadas na Web crescem, cresce também uma preocupação sobre a maneira que estão sendo criadas e se torna necessário o uso de métodos da Engenharia de Software aplicados ao desenvolvimento Web, para que as aplicações mantenham um alto padrão de qualidade e integridade ao longo do tempo.

A Engenharia Web nasceu como sub-área da Engenharia de Software, sendo o estabelecimento e uso de conhecimentos científicos, de engenharia, de gestão e abordagens disciplinadas e sistemáticas para o desenvolvimento, implantação e manutenção de sistemas e aplicações de alta qualidade baseadas na Web \cite{san2000webeng}. A Engenharia Web é multidisciplinar e abrange diversos princípios de diversas áreas, como interface humano-computador, gerenciamento de projetos, banco de dados, segurança da informação, comunicação social, redes, etc.

Mesmo com as diferenças de softwares convencionais e softwares de aplicações Web, se percebe uma similaridade no modo com que são divididas as fases de processo e desenvolvimento dos softwares. \citeonline{san2000webeng} diz que, como nos modelos de processo da Engenharia de Software, há algumas similaridades para a Engenharia Web, a saber: necessidade de métodos, levantamento de requisitos, implementação, testes e manutenção das partes que lidam com programação e funcionalidades.
O que se percebe é que o processo de desenvolvimento de um software Web é muito semelhante na parte de processos se comparado a um software convencional.


%%% Início de seção. %%%
\section{Tecnologias e Ferramentas}
\label{sec-referencial-tecnologias}

Para o desenvolvimento do \emph{\imprimirtitulo}, foi utilizada a \href{https://help.eclipse.org/latest/index.jsp}{IDE Eclipse}, que é um ambiente de desenvolvimento \textit{open source} para várias linguagens de programação como Java, C/C++, PHP, Python, etc. por meio de \textit{plugins} que podem ser obtidos e integrados ao ambiente. O principal trunfo utilizando o Eclipse é a redução da complexidade de desenvolvimento, implantação e gerenciamento das aplicações Web a partir de seus componentes de infraestrutura prontos para o uso.

Junto ao Eclipse, a linguagem de programação escolhida para escrever as classes do módulo do projeto foi \href{https://dev.java/learn/}{Java\texttrademark\ 13}, junto da plataforma \href{https://javaee.github.io/glassfish/documentation}{Java Enterprise Edition (Java EE) 8}. Java é uma linguagem de programação orientada a objetos pertencente à Oracle e distribuída com um vasto conjunto de bibliotecas e APIs que podem ser usadas nos projetos. Java EE 8 é uma plataforma de programação que oferece suporte a diversos \textit{frameworks} e APIs. Desta plataforma, utilizamos para o desenvolvimento do módulo as APIs JSF, CDI, JPA e Facelets.

O \href{https://javaee.github.io/javaserverfaces-spec/}{JSF (\textit{JavaServer Faces})} é uma API que permite criar aplicações Java para Web utilizando componentes visuais pré-prontos que facilitam significativamente a tarefa de escrever e manter as aplicações que são executadas em um servidor de aplicações Java e renderizar as interfaces de usuário de volta a um cliente.

O \href{https://docs.oracle.com/javaee/7/tutorial/cdi-basic.htm}{CDI (\textit{Context and Dependency Injection})} é parte integrante do Java EE e fornece uma arquitetura que permite aos componentes da plataforma, como os servlets e EJB's (\textit{Enterprise Java Beans}), existirem dentro do ciclo de vida de uma aplicação com escopos bem definidos, associando-os a contextos e oferecendo uma série de serviços de infraestrutura à aplicação. O CDI também oferece um mecanismo de injeção de dependências entre classes de uma aplicação Java EE, que serão associadas a contextos e possuem um ciclo de vida gerênciado por um \textit{container}. Com isso, o CDI oferece funcionalidades como decoradores, interceptadores e qualificadores, que flexibilizam o desenvolvimento de uma aplicação Java EE.

O \href{https://docs.oracle.com/javaee/6/tutorial/doc/bnbpz.html}{JPA (\textit{Java Persistence API)}} é um \textit{framework} para persistência de dados que possui uma API de mapeamento ORM (\textit{Object/Relational Mapping} ou Mapeamento Objeto/Relacional). 
Entre as vantagens, podemos citar \cite{nascimento2015}: 

\begin{itemize}
    \item Códigos de acesso a banco de dados com queries SQL são custosos de se desenvolver. JPA melhora a produtividade, eliminando o trabalho e deixando o desenvolvedor se concentrar na lógica de negócio;
    
    \item A manutenabilidade de sistemas desenvolvidos com ORM é excelente, pois o mecanismo faz com que menos linhas de código sejam necessárias;
    
    \item Com JPA, você pode desenvolver um sistema usando um banco de dados e colocá-lo em produção usando diversos outros banco de dados, sem precisar alterar códigos-fontes para adequar sintaxe de queries que só funcionam em SGBDs de determinados fornecedores.
\end{itemize}

Para estilização, foi utilizada uma biblioteca de interfaces pré-prontas chamada \href{https://www.primefaces.org/showcase/index.xhtml?jfwid=ecddc}{PrimeFaces}. O seu objetivo é otimizar o tempo gasto com desenvolvimento de componentes e estilizações como botões, tabelas, páginas responsívas e etc. entregando uma interface robusta e amigável para o usuário final.

Para simplificar a utilização de \textit{frameworks} como JPA, CDI e JSF, foi utilizada a ferramenta  \href{https://gitlab.labes.inf.ufes.br/labes/jbutler}{JButler}, desenvolvida na Universidade Federal do Espírito Santo (UFES) que fornece ao usuário facilidades à funcionalidades CRUD (\textit{create, retrieve, update, delete}), implementando as operações básicas entre a aplicação e o banco de dados. 

Para nos fornecer as implementações de APIs e para hospedar as aplicações Web, usamos o \href{https://www.wildfly.org/}{WildFly}, que é um servidor de aplicação \textit{open source} baseado na plataforma Java EE e implementado inteiramente na linguagem Java.

Para o banco de dados foi utilizado o \href{https://dev.mysql.com/doc/refman/8.0/en/}{MySQL Server 8}, que é um sistema de gerenciamente de banco de dados relacionais que utiliza a linguagem SQL como interface. Nele, teremos guardados todos os usuários do sistema e principalmente os papéis atribuídos a cada um. Os papeis são os pontos críticos deste módulo, pois são eles que possibilitarão a cada usuário acessar áreas restritas dentro do sistema.


%%% Início de seção. %%%
\section{O Método FrameWeb}
\label{sec-referencial-frameweb}

Poupar tempo com tarefas corriqueiras é algo almejado por programadores desde o princípio da programação. Com isso em mente, foram desenvolvidos padrões arquiteturais de software e \textit{frameworks} capazes de auxiliar no reúso de código e no aumento da produtividade.

O FrameWeb é um método de projeto de WISs (\textit{Web-based Information Systems}) baseado em \textit{frameworks}~\cite{souza:Nemo-fw}. Nele, é definida uma arquitetura baseada no padrão arquitetônico Camada de Serviço (\textit{Service layer})~\cite{fowler:book02} utilizando esses \textit{frameworks}. A organização das camadas desta arquitetura é mostrada na Figura~\ref{fw-arquitetura-padrao}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{figuras/fw-arq.jpg}
	\caption{Arquitetura padrão proposta pelo FrameWeb, baseado no \textit{Service Layer} ~\cite{campos-souza:webmedia17}}
	\label{fw-arquitetura-padrao}
\end{figure}

De acordo com \citeonline{souza:Nemo-fw}, a arquitetura é composta das seguintes camadas:

\begin{itemize}
   \item \textbf{Camada de Apresentação} ou \textit{Presentation Tier}: responsável pela interação com o usuário, é dividida em dois pacotes: \textbf{Visão} (\textit{View}), responsável por conter os artefatos de interface com o usuário, como páginas, scripts e leiautes; e \textbf{Controle} (\textit{Control}), contendo as classes de controle que manipulam as solicitações feitas pelo pacote de Visão, e também responsável pela comunicação com o pacote de Aplicação (\textit{Application}), pertencente à camada de negócio;
   
   \item \textbf{Camada de Negócio} ou \textit{Business Tier}: responsável pela lógica de negócio, assim como a camada de apresentação, é dividida em dois pacotes: pacote de \textbf{Aplicação} (\textit{Application}), que contêm as classes responsáveis pelas funcionalidades do sistema; e o pacote de \textbf{Domínio} (\textit{Domain}), que contém as classes que representam o domínio do problema, identificadas na fase de análise de requisitos;
   
   \item \textbf{Camada de Acesso de Dados} ou \textit{Data Access Tier}: com apenas o pacote de \textbf{Persistência} (\textit{Persistence}), é responsável pela persistência de dados, como por exemplo, compartilhamento de arquivos e acesso a bancos de dados.
\end{itemize}

Com base nesta arquitetura, o FrameWeb define quatro tipos de diagramas baseados no Diagrama de Classes da UML, que demonstram especificamente os componentes de cada camada. São eles~\cite{campos-souza:webmedia17}:

\begin{itemize}
   \item \textbf{Modelo de Entidades} (\textit{Entity Model}): representa os objetos de domínio do problema e sua persistência de dados;
   
   \item \textbf{Modelo de Persistencia} (\textit{Persistence Model}): representa a camada de acesso a dados, seguindo o padrão de projeto \textit{Data Access Object} (DAO)~\cite{alur-et-al:book03}, responsáveis pela persistência das classes de domínio;

   \item \textbf{Modelo de Navegação} (\textit{Navigation Model}): representam o conjunto de componentes que formam a camada responsável pela interação com o usuário, como as páginas, scripts e os controladores;
   
   \item \textbf{Modelo de Aplicação} (\textit{Application Model}): representa as classes de serviço responsáveis pela implementação das funcionalidades do WIS, e suas dependências.
\end{itemize}