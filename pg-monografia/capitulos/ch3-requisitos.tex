% ==============================================================================
% PG - Vitor Araujo Chane
% Capítulo 3 - Análise e Especificação de Requisitos
% ==============================================================================


\chapter{Análise e Especificação de Requisitos}
\label{ch-requisitos}

Este capítulo tem como objetivo descrever o escopo do \emph{\imprimirtitulo} e apresentar o que foi levantado na fase de Análise e Especificação de Requisitos, como por exemplo: estórias de usuário, modelos de caso de uso e diagramas de classes da UML.


\section{Descrição do Escopo}
\label{sec-requisitos-escopo}

Possibilitar o gerenciamento dos papeis que cada usuário terá dentro do Marvin, garantindo que cada um possua acesso a funcionalidades específicas de acordo com seu papel, é algo indispensável. O módulo \emph{\imprimirtitulo} tem por objetivo suprir esta necessidade.

A partir de papeis básicos já definidos no Marvin --- a saber, Professor, Servidor Técnico-Administrativo, Estudante ---, este novo módulo englobará vários outros papeis associados a unidades organizacionais específicas. Por exemplo: um professor poderá ter o papel de Chefe do Departamento. Um estudante poderá ter o papel de Representante Discente, mas nunca poderá ter o papel Chefe do Departamento. Analogamente, um usuário com papel Professor nunca poderá ter o papel Representante Discente, por ser um papel específico de um Estudante.

Apenas o administrador do sistema e usuários com cargos hierárquicos de topo como secretários e chefes/coordenadores podem conceder outros papéis a um usuário do sistema, de modo que seja respeitado o que cada usuário pode acessar. No final, espera-se que estes acessos sejam concedidos de maneira simples e evitando qualquer tipo de erros.

Podemos descrever o uso do módulo \emph{\imprimirtitulo} por meio dos passos a seguir.

A princípio, o administrador do sistema (definido no Marvin pelo papel \textit{SysAdmin}) será responsável por fazer cadastros gerais de todas os objetos que compõem o sistema. Primeiramente, são cadastradas as unidades administrativas, como centros de ensino (ex.: Centro Tecnológico), departamentos (ex.: Departamento de Informática), cursos de graduação (ex.: curso de Ciência da Computação), programas de pós-graduação (PPG) (ex.: Programa de Pós-Graduação em Informática) e cursos de pós-graduação (ex.: Mestrado em Informática). Departamentos, PPGs e cursos de graduação são vinculados ao centro, já os cursos de pós-graduação são vinculados a um PPG.
    
Após a criação das organizações administrativas, o administrador começará a popular esses órgãos com acadêmicos. Para acessar o sistema, esses acadêmicos terão que ser cadastrados informando: nome, e-mail, CPF, data de nascimento e senha. Também serão associados a um ou mais papeis dentre os papéis existentes.

Criado o departamento, podem ser associados a ele os seguintes papeis específicos: chefe e subchefe (exclusivo para professores(as)), secretários(as) (servidores(as) técnico-administrativos(as)), representantes discentes (estudantes) e professores(as) ou servidores(as) lotados(as) no departamento. Em todos os casos, o sistema deve registrar a data de início da relação do acadêmico com o departamento, bem como a data final para que seja mantido o histórico.

O curso de graduação deverá ter um coordenador(a) e um subcoordenador(a) (professores(as)), secretários(as) (servidores(as) técnico-administrativos(as)), representantes discentes (estudantes), um(a) coordenador(a) de estágio (professores(as)) e professores(as) ou servidores(as) que compõem o colegiado do curso. Em todos esses cadastros, o sistema deve registrar a data de início da relação do acadêmico com o curso de graduação, bem como a data final para que seja mantido o histórico. Também deve ser possível que estudantes sejam matriculados nos cursos em questão, informadas a matricula e a data de início do estudante, sendo que também deve ser informado a data de finalização do vínculo em casos de desistência, abandono ou conclusão do curso.

Na pós-graduação ocorre um processo similar, com a diferença de que existe um programa de pós-graduação (PPG) entre o centro de ensino e o curso. Como um curso de graduação, um PPG poderá também ter coordenadores(as) e subcoordenadores(as), secretários(as), representantes discentes e professores(as) membros de seu colegiado. Além disso, há a possibilidade de professores(as) serem membros do PPG mas não do colegiado. Por fim, cursos de pós-graduação podem ter estudantes matriculados(as) como no caso de cursos de graduação.

Após os acadêmicos serem cadastrados pelo administrador do sistema, estes (dependendo de seu papel e cargo) tomarão posse da responsabilidade de conceder e revogar acesso a outros acadêmicos dentro dos subsistemas informados. Por exemplo, um chefe de departamento pode indicar que sua chefia está terminando e um(a) colega está assumindo a chefia em seu lugar a partir daquele momento.

\section{Estórias de Usuário}
\label{sec-requisitos-estorias}

Uma estória de usuário \textit{(User Story)} é uma explicação informal e geral sobre um recurso de software escrita a partir da perspectiva do usuário final. Considerando como principais \textit{stakeholders}, professores, estudantes, técnicos administrativos e visitantes, foram identificados vários requisitos de usuário e regras de negócio que são apresentadas no Documento de Requisitos anexo no apêndice deste documento. 

Para título de exemplo, como a maioria dos casos de uso deste trabalho possuem certa semelhança por se tratarem de vários CRUD's e funcionalidades relacionadas a indicação de acadêmicos a um cargo, são demonstradas na Tabela~\ref{tbl-requisitos-uss} as estórias de usuário para dois exemplos: um registro de um chefe de departamento pelo administrador do sistema e uma indicação de um acadêmico a um cargo de chefia de um departamento por um outro acadêmico apto a fazer isso.

% Define uma macro para as linhas de user stories.
% Usar \userstory{label}{dependências}{prioridade}{descrição}{critérios de qualidade (usando \item)}.
\newcommand{\userstory}[5]{
	\\\hline
	\cellcolor{lightgray}\textbf{Descrição:} & 
	\multicolumn{5}{|p{12cm}|}{#4}\\\hline
	\cellcolor{lightgray}\parbox{2.5cm}{\raggedleft \textbf{Critérios de\\Aceitação:}} & 
	\multicolumn{5}{|p{12cm}|}{
		\parbox{12cm}{
			\begin{enumerate}[leftmargin=15mm,label=-- CA\arabic*:]\itemsep-2mm
				#5
			\end{enumerate}
		}		
	}\\\hline
	\multicolumn{6}{c}{}
}

\begin{longtable}{|r|p{1.3cm}|r|p{4cm}|r|p{1.3cm}|}
\caption{Exemplos de estórias de usuário.}
\label{tbl-requisitos-uss}

\userstory{us-admin-cadastrar-chefia-departamento}{\ref{us-admin-cadastrar-academicos}, \ref{us-atribuir-papeis-academicos}, \ref{us-cadastrar-departamentos}}{Alta}
{Cadastro/CRUD: Como administrador(a) do sistema, eu quero cadastrar professores(as) em cargos de chefia do departamento, para que eles(as) possam ter acesso a funcionalidades específicas deste papel.}
{
    \item O(A) acadêmico(a) deve estar válido no sistema, deve ter o papel \textsl{Professor} e deve estar lotado no departamento em questão;
    \item Para cadastrar, devem ser informados: acadêmico(a), departamento, cargo, data de início e data de fim da atuação (esta última, quando nula, indica que a atuação está ativa);
    \item A consulta pode ser feita por: nome do(a) acadêmico(a), departamento, cargo ou período de atuação;
	\item Deve ser requerida confirmação para exclusões.
}

\userstory{us-registrar-chefia-departamento}{\ref{us-admin-cadastrar-academicos}, \ref{us-atribuir-papeis-academicos}, \ref{us-cadastrar-departamentos}, \ref{us-admin-cadastrar-chefia-departamento}, \ref{us-admin-cadastrar-secretarios-departamento}}{Média}
{Como chefe/sub-chefe ou secretário(a) de um departamento, eu quero registrar substituição de chefia do departamento, para que os(as) novos(as) chefes possam ter acesso a funcionalidades específicas deste papel.}
{
	\item Podem ser indicados(as) apenas acadêmicos(as) lotados(as) no departamento em questão e com papel \textsl{Professor};
	\item Inicialmente o sistema deve listar os(as) chefes e sub-chefes atuais e aqueles(as) que já foram no passado, com respectivas datas de atuação;
	\item Pode-se pesquisar nesta lista por nome do(a) acadêmico(a), cargo ou período de atuação;
	\item Para terminar a atuação de um chefe ou sub-chefe, basta escolhê-lo(a) na lista e confirmar o encerramento, que é efetivado com data atual;
	\item Para indicar uma nova chefia, escolhe se a indicação é de chefe ou sub-chefe, pode-se pesquisar por nome, CPF ou e-mail do(a) acadêmico(a) e confirma a indicação, que é efetivada com a data atual, o que também encerra com data atual a atuação do(a) atual acadêmico(a) que ocupa o referido cargo (se algum).
}

\end{longtable}


\section{Modelos de Caso de Uso}
\label{sec-requisitos-casodeuso}

O modelo de casos de uso corresponde a uma tentativa de descrever a relação das funcionalidades do sistema com cada um de seus atores. Os atores identificados no contexto deste projeto estão descritos na Tabela~\ref{tbl-casos-de-uso-atores}.

% Tabela de atores.
\begin{longtable}{|p{3.5cm}|p{11.5cm}|}
	\caption{Descrição dos atores envolvidos nos casos de uso.}
	\label{tbl-casos-de-uso-atores} \\\hline 
	
	% Cabeçalho e repetição do mesmo em cada nova página. Manter como está.
	\rowcolor{lightgray}
	\textbf{Ator} & \textbf{Descrição} \\\hline		
	\endfirsthead
	\hline
	\rowcolor{lightgray}
	\textbf{Ator} & \textbf{Descrição} \\\hline		
	\endhead
	
	Administrador do Sistema & Pessoa responsável pelo cadastro inicial dos usuários dentro do sistema. \\\hline
	
	Chefe de Departamento & Professor responsável por coordenar e supervisionar todas as atividades da competência do departamento. Deve conceder acesso aos outros usuários do departamento. \\\hline
	
	Subchefe de Departamento & Professor responsável por coordenar e supervisionar todas as atividades da competência do departamento, na ausência do Chefe de Departamento. Deve conceder acesso aos outros usuários do departamento. \\\hline
	
	Secretário(a) do Departamento & Encarregado da execução de todos os serviços administrativos do departamento, incluindo concessão de acesso aos usuários. \\\hline
	
	Coordenador(a) de Curso de Graduação & Professor responsável por representar oficialmente o colegiado do curso de graduação. Deve poder conceder acesso aos usuários relacionados ao curso de graduação. \\\hline
	
	Subcoordenador(a) de Curso de Graduação & Professor responsável por representar oficialmente o colegiado do curso de graduação na ausência do coordenador. Deve poder conceder acesso aos usuários relacionados ao curso de graduação. \\\hline
	
	Secretario(a) do Colegiado da Graduação & Encarregado da execução de todos os serviços administrativos do curso de graduação, incluindo concessão de acesso aos usuários. \\\hline
	
	Coordenador(a) PPG & Professor responsável por representar oficialmente o colegiado de um programa de pós-graduação (PPG). Deve poder conceder acesso aos acadêmicos participantes de um PPG. \\\hline
	
	Subcoordenador(a) PPG & Professor responsável por representar oficialmente o colegiado PPG na ausência do coordenador. Deve poder conceder acesso aos acadêmicos participantes de um PPG. \\\hline
	
	Secretario(a) PPG & Encarregado da execução de todos os serviços administrativos do PPG, incluindo concessão de acesso aos acadêmicos. \\\hline
\end{longtable}

Todos os atores descritos na Tabela~\ref{tbl-casos-de-uso-atores} são acadêmicos que devem, de alguma forma, lidar com os cadastros mencionados na Seção~\ref{sec-requisitos-escopo}. O Administrador do Sistema é um tipo de superusuário cujos privilégios são maiores do que os outros atores apresentados, podendo ser capaz de cadastrar todos os elementos mencionados na Seção~\ref{sec-requisitos-escopo}, a saber: cadastro básico de acadêmicos, cadastro das unidades administrativas e cadastro dos papéis específicos de cada unidade administrativa. Os casos de uso referentes ao Administrador do Sistema são apresentados em seu diagrama de caso de uso na Figura~\ref{fig-casos-de-uso-admin}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/Diagrama_caso_de_uso_admin.png}
	\caption{Diagrama de Casos de Uso do administrador do sistema.}
	\label{fig-casos-de-uso-admin}
\end{figure}

Além do administrador, os próprios acadêmicos com papeis específicos podem também indicar outros acadêmicos para ocupar papeis dentro do contexto de um departamento, curso de graduação ou programa de pós-graduação. O módulo foi, então, dividido em três subsistemas, descritos a seguir.


\subsection{Subsistema Departamento}
\label{sec-casos-de-uso-subsistema-departamento}

Após o administrador fazer os cadastros iniciais, teremos um departamento cadastrado com seus elementos como descrito na Seção~\ref{sec-requisitos-escopo}. Após esse cadastro inicial, os próximos cadastros relativos ao departamento ficam a cargos dos chefes do departamento e dos secretários(as) do departamento, como pode ser visto na Figura~\ref{fig-casos-de-uso-subsistema-departamento} e detalhados a seguir.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{figuras/Diagrama_caso_de_uso_departamento.png}
	\caption{Diagrama de Casos de Uso do subsistema Departamento.}
	\label{fig-casos-de-uso-subsistema-departamento}
\end{figure}

Nos casos de uso \textbf{Indicar chefe ou subchefe de departamento}, \textbf{Indicar secretário(a) do departamento} e \textbf{Indicar acadêmico lotado em departamento}, os atores (secretário(a), chefe e subchefe do departamento) poderão indicar uma nova relação entre um acadêmico e o departamento (ex.: novo chefe ou subchefe), ou uma conclusão desta relação quando o acadêmico finaliza seu período de atuação ou se aposenta. Para o caso dos chefes de departamento, é definido pelo regimento interno que o período de atuação é de apenas 2 anos, podendo ser renovado por igual período, como é citado no Documento de Requisitos deste módulo.

Para o caso de uso \textbf{Indicar representante discente em departamento} um estudante pode ser indicado em uma nova relação entre acadêmico e departamento pelos atores, para exercer o cargo de representante discente, mantendo-se esta relação até a conclusão da mesma, seja por tempo ou em caso de desistência/conclusão do curso. Ao final, todas as datas de indicações e conclusões devem ser armazenadas no histórico.


\subsection{Subsistema Graduação}
\label{sec-casos-de-uso-subsistema-graduacao}

Com o cadastro dos cursos de graduação e seus elementos vinculados pelo administrador do sistema, como descrito na Seção~\ref{sec-requisitos-escopo}, os próximos cadastros relativos a um curso de graduação ficarão a cargo dos coordenadores(as) e dos secretários(as), como é mostrado na Figura~\ref{fig-casos-de-uso-subsistema-graduacao} e detalhado a seguir.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{figuras/Diagrama_caso_de_uso_graduacao.png}
	\caption{Diagrama de Casos de Uso do subsistema Graduação.}
	\label{fig-casos-de-uso-subsistema-graduacao}
\end{figure}

Nos casos de uso \textbf{Indicar coordenador(a)ou subcoordenador(a) de curso de graduação}, \textbf{Indicar secretário(a) de curso de graduação}, \textbf{Indicar coordenador(a) de estágio de curso de graduação} e \textbf{Indicar  membro(a) de colegiado de curso de graduação}, os atores (secretário(a), coordenadores(as) e subcoordenadores(as)) poderão indicar uma nova relação entre um acadêmico e o curso de graduação (ex.: novo secretário(a)), ou uma conclusão desta relação quando o acadêmico finaliza seu período de atuação ou se aposenta. Igualmente ao caso dos chefes de departamento (Subseção~\ref{sec-casos-de-uso-subsistema-departamento}), é definido pelo regimento interno que o período de atuação dos coordenadores(as) do curso de graduação e de estágio é de apenas 2 anos, podendo ser renovado por igual período.

Para os casos de uso \textbf{Indicar representante discente de curso de graduação} e \textbf{Matricular estudante em curso de graduação} um estudante pode ser indicado em uma nova relação entre acadêmico e um curso de graduação pelos atores, para exercer o cargo de representante discente ou para ser matriculado em um dos cursos de graduação disponíveis. Esta relação é mantida até a conclusão da mesma, seja por tempo ou em caso de desistência/conclusão do curso. Ao final, todas as datas de indicações e conclusões devem ser armazenadas no histórico.


\subsection{Subsistema PPG}
\label{sec-casos-de-uso-subsistema-ppg}

Por fim, analogamente como acontece na Subseção~\ref{sec-casos-de-uso-subsistema-graduacao}, os próximos cadastros relativos a um programa de pós-graduação e dos cursos de pós-graduação ficarão a cargo dos coordenadores(as) e dos secretários(as), como é mostrado na Figura~\ref{fig-casos-de-uso-subsistema-ppg} e detalhado a seguir.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{figuras/Diagrama_caso_de_uso_ppg.png}
	\caption{Diagrama de Casos de Uso do subsistema PPG.}
	\label{fig-casos-de-uso-subsistema-ppg}
\end{figure}

Nos casos de uso \textbf{Indicar coordenador(a)ou subcoordenador(a) de PPG}, \textbf{Indicar secretário(a) de PPG}, \textbf{Indicar  membro(a) de PPG} e \textbf{Indicar  membro(a) de colegiado de PPG}, os atores (secretário(a), coordenadores(as) e subcoordenadores(as)) poderão indicar uma nova relação ou uma conclusão de relação entre um acadêmico e um PPG, ou um acadêmico e um curso de pós-graduação (ex.: novo membro(a) de um PPG). Os coordenadores(as) de um PPG também seguem a regra de que seus cargos devem ter um período de duração de apenas 2 anos, podendo ser renovado por igual período.

Para os casos de uso \textbf{Indicar representante discente de PPG} e \textbf{Matricular estudante em curso de pós-graduação} um estudante pode ser indicado em uma nova relação entre acadêmico e um curso de pós-graduação, ou entre um acadêmico e um PPG. Os responsáveis por essa indicação serão os atores, que definirão se o cargo a ser exercido é o de representante discente ou se é para ser uma matricula de um estudante em um dos cursos de pós-graduação disponíveis. Esta relação é mantida até a conclusão da mesma, seja por tempo ou em caso de desistência/conclusão do curso. Ao final, todas as datas de indicações e conclusões devem ser armazenadas no histórico.


\section{Diagrama de Classes}
\label{ch3-requisitos-classes}

As figuras~\ref{fig-modelo-estrutural-subsistema-departamento}, \ref{fig-modelo-estrutural-subsistema-graduacao} e~\ref{fig-modelo-estrutural-subsistema-ppg} apresentam os diagramas de classes dos subsistemas Departamento, Graduação e PPG respectivamente.

Nota-se que as classes mais importantes são as classes das organizações administrativas (\textbf{CentroEnsino}, \textbf{Departamento}, \textbf{PPG} e \textbf{CursoGrad}) e a classe \textbf{Acadêmico}, pois são as classes que possuem a maior quantidade de relações com as outras classes do sistema, inclusive entre elas mesmas.  

\subsection{Subsistema Departamento}
\label{sec-classes-subsistema-departamento}

Dentro do diagrama de classe do subsistema Departamento, representado na Figura~\ref{fig-modelo-estrutural-subsistema-departamento}, temos a classe \textbf{Departamento} como classe central. A partir dela, temos a associação com a classe \textbf{CentroEnsino}, que acaba servindo como ligação do subsistema departamento com os demais subsistemas (Graduação e PPG), tendo em vista que \textbf{CentroEnsino} tem um papel em todos eles. As demais associações da classe \textbf{Departamento} são relações com os papeis específicos que um
dado acadêmico pode adquirir, sendo esses papeis também relacionados com a classe \textbf{Academic} (ex.: um acadêmico pode participar de um cargo de chefia do departamento tendo associação com a classe \textbf{ChefiaDepartamento}). Podemos dizer então que as classes intermediárias (de papéis de um acadêmico específicos a uma unidade organizacional) são a ponte entre os acadêmicos e o departamento dentro do sistema. O nome de cada uma delas indica de maneira clara de qual papel específico se trata.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/fig-subsistema-departamento.png}
	\caption{Diagrama de classes do subsistema Departamento.}
	\label{fig-modelo-estrutural-subsistema-departamento}
\end{figure}


\subsection{Subsistema Graduação}
\label{sec-classes-subsistema-graduacao}

Analogamente ao diagrama de classe do subsistema Departamento (Figura~\ref{fig-modelo-estrutural-subsistema-departamento}), no diagrama de classes do subsistema Graduação, representado pela Figura~\ref{fig-modelo-estrutural-subsistema-graduacao}, temos as classes \textbf{CursoGrad} e \textbf{Academic} sendo as classes centrais do subsistema. A classe \textbf{CentroEnsino} também está presente e serve como ponte entre os subsistemas. As classes intermediárias (entre CursoGrad e Academic) são relações entre o curso de graduação, o papel que um acadêmico terá neste curso, e o próprio academico (ex.: um acadêmico pode participar de um cargo de coordenação sendo associado com a classe  \textbf{CoordenacaoCursoGrad} que é associada a classe \textbf{CursoGrad}).

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/fig-subsistema-graduacao.png}
	\caption{Diagrama de classes do subsistema Graduação.}
	\label{fig-modelo-estrutural-subsistema-graduacao}
\end{figure}


\subsection{Subsistema PPG}
\label{sec-classes-subsistema-ppg}

Por fim, dentro do subsistema PPG temos a mesma ideia. As classes \textbf{PPG} e \textbf{Academic} sendo as classes centrais do subsistema, com a classe \textbf{CentroEnsino} presente e servindo como ponte entre os subsistemas. Classes intermediárias sendo relações entre o PPG, o acadêmico e o papel que um acadêmico terá no PPG. A exceção é a classe \textbf{CursoPosGrad} que representa cursos de pós-graduação que, diferentemente dos de graduação, não são ligados diretamente ao CentroEnsino, mas sim ao PPG. A relação de matrícula de estudantes em cursos, portanto, acontece entre Academic e CursoPosGrad. O diagrama de classes do subsistema PPG é apresentado na Figura~\ref{fig-modelo-estrutural-subsistema-ppg}.


\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/fig-subsistema-ppg.png}
	\caption{Diagrama de classes do subsistema PPG.}
	\label{fig-modelo-estrutural-subsistema-ppg}
\end{figure}