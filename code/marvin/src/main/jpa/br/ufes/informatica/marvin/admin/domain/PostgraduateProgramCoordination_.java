package br.ufes.informatica.marvin.admin.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import br.ufes.informatica.marvin.core.domain.Academic;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2022-08-24T04:16:50.418-0300")
@StaticMetamodel(PostgraduateProgramCoordination.class)
public class PostgraduateProgramCoordination_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<PostgraduateProgramCoordination, Date> beginDate;
	public static volatile SingularAttribute<PostgraduateProgramCoordination, Date> endDate;
	public static volatile SingularAttribute<PostgraduateProgramCoordination, Boolean> isCoordinator;
	public static volatile SingularAttribute<PostgraduateProgramCoordination, Academic> academic;
	public static volatile SingularAttribute<PostgraduateProgramCoordination, PostgraduateProgram> postgraduateProgram;
}
