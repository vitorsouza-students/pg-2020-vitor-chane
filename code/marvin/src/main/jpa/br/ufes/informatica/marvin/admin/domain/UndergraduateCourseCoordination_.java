package br.ufes.informatica.marvin.admin.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import br.ufes.informatica.marvin.core.domain.Academic;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2022-08-17T23:32:23.777-0300")
@StaticMetamodel(UndergraduateCourseCoordination.class)
public class UndergraduateCourseCoordination_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<UndergraduateCourseCoordination, Date> beginDate;
	public static volatile SingularAttribute<UndergraduateCourseCoordination, Date> endDate;
	public static volatile SingularAttribute<UndergraduateCourseCoordination, Boolean> isCoordinator;
	public static volatile SingularAttribute<UndergraduateCourseCoordination, Academic> academic;
	public static volatile SingularAttribute<UndergraduateCourseCoordination, UndergraduateCourse> undergraduateCourse;
}
