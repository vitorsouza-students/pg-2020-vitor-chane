package br.ufes.informatica.marvin.admin.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import br.ufes.informatica.marvin.core.domain.Academic;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2022-08-17T23:32:23.780-0300")
@StaticMetamodel(UndergraduateCourseInternshipCoordination.class)
public class UndergraduateCourseInternshipCoordination_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<UndergraduateCourseInternshipCoordination, Date> beginDate;
	public static volatile SingularAttribute<UndergraduateCourseInternshipCoordination, Date> endDate;
	public static volatile SingularAttribute<UndergraduateCourseInternshipCoordination, Academic> academic;
	public static volatile SingularAttribute<UndergraduateCourseInternshipCoordination, UndergraduateCourse> undergraduateCourse;
}
