package br.ufes.informatica.marvin.admin.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import br.ufes.informatica.marvin.core.domain.Academic;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2022-08-17T23:32:23.778-0300")
@StaticMetamodel(UndergraduateCourseEnrollment.class)
public class UndergraduateCourseEnrollment_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<UndergraduateCourseEnrollment, Date> beginDate;
	public static volatile SingularAttribute<UndergraduateCourseEnrollment, Date> endDate;
	public static volatile SingularAttribute<UndergraduateCourseEnrollment, Integer> enrollment;
	public static volatile SingularAttribute<UndergraduateCourseEnrollment, Boolean> isConcluded;
	public static volatile SingularAttribute<UndergraduateCourseEnrollment, Boolean> wasDisconnected;
	public static volatile SingularAttribute<UndergraduateCourseEnrollment, String> disconnectionReason;
	public static volatile SingularAttribute<UndergraduateCourseEnrollment, Academic> academic;
	public static volatile SingularAttribute<UndergraduateCourseEnrollment, UndergraduateCourse> undergraduateCourse;
}
