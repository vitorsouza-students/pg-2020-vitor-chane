package br.ufes.informatica.marvin.admin.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2022-08-17T23:32:23.775-0300")
@StaticMetamodel(UndergraduateCourse.class)
public class UndergraduateCourse_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<UndergraduateCourse, String> name;
	public static volatile SingularAttribute<UndergraduateCourse, Integer> code;
	public static volatile SingularAttribute<UndergraduateCourse, String> shift;
	public static volatile SingularAttribute<UndergraduateCourse, Faculty> faculty;
}
