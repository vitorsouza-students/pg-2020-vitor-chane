package br.ufes.informatica.marvin.admin.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2022-08-30T00:05:18.475-0300")
@StaticMetamodel(PostgraduateCourse.class)
public class PostgraduateCourse_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<PostgraduateCourse, String> name;
	public static volatile SingularAttribute<PostgraduateCourse, Integer> code;
	public static volatile SingularAttribute<PostgraduateCourse, Integer> level;
	public static volatile SingularAttribute<PostgraduateCourse, String> shift;
	public static volatile SingularAttribute<PostgraduateCourse, PostgraduateProgram> postgraduateCourse;
}
