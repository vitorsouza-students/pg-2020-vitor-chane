package br.ufes.informatica.marvin.admin.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import br.ufes.informatica.marvin.core.domain.Academic;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2022-08-17T23:32:23.776-0300")
@StaticMetamodel(UndergraduateCourseCollegiateMembers.class)
public class UndergraduateCourseCollegiateMembers_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<UndergraduateCourseCollegiateMembers, Date> beginDate;
	public static volatile SingularAttribute<UndergraduateCourseCollegiateMembers, Date> endDate;
	public static volatile SingularAttribute<UndergraduateCourseCollegiateMembers, Academic> academic;
	public static volatile SingularAttribute<UndergraduateCourseCollegiateMembers, UndergraduateCourse> undergraduateCourse;
}
