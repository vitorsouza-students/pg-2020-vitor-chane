package br.ufes.informatica.marvin.admin.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2022-08-17T23:30:54.393-0300")
@StaticMetamodel(Department.class)
public class Department_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<Department, String> name;
	public static volatile SingularAttribute<Department, Faculty> faculty;
}
