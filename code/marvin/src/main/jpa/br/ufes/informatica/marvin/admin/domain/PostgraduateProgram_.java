package br.ufes.informatica.marvin.admin.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2022-08-24T04:16:50.418-0300")
@StaticMetamodel(PostgraduateProgram.class)
public class PostgraduateProgram_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<PostgraduateProgram, String> name;
	public static volatile SingularAttribute<PostgraduateProgram, Faculty> faculty;
}
