package br.ufes.informatica.marvin.admin.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import br.ufes.informatica.marvin.core.domain.Academic;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2022-08-24T04:16:50.418-0300")
@StaticMetamodel(PostgraduateCourseEnrollment.class)
public class PostgraduateCourseEnrollment_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<PostgraduateCourseEnrollment, Date> beginDate;
	public static volatile SingularAttribute<PostgraduateCourseEnrollment, Date> endDate;
	public static volatile SingularAttribute<PostgraduateCourseEnrollment, Integer> enrollment;
	public static volatile SingularAttribute<PostgraduateCourseEnrollment, Boolean> isConcluded;
	public static volatile SingularAttribute<PostgraduateCourseEnrollment, Boolean> wasDisconnected;
	public static volatile SingularAttribute<PostgraduateCourseEnrollment, String> disconnectionReason;
	public static volatile SingularAttribute<PostgraduateCourseEnrollment, Academic> academic;
	public static volatile SingularAttribute<PostgraduateCourseEnrollment, PostgraduateCourse> postgraduateCourse;
}
