package br.ufes.informatica.marvin.admin.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import br.ufes.informatica.marvin.core.domain.Academic;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2022-08-17T23:30:54.395-0300")
@StaticMetamodel(DepartmentParticipation.class)
public class DepartmentParticipation_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<DepartmentParticipation, Date> beginDate;
	public static volatile SingularAttribute<DepartmentParticipation, Date> endDate;
	public static volatile SingularAttribute<DepartmentParticipation, String> specificRole;
	public static volatile SingularAttribute<DepartmentParticipation, String> disconnectionReason;
	public static volatile SingularAttribute<DepartmentParticipation, Academic> academic;
	public static volatile SingularAttribute<DepartmentParticipation, Department> department;
}
