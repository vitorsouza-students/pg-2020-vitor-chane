package br.ufes.informatica.marvin.admin.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2022-08-17T23:30:54.399-0300")
@StaticMetamodel(Faculty.class)
public class Faculty_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<Faculty, String> name;
	public static volatile SetAttribute<Faculty, Department> departments;
}
