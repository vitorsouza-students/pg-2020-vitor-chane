package br.ufes.informatica.marvin.admin.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import br.ufes.informatica.marvin.core.domain.Academic;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2022-08-17T23:30:54.398-0300")
@StaticMetamodel(DepartmentStudentRepresentation.class)
public class DepartmentStudentRepresentation_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<DepartmentStudentRepresentation, Date> beginDate;
	public static volatile SingularAttribute<DepartmentStudentRepresentation, Date> endDate;
	public static volatile SingularAttribute<DepartmentStudentRepresentation, Academic> academic;
	public static volatile SingularAttribute<DepartmentStudentRepresentation, Department> department;
}
