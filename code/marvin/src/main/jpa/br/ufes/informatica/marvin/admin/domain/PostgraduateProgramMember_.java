package br.ufes.informatica.marvin.admin.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import br.ufes.informatica.marvin.core.domain.Academic;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2022-08-24T04:16:50.418-0300")
@StaticMetamodel(PostgraduateProgramMember.class)
public class PostgraduateProgramMember_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<PostgraduateProgramMember, Date> beginDate;
	public static volatile SingularAttribute<PostgraduateProgramMember, Date> endDate;
	public static volatile SingularAttribute<PostgraduateProgramMember, String> bondType;
	public static volatile SingularAttribute<PostgraduateProgramMember, Academic> academic;
	public static volatile SingularAttribute<PostgraduateProgramMember, PostgraduateProgram> postgraduateProgram;
}
