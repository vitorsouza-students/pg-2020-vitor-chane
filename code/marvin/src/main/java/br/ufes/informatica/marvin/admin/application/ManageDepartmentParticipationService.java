package br.ufes.informatica.marvin.admin.application;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.admin.domain.Department;
import br.ufes.informatica.marvin.admin.domain.DepartmentParticipation;
import br.ufes.informatica.marvin.core.domain.Academic;

@Local
public interface ManageDepartmentParticipationService extends CrudService<DepartmentParticipation> {

	void saveEntity(DepartmentParticipation entity);
	
	boolean checkIfAcademicHasDepartmentParticipation(Academic academic);
	
	List<DepartmentParticipation> returnDepartmentParticipationByAcademic(Academic academic);

	List<DepartmentParticipation> returnDepartmentParticipationByDepartment(Department department);
	
	PersistentObjectConverterFromId<Academic> getAcademicConverter();
	
	PersistentObjectConverterFromId<Department> getDepartmentConverter();
	
	List<Academic> findAcademicByQuery(String query);
	
	List<Department> findDepartmentByName(String name);
}
