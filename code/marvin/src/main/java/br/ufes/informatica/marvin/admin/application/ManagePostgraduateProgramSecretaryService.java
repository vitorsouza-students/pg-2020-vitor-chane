package br.ufes.informatica.marvin.admin.application;

import java.util.List;
import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgram;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgramSecretary;

/**
 * Local EJB interface for the session information component. This bean provides an authentication
 * method for the controller, identifying users of the system.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Local
public interface ManagePostgraduateProgramSecretaryService extends CrudService<PostgraduateProgramSecretary> {

	void saveEntity(PostgraduateProgramSecretary entity);
	
	boolean checkIfAcademicIsPostgraduateProgramSecretary(Academic academic);
			
	List<PostgraduateProgramSecretary> returnPostgraduateProgramSecretaryByAcademic(Academic academic);
	
	List<PostgraduateProgramSecretary> returnPostgraduateProgramSecretaryByPostgraduateProgram(PostgraduateProgram postgraduateProgram);

	PersistentObjectConverterFromId<Academic> getAcademicConverter();
	
	PersistentObjectConverterFromId<PostgraduateProgram> getPostgraduateProgramConverter();
	
	List<Academic> findAcademicByQuery(String query);
	
	List<PostgraduateProgram> findPostgraduateProgramByQuery(String query);
}
