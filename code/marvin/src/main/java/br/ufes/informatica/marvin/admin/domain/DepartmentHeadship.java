package br.ufes.informatica.marvin.admin.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * The department headship class for the MACS (Marvin Access Control System) module of Marvin.
 *
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
 * @version 1.0
*/

@Entity
public class DepartmentHeadship extends PersistentObjectSupport implements Comparable<DepartmentHeadship> {
	
	/**==========================================DEPARTMENT HEADSHIP INFORMATION=================================================*/

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The timestamp of the moment the academic started to work on the department headship */
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private Date beginDate;
	
	/** Getter for beginDate. */
	public Date getBeginDate() {
	  return beginDate;
	}
	
	/** Setter for beginDate. */
	public void setBeginDate(Date beginDate) {
	  this.beginDate = beginDate;
	}
	
	/** The timestamp of the moment the academic ended the work on the department headship */
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	
	/** Getter for endDate. */
	public Date getEndDate() {
	  return endDate;
	}
	
	/** Setter for endDate. */
	public void setEndDate(Date endDate) {
	  this.endDate = endDate;
	}
	
	/** If the academic is the department boss(true) or underboss(false). */
	private boolean isBoss;
	
	/** Getter for isBoss. */
	public boolean getIsBoss() {
		return isBoss;
	}
	
	/** Setter for isBoss. */
	public void setIsBoss(boolean isBoss) {
	  this.isBoss = isBoss;
	}
	
	/** Compare function for department headship. */
	@Override
	public int compareTo(DepartmentHeadship o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**========================================================ACADEMIC========================================================*/
	
	/** Department boss or underboss (Academic with Professor role) for this department headship. */
	@NotNull
	@ManyToOne
	private Academic academic;
	
	/** Getter for academic. */
	public Academic getAcademic() {
		return academic;
	}
 	
 	/** Setter for academic. */
 	public void setAcademic(Academic academic) {
 		this.academic = academic;
 	}
	
	/**========================================================DEPARTMENT========================================================*/

	/** Related department for this department headship.*/
	@NotNull
	@ManyToOne
	private Department department;
	
	/** Getter for department. */
	public Department getDepartment() {
		return department;
	}
	
	/** Setter for department. */
	public void setDepartment(Department department) {
		this.department = department;
	}
}
