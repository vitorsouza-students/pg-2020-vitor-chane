package br.ufes.informatica.marvin.admin.application;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudException;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.PersistentObjectNotFoundException;
import br.ufes.informatica.marvin.admin.domain.PostgraduateCourse;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgram;
import br.ufes.informatica.marvin.admin.persistence.PostgraduateCourseDAO;
import br.ufes.informatica.marvin.admin.persistence.PostgraduateProgramDAO;

/**
 * Stateless session bean implementing the PostgraduateCourse service.
 * See the implemented interface documentation for details.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
@RolesAllowed("SysAdmin")
public class ManagePostgraduateCourseServiceBean extends CrudServiceBean<PostgraduateCourse> implements ManagePostgraduateCourseService {

	/**=========================================UNDERGRADUATE COURSE========================================================*/

	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(ManagePostgraduateCourseServiceBean.class.getCanonicalName());

	/** The PostgraduateCourseDAO is used to save the Undergraduate Course that is registered by the SysAdmin. */
	@EJB
	private PostgraduateCourseDAO postgraduateCourseDAO;
	
	/** Getter for PostgraduateCourseDAO. */
	@Override
	public BaseDAO<PostgraduateCourse> getDAO() {
		return postgraduateCourseDAO;
	}
	
	/** 
	 * Check if there is already a PostgraduateCourse with the same name. If exists throw a exception.
	 * 
	 * @param postgraduateCourse
	 * @throws CrudException
	*/
	@Override
	public void validateCreate(PostgraduateCourse postgraduateCourse) throws CrudException {
		
		// Checks if the chosen name is already in use.
		try {
			postgraduateCourseDAO.retrieveByName(postgraduateCourse.getName());
			logger.log(Level.WARNING, "New PostgraduateCourse violates unique name rule: \"{0}\"", postgraduateCourse.getName());
			throw new CrudException(null, "New PostgraduateCourse violates unique name rule!", null);
		}
		catch (PersistentObjectNotFoundException e) {
			// This is the expected outcome. Just log that everything is OK.
			logger.log(Level.INFO, "New PostgraduateCourse satisfies unique name rule: \"{0}\"", postgraduateCourse.getName());
		}
		catch (MultiplePersistentObjectsFoundException e) {
			// This is a severe problem: the unique constraint has already been violated.
			logger.log(Level.SEVERE, "There are already PostgraduateCourse with the name: \"{0}\"!", postgraduateCourse.getName());
			throw new EJBException(e);
		}
	}
	
	/**===============================================PostgraduateProgram========================================================*/

	/** The PostgraduateProgramDAO is used to save the PostgraduateProgram that is registered by the SysAdmin. */
	@EJB
	private PostgraduateProgramDAO postgraduateProgramDAO;
	
	private PersistentObjectConverterFromId<PostgraduateProgram> postgraduateProgramConverter;
	
	@Override
	public PersistentObjectConverterFromId<PostgraduateProgram> getPostgraduateProgramConverter() {
		
		if(postgraduateProgramConverter == null) {
			postgraduateProgramConverter = new PersistentObjectConverterFromId<PostgraduateProgram>(postgraduateProgramDAO);
		}
		return postgraduateProgramConverter;
	}
	
	@Override
	public List<PostgraduateProgram> findPostgraduateProgramByName(String query) {
		return postgraduateProgramDAO.findByQuery(query);
	}
}
