package br.ufes.informatica.marvin.admin.persistence;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;
import br.ufes.informatica.marvin.admin.domain.DepartmentStudentRepresentation_;
import br.ufes.informatica.marvin.admin.domain.Department;
import br.ufes.informatica.marvin.admin.domain.DepartmentStudentRepresentation;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * Stateless session bean implementing a DAO for objects of the DepartmentStudentRepresentation domain class using JPA2.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation implementations are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any is defined
 * in the implementing DAO interface) have to be implemented in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
public class DepartmentStudentRepresentationJPADAO extends BaseJPADAO<DepartmentStudentRepresentation> implements DepartmentStudentRepresentationDAO {
	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(DepartmentStudentRepresentationJPADAO.class.getCanonicalName());

	/** The application's persistent context provided by the application server. */
	@PersistenceContext
	private EntityManager entityManager;
	
	/** Getter for Entity Manager. */
	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	public List<DepartmentStudentRepresentation> retrieveByAcademic(Academic academic) {
		logger.log(Level.FINE, "Retrieving all department student representation instances whose academic is \"{0}\"...", academic);
		
	    // Constructs the query over the DepartmentStudentRepresentation class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<DepartmentStudentRepresentation> cq = cb.createQuery(DepartmentStudentRepresentation.class);
	    Root<DepartmentStudentRepresentation> root = cq.from(DepartmentStudentRepresentation.class);
	
	    // Filters the query with the academic.
	    cq.where(cb.equal(root.get(DepartmentStudentRepresentation_.academic), academic));
	    List<DepartmentStudentRepresentation> result = entityManager.createQuery(cq).getResultList();
	    logger.log(Level.INFO, "Retrieving all department student representation instances by the academic \"{0}\" returned {1} results", new Object[] {academic, result.size()});
	    
	    return result;
	}
	
	public List<DepartmentStudentRepresentation> retrieveByDepartment(Department department) {
		logger.log(Level.FINE, "Retrieving all department student representation instances whose department is \"{0}\"...", department);
		
		// Constructs the query over the DepartmentStudentRepresentation class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<DepartmentStudentRepresentation> cq = cb.createQuery(DepartmentStudentRepresentation.class);
	    Root<DepartmentStudentRepresentation> root = cq.from(DepartmentStudentRepresentation.class);
	    
	    // Filters the query with the department.
	    cq.where(cb.equal(root.get(DepartmentStudentRepresentation_.department), department));
	    List<DepartmentStudentRepresentation> result = entityManager.createQuery(cq).getResultList();
	    
	    logger.log(Level.INFO, "Retrieving all department student representation instances by the department \"{0}\" returned {1} results", new Object[] {department, result.size()});
	    
	    return result;
	}
}