package br.ufes.informatica.marvin.admin.application;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.core.persistence.AcademicDAO;
import br.ufes.informatica.marvin.admin.domain.PostgraduateCourse;
import br.ufes.informatica.marvin.admin.domain.PostgraduateCourseEnrollment;
import br.ufes.informatica.marvin.admin.persistence.PostgraduateCourseDAO;
import br.ufes.informatica.marvin.admin.persistence.PostgraduateCourseEnrollmentDAO;

/**
 * Stateless session bean implementing the ManagePostgraduateCourseEnrollmentService.
 * See the implemented interface documentation for details.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
@RolesAllowed({"SysAdmin", "Professor", "Staff"})
public class ManagePostgraduateCourseEnrollmentServiceBean extends CrudServiceBean<PostgraduateCourseEnrollment> implements ManagePostgraduateCourseEnrollmentService {

	/**=====================================PostgraduateCourseEnrollment=====================================*/

	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** The PostgraduateCourseEnrollmentDAO is used to save the PostgraduateCourseEnrollment that is registered by the SysAdmin. */
	@EJB
	private PostgraduateCourseEnrollmentDAO postgraduateCourseEnrollmentDAO;
	
	/** Getter for PostgraduateCourseEnrollmentDAO. */
	@Override
	public BaseDAO<PostgraduateCourseEnrollment> getDAO() {
		return postgraduateCourseEnrollmentDAO;
	}
	
	@Override
	public List<PostgraduateCourseEnrollment> returnPostgraduateCourseEnrollmentByAcademic(Academic academic) {
		return postgraduateCourseEnrollmentDAO.retrieveByAcademic(academic);
	}
	
	@Override
	public List<PostgraduateCourseEnrollment> returnPostgraduateCourseEnrollmentByPostgraduateCourse(PostgraduateCourse postgraduateCourse){
		return postgraduateCourseEnrollmentDAO.retrieveByPostgraduateCourse(postgraduateCourse);
	}
	
	public void saveEntity(PostgraduateCourseEnrollment entity) {
		// Validates the entity before persisting.
		entity = validate(entity, null);
		
		// Save the entity.
		postgraduateCourseEnrollmentDAO.save(entity);
	}
	
	/**========================================================ACADEMIC========================================================*/

	@EJB
	private AcademicDAO academicDAO;
	
	private PersistentObjectConverterFromId<Academic> academicConverter;
	
	@Override
	public PersistentObjectConverterFromId<Academic> getAcademicConverter() {
		if(academicConverter == null) {
			academicConverter = new PersistentObjectConverterFromId<Academic>(academicDAO);
		}
		return academicConverter;
	}
	
	@Override
	public List<Academic> findAcademicByQuery(String query) {
		return academicDAO.findByNameEmailOrCpf(query);
	}
	
	/**===========================================PostgraduateCourse===========================================*/

	@EJB
	private PostgraduateCourseDAO postgraduateCourseDAO;
	
	private PersistentObjectConverterFromId<PostgraduateCourse> postgraduateCourseConverter;
	
	@Override
	public PersistentObjectConverterFromId<PostgraduateCourse> getPostgraduateCourseConverter() {
		if(postgraduateCourseConverter == null) {
			postgraduateCourseConverter = new PersistentObjectConverterFromId<PostgraduateCourse>(postgraduateCourseDAO);
		}
		return postgraduateCourseConverter;
	}

	@Override
	public List<PostgraduateCourse> findPostgraduateCourseByQuery(String query) {
		return postgraduateCourseDAO.findByQuery(query);
	}
}
