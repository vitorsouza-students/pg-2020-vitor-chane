package br.ufes.informatica.marvin.admin.controller;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.application.filters.LikeFilter;
import br.ufes.inf.nemo.jbutler.ejb.controller.CrudController;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.admin.domain.Faculty;
import br.ufes.informatica.marvin.admin.application.ManageUndergraduateCourseService;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourse;

@Named
@SessionScoped
public class ManageUndergraduateCourseController extends CrudController<UndergraduateCourse>{

	/**=============================================UNDEGRADUATE COURSE======================================================*/

	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** The "Manage Undergraduate Course" service. */
	@EJB
	private ManageUndergraduateCourseService manageUndergraduateCourseService;
	
	/** Getter for "Manage Undergraduate Course" service. */
	@Override
	protected CrudService<UndergraduateCourse> getCrudService() {
		return manageUndergraduateCourseService;
	}
	
	@Override
	protected void initFilters() {
		addFilter(new LikeFilter("manageUndergraduateCourse.filter.byName", "name", "Filter Undergraduate Course by name"));
		addFilter(new LikeFilter("manageUndergraduateCourse.filter.byCode", "code", "Filter Undergraduate Course by code"));
		addFilter(new LikeFilter("manageUndergraduateCourse.filter.byShift", "shift", "Filter Undergraduate Course by shift"));
	}
	
	/**=====================================================FACULTY=====================================================*/
	
	public PersistentObjectConverterFromId<Faculty> getFacultyConverter() {
		return manageUndergraduateCourseService.getFacultyConverter();
	}
	
	public Faculty getSelectedFaculty() {
		Faculty faculty = selectedEntity.getFaculty();
		
		return (faculty == null) ? new Faculty() : faculty;
	}
	
	public void setSelectedFaculty(Faculty selectedFaculty) {
		selectedEntity.setFaculty((selectedFaculty == null) ? new Faculty() : selectedFaculty);
	}
	
	public List<Faculty> completeFaculty(String query) {
		return manageUndergraduateCourseService.findFacultyByName(query);
	}
}
