package br.ufes.informatica.marvin.admin.application;

import java.util.List;
import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourse;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourseInternshipCoordination;

/**
 * Local EJB interface for the session information component. This bean provides an authentication
 * method for the controller, identifying users of the system.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Local
public interface ManageUndergraduateCourseInternshipCoordinationService extends CrudService<UndergraduateCourseInternshipCoordination> {

	void saveEntity(UndergraduateCourseInternshipCoordination entity);
		
	List<UndergraduateCourseInternshipCoordination> returnUndergraduateCourseInternshipCoordinationByAcademic(Academic academic);
	
	List<UndergraduateCourseInternshipCoordination> returnUndergraduateCourseInternshipCoordinationByDepartment(UndergraduateCourse undergraduateCourse);

	PersistentObjectConverterFromId<Academic> getAcademicConverter();
	
	PersistentObjectConverterFromId<UndergraduateCourse> getUndergraduateCourseConverter();
	
	List<Academic> findAcademicByQuery(String query);
	
	List<UndergraduateCourse> findUndergraduateCourseByQuery(String query);
}