package br.ufes.informatica.marvin.admin.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudException;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudValidationError;
import br.ufes.inf.nemo.jbutler.ejb.application.filters.LikeFilter;
import br.ufes.inf.nemo.jbutler.ejb.controller.CrudController;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.PersistentObjectNotFoundException;
import br.ufes.informatica.marvin.core.controller.SessionController;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.core.domain.Role;
import br.ufes.informatica.marvin.admin.application.ManageUndergraduateCourseCoordinationService;
import br.ufes.informatica.marvin.admin.application.ManageUndergraduateCourseCoordinationServiceBean;
import br.ufes.informatica.marvin.admin.application.ManageUndergraduateCourseSecretaryService;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourse;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourseCoordination;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourseSecretary;

@Named
@SessionScoped
public class ManageUndergraduateCourseCoordinationController extends CrudController<UndergraduateCourseCoordination>{

	/**====================================UndergraduateCourseCoordination======================================================*/

	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(ManageUndergraduateCourseCoordinationServiceBean.class.getCanonicalName());

	/** The ManageUndergraduateCourseCoordinationService. */
	@EJB
	private ManageUndergraduateCourseCoordinationService manageUndergraduateCourseCoordinationService;
	
	/** Getter for manageUndergraduateCourseService. */
	@Override
	protected CrudService<UndergraduateCourseCoordination> getCrudService() {
		return manageUndergraduateCourseCoordinationService;
	}
	
	@Override
	protected void initFilters() {
		addFilter(new LikeFilter("manageUndergraduateCourseCoordination.filter.byName", "name", "Filter Undergraduate Course Coordination by name"));
		addFilter(new LikeFilter("manageUndergraduateCourseCoordination.filter.byUndergraduateCourse", "undergraduateCourse", "Filter Undergraduate Course Coordination by undergraduate course"));
		addFilter(new LikeFilter("manageUndergraduateCourseCoordination.filter.byBeginDate", "beginDate", "Filter Undergraduate Course Coordination by begin date"));
		addFilter(new LikeFilter("manageUndergraduateCourseCoordination.filter.byEndDate", "endDate", "Filter Undergraduate Course Coordination by end date"));
		addFilter(new LikeFilter("manageUndergraduateCourseCoordination.filter.byIsCoordinator", "isCoordinator", "Filter Undergraduate Course Coordination by specific role"));
	}
	
	/**=======================Appoint Undergraduate Course Coordinator=======================*/

	/** Path to the folder where the view files (web pages) for Appoint Undergraduate Course Coordination is. */
	private static final String VIEW_PATH = "/admin/appointUndergraduateCourseCoordination/";

	/** The session controller, with information of the authenticated user. */
	@Inject
	private SessionController sessionController;
	
	@EJB
	private ManageUndergraduateCourseSecretaryService manageUndergraduateCourseSecretaryService;
	
	/** The entity that will have its participation finalized. */
	protected UndergraduateCourseCoordination finishCan;
	
	/** Getter for finishCan. */
	public UndergraduateCourseCoordination getFinishCan() {
		return finishCan;
	}
	
	/** Return if the current logged user is a valid Undergraduate Course Coordinator/ Subcoordinator/ Secretary */
	public boolean isCurrentUserValidUndergraduateCourseAdmin() {
		Academic currentUser = sessionController.getCurrentUser();
		
		if(sessionController.isLoggedIn()) {
			if(sessionController.isProfessor()) {
				if(manageUndergraduateCourseCoordinationService.checkIfAcademicIsUndergraduateCourseCoordinator(currentUser)) {
					return true;
				}
			}
			if(sessionController.isStaff()) {
				if(manageUndergraduateCourseSecretaryService.checkIfAcademicIsUndergraduateCourseSecretary(currentUser)) {
					return true;
				}
			}
		}

		return false;
	}
	
	/** Return the logged user undergraduate course if he is a valid Undergraduate Course Coordinator/ Subcoordinator/ Secretary */
	public UndergraduateCourse getAdminUndergraduateCourse() {
		Academic currentUser = sessionController.getCurrentUser();

		List<UndergraduateCourseCoordination> undergraduateCourseCoordination = manageUndergraduateCourseCoordinationService.returnUndergraduateCourseCoordinationByAcademic(currentUser);
		List<UndergraduateCourseSecretary> undergraduateCourseSecretary = manageUndergraduateCourseSecretaryService.returnUndergraduateCourseSecretaryByAcademic(currentUser);
		
		if(isCurrentUserValidUndergraduateCourseAdmin()) {
			if(sessionController.isProfessor()) {
				return undergraduateCourseCoordination.get(firstEntityIndex).getUndergraduateCourse();
			}
		}
		if(isCurrentUserValidUndergraduateCourseAdmin()) {
			if(sessionController.isStaff()) {
				return undergraduateCourseSecretary.get(firstEntityIndex).getUndergraduateCourse();
			}
		}
		return null;
	}
	
	/** Render the Appoint new academic form */
	public String appointNew() {

		// Sets the data as read-write.
		readOnly = false;
		
		// Resets the entity so we can create a new one.
		selectedEntity = createNewEntity();
		
	    return VIEW_PATH + "form.xhtml?faces-redirect=true";
	}
	
	public String saveAppointment() {
		logger.log(Level.INFO, "Saving new appoint entity...");

		// Prepare the entity for saving.
		prepEntity();
		
		Long dateNow = System.currentTimeMillis();
        Timestamp timestamp = new Timestamp(dateNow);

		// Validates the operation first and stops in case of errors.
		try {
			if (selectedEntity.getId() == null) {
				selectedEntity.setBeginDate(timestamp);
				selectedEntity.setUndergraduateCourse(getAdminUndergraduateCourse());
				
				if (selectedEntity.getIsCoordinator() == true) {
					List<UndergraduateCourseCoordination> allCoordinators;
					allCoordinators = manageUndergraduateCourseCoordinationService.returnUndergraduateCourseCoordinationByDepartment(getAdminUndergraduateCourse());
					
					for(UndergraduateCourseCoordination entity: allCoordinators) {
						if(entity.getIsCoordinator() == true && entity.getEndDate() == null) {
							logger.log(Level.INFO, "There is an active coordinator at the moment that will be finalized");
							entity.setEndDate(timestamp);
							manageUndergraduateCourseCoordinationService.saveEntity(entity);
						}
					}
				}
				if (selectedEntity.getIsCoordinator() == false) {	
					List<UndergraduateCourseCoordination> allSubcoordinators;
					allSubcoordinators = manageUndergraduateCourseCoordinationService.returnUndergraduateCourseCoordinationByDepartment(getAdminUndergraduateCourse());
					
					for(UndergraduateCourseCoordination entity: allSubcoordinators) {
						if(entity.getIsCoordinator() == false && entity.getEndDate() == null) {
							logger.log(Level.INFO, "There is an active subcoordinator at the moment that will be finalized");
							entity.setEndDate(timestamp);
							manageUndergraduateCourseCoordinationService.saveEntity(entity);
						}
					}
				}
				
				getCrudService().validateCreate(selectedEntity);
				getCrudService().create(selectedEntity);
				addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_INFO, getBundlePrefix() + ".text.createSucceeded", summarizeSelectedEntity());
			}
		}
		catch (CrudException crudException) {
			// Adds an error message to each validation error included in the exception.
			for (CrudValidationError error : crudException) {
				logger.log(Level.WARNING, "Exception while saving " + selectedEntity, crudException.getMessage());

				// Checks if the field name was specified. If it was, attach the message to the form field.
				if (error.getFieldName() != null) {
					addFieldI18nMessage(getFieldName(error.getFieldName()), getBundleName(), FacesMessage.SEVERITY_ERROR, error.getMessageKey(), error.getMessageParams());
				}
				else {
					addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_ERROR, error.getMessageKey(), error.getMessageParams());
				}
			}

			// Goes back to the same page, i.e., the form.
			return null;
		}

		// Goes back to the listing.
		return listEntities();
	}
	
	public String listEntities() {
		logger.log(Level.INFO, "Listing entities...");

		// Clears the selection.
		selectedEntity = null;

		// Gets the entity count.
		count();

		// Checks if the index of the listing should be changed and reload the page.
		if (firstEntityIndex < 0) goFirst();
		else if (lastEntityIndex > entityCount) goLast();
		else retrieveEntities();

		// Goes to the listing.
		return VIEW_PATH + "index.xhtml?faces-redirect=true";
	}
	
	public String cancelAppointment() {
		
		// Removes the entity from the finish can and cancel his finalization.
		logger.log(Level.INFO, "Appointment has been cancelled. Clearing selected entity");
		selectedEntity = null;
		
		// Goes back to the listing.
		return VIEW_PATH + "index.xhtml?faces-redirect=true";
	}
	
	/** Moves the selected entity to the finish can for possible future end of allocation. */
	public void finishParticipation() {
		
		// Proceed only if there is a selected entity or selected entity doesn't have a setted end date.
		if ((selectedEntity == null) || (selectedEntity.getEndDate() != null)) {
			logger.log(Level.WARNING, "Method finishParticipation() called, but selectedEntity is null or have been finalized already!");
			return;
		}		

		// Adds the selected entity to the finish can so the user can confirm the finalization.
		logger.log(Level.INFO, "Adding {0} (id {1}) to the finish can for future end of allocation.", new Object[] { selectedEntity, selectedEntity.getId() });
		finishCan = selectedEntity;
	}
	
	/** Set the end Date for the entity in the finish can. The date will be equal to the current date. */
	public void setEndDate() {
		
		logger.log(Level.INFO, "Setting end date for the selected entity...");
		Long dateNow = System.currentTimeMillis();
        Timestamp timestamp = new Timestamp(dateNow);
		
		finishCan.setEndDate(timestamp);
		manageUndergraduateCourseCoordinationService.saveEntity(finishCan);
		
		// Clears the finishCan
		finishCan = null;
				
		// Clears the selection.
		selectedEntity = null;
	}
	
	/** Cancel finishing selected entity allocation and cleans the finish can. */
	public void cancelFinishParticipation() {
		
		// Removes the entity from the finish can and cancel his finalization.
		logger.log(Level.INFO, "Finalization has been cancelled. Clearing finish can");
		finishCan = null;

		// Clears the selection.
		selectedEntity = null;
	}
	
	/**=====================================================ACADEMIC=====================================================*/
	
	public PersistentObjectConverterFromId<Academic> getAcademicConverter() {
		return manageUndergraduateCourseCoordinationService.getAcademicConverter();
	}
	
	public Academic getSelectedAcademic() {		
		Academic academic = selectedEntity.getAcademic();
		
		return (academic == null) ? new Academic() : academic;
	}
	
	public void setSelectedAcademic(Academic selectedAcademic) throws CrudException {
		Set<Role> setRole = selectedAcademic.getRoles();
		List<Role> listRole = convertSetToList(setRole);
		boolean haveProfessorRole = false;
		
		for(int i = 0; i< listRole.size(); i++) {
			if(listRole.get(i).getName().compareTo("Professor") == 0) {
				selectedEntity.setAcademic((selectedAcademic == null) ? new Academic() : selectedAcademic);
				haveProfessorRole = true;
			}
		}
		
		if(haveProfessorRole == false) {
			logger.log(Level.WARNING, "New academic violates role condition");
			throw new CrudException("New academic must have the role: Professor", null, null);
		}
	}
	
	public static <T> List<T> convertSetToList(Set<T> set) {
        
		// create an empty list
        List<T> list = new ArrayList<>();
  
        // push each element in the set into the list
        for (T t : set)
            list.add(t);
  
        // return the list
        return list;
    }
	
	public List<Academic> completeAcademic(String query) throws PersistentObjectNotFoundException, MultiplePersistentObjectsFoundException {
		return manageUndergraduateCourseCoordinationService.findAcademicByQuery(query);
	}
	
	/**==================================================UndergraduateCourse==================================================*/

	public PersistentObjectConverterFromId<UndergraduateCourse> getUndergraduateCourseConverter() {
		return manageUndergraduateCourseCoordinationService.getUndergraduateCourseConverter();
	}
	
	public UndergraduateCourse getSelectedUndergraduateCourse() {
		UndergraduateCourse undergraduateCourse = selectedEntity.getUndergraduateCourse();
		return (undergraduateCourse == null) ? new UndergraduateCourse() : undergraduateCourse;
	}
	
	public void setSelectedUndergraduateCourse(UndergraduateCourse selectedUndergraduateCourse) {
		selectedEntity.setUndergraduateCourse((selectedUndergraduateCourse == null) ? new UndergraduateCourse() : selectedUndergraduateCourse);
	}
	
	public List<UndergraduateCourse> completeUndergraduateCourse(String query) {
		return manageUndergraduateCourseCoordinationService.findUndergraduateCourseByQuery(query);
	}
}