package br.ufes.informatica.marvin.admin.application;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.core.persistence.AcademicDAO;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourse;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourseCollegiateMembers;
import br.ufes.informatica.marvin.admin.persistence.UndergraduateCourseCollegiateMembersDAO;
import br.ufes.informatica.marvin.admin.persistence.UndergraduateCourseDAO;

/**
 * Stateless session bean implementing the ManageUndergraduateCourseCollegiateMembersService.
 * See the implemented interface documentation for details.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
@RolesAllowed({"SysAdmin", "Professor", "Staff"})
public class ManageUndergraduateCourseCollegiateMembersServiceBean extends CrudServiceBean<UndergraduateCourseCollegiateMembers> implements ManageUndergraduateCourseCollegiateMembersService {
	
	/**=====================================UndergraduateCourseCollegiateMembers=====================================*/

	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;

	/** The UndergraduateCourseCollegiateMembersDAO is used to save the UndergraduateCourseCollegiateMembers that is registered by the SysAdmin. */
	@EJB
	private UndergraduateCourseCollegiateMembersDAO undergraduateCourseCollegiateMembersDAO;
	
	/** Getter for UndergraduateCourseCollegiateMembersDAO. */
	@Override
	public BaseDAO<UndergraduateCourseCollegiateMembers> getDAO() {
		return undergraduateCourseCollegiateMembersDAO;
	}
	
	@Override
	public List<UndergraduateCourseCollegiateMembers> returnUndergraduateCourseCollegiateMembersByAcademic(Academic academic) {
		return undergraduateCourseCollegiateMembersDAO.retrieveByAcademic(academic);
	}
	
	@Override
	public List<UndergraduateCourseCollegiateMembers> returnUndergraduateCourseCollegiateMembersByDepartment(UndergraduateCourse undergraduateCourse){
		return undergraduateCourseCollegiateMembersDAO.retrieveByUndergraduateCourse(undergraduateCourse);
	}
	
	public void saveEntity(UndergraduateCourseCollegiateMembers entity) {
		// Validates the entity before persisting.
		entity = validate(entity, null);
		
		// Save the entity.
		undergraduateCourseCollegiateMembersDAO.save(entity);
	}
	
	/**========================================================ACADEMIC========================================================*/

	@EJB
	private AcademicDAO academicDAO;
	
	private PersistentObjectConverterFromId<Academic> academicConverter;
	
	@Override
	public PersistentObjectConverterFromId<Academic> getAcademicConverter() {
		if(academicConverter == null) {
			academicConverter = new PersistentObjectConverterFromId<Academic>(academicDAO);
		}
		return academicConverter;
	}
	
	@Override
	public List<Academic> findAcademicByQuery(String query) {
		return academicDAO.findByNameEmailOrCpf(query);
	}
	
	/**===========================================UndergraduateCourse===========================================*/

	@EJB
	private UndergraduateCourseDAO undergraduateCourseDAO;
	
	private PersistentObjectConverterFromId<UndergraduateCourse> undergraduateCourseConverter;
	
	@Override
	public PersistentObjectConverterFromId<UndergraduateCourse> getUndergraduateCourseConverter() {
		if(undergraduateCourseConverter == null) {
			undergraduateCourseConverter = new PersistentObjectConverterFromId<UndergraduateCourse>(undergraduateCourseDAO);
		}
		return undergraduateCourseConverter;
	}

	@Override
	public List<UndergraduateCourse> findUndergraduateCourseByQuery(String query) {
		return undergraduateCourseDAO.findByQuery(query);
	}
}
