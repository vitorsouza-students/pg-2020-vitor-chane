package br.ufes.informatica.marvin.admin.application;

import java.util.List;
import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgram;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgramMember;

/**
 * Local EJB interface for the session information component. This bean provides an authentication
 * method for the controller, identifying users of the system.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Local
public interface ManagePostgraduateProgramMemberService extends CrudService<PostgraduateProgramMember> {

	void saveEntity(PostgraduateProgramMember entity);
	
	boolean checkIfAcademicIsPostgraduateProgramMember(Academic academic);
			
	List<PostgraduateProgramMember> returnPostgraduateProgramMemberByAcademic(Academic academic);
	
	List<PostgraduateProgramMember> returnPostgraduateProgramMemberByPostgraduateProgram(PostgraduateProgram postgraduateProgram);

	PersistentObjectConverterFromId<Academic> getAcademicConverter();
	
	PersistentObjectConverterFromId<PostgraduateProgram> getPostgraduateProgramConverter();
	
	List<Academic> findAcademicByQuery(String query);
	
	List<PostgraduateProgram> findPostgraduateProgramByQuery(String query);
}