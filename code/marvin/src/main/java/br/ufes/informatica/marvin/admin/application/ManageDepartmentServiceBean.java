package br.ufes.informatica.marvin.admin.application;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudException;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.PersistentObjectNotFoundException;
import br.ufes.informatica.marvin.admin.domain.Department;
import br.ufes.informatica.marvin.admin.domain.Faculty;
import br.ufes.informatica.marvin.admin.persistence.DepartmentDAO;
import br.ufes.informatica.marvin.admin.persistence.FacultyDAO;

/**
 * Stateless session bean implementing the department service. See the implemented interface
 * documentation for details.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
@RolesAllowed("SysAdmin")
public class ManageDepartmentServiceBean extends CrudServiceBean<Department> implements ManageDepartmentService {
	
	/**===============================================DEPARTMENT========================================================*/
	
	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(ManageDepartmentServiceBean.class.getCanonicalName());
	
	/** The Department DAO is used to save the department that is registered by the SysAdmin. */
	@EJB
	private DepartmentDAO departmentDAO;
	
	/** Getter for department DAO. */
	@Override
	public BaseDAO<Department> getDAO() {
		return departmentDAO;
	}
	
	/** 
	 * Check if there is already a department with the same name. If exists throw a exception.
	 * 
	 * @param department
	 * @throws CrudException
	*/
	@Override
	public void validateCreate(Department department) throws CrudException {
		
		// Checks if the chosen name is already in use.
		try {
			departmentDAO.retrieveByName(department.getName());
			logger.log(Level.WARNING, "New department violates unique name rule: \"{0}\"", department.getName());
			throw new CrudException(null, "New department violates unique name rule!", null);
		}
		catch (PersistentObjectNotFoundException e) {
			// This is the expected outcome. Just log that everything is OK.
			logger.log(Level.INFO, "New department satisfies unique name rule: \"{0}\"", department.getName());
		}
		catch (MultiplePersistentObjectsFoundException e) {
			// This is a severe problem: the unique constraint has already been violated.
			logger.log(Level.SEVERE, "There are already departments with the name: \"{0}\"!", department.getName());
			throw new EJBException(e);
		}
	}
	
	/**===============================================FACULTY========================================================*/

	/** The Faculty DAO is used to save the faculty that is registered by the SysAdmin. */
	@EJB
	private FacultyDAO facultyDAO;
	
	private PersistentObjectConverterFromId<Faculty> facultyConverter;
	
	@Override
	public PersistentObjectConverterFromId<Faculty> getFacultyConverter() {
		
		if(facultyConverter == null) {
			facultyConverter = new PersistentObjectConverterFromId<Faculty>(facultyDAO);
		}
		return facultyConverter;
	}
	
	@Override
	public List<Faculty> findFacultyByName(String name) {
		return facultyDAO.findByName(name);
	}
}