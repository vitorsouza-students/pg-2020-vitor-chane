package br.ufes.informatica.marvin.admin.persistence;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.PersistentObjectNotFoundException;
import br.ufes.informatica.marvin.admin.domain.Department;
import br.ufes.informatica.marvin.admin.domain.Department_;

/**
 * Stateless session bean implementing a DAO for objects of the Department domain class using JPA2.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation implementations are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any is defined
 * in the implementing DAO interface) have to be implemented in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
public class DepartmentJPADAO extends BaseJPADAO<Department> implements DepartmentDAO {
	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(DepartmentJPADAO.class.getCanonicalName());
	
	/** The application's persistent context provided by the application server. */
	@PersistenceContext
	private EntityManager entityManager;

	/** Getter for Entity Manager. */
	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	@Override
	public Department retrieveByName(String name) throws PersistentObjectNotFoundException, MultiplePersistentObjectsFoundException {
		
		logger.log(Level.FINE, "Retrieving the department whose name is \"{0}\"...", name);
		
		// Constructs the query over the Faculty class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<Department> cq = cb.createQuery(Department.class);
	    Root<Department> root = cq.from(Department.class);

	    // Filters the query
	    cq.where(cb.equal(root.get(Department_.name), name));
	    Department result = executeSingleResultQuery(cq, name);
	    logger.log(Level.INFO, "Retrieve department with the name \"{0}\" returned \"{1}\"", new Object[] {name, result});
	    
	    return result;
	}
	
	@Override
	public List<Department> findByName(String name) {
		
		logger.log(Level.FINE, "Finding departments whose department contain \"{0}\"...", name);
		
		// Constructs the query over the Department class.
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Department> cq = cb.createQuery(Department.class);
		Root<Department> root = cq.from(Department.class);
		
		// Filters the query
	    name = "%" + name + "%";
	    cq.where(cb.like(root.get(Department_.name), name));
	    List<Department> result = entityManager.createQuery(cq).getResultList();
	    logger.log(Level.INFO, "Found {0} department whose name contains \"{1}\".", new Object[] { result.size(), name });
	    
	    return result;
	}
}
