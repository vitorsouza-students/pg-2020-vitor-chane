package br.ufes.informatica.marvin.admin.application;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.admin.domain.Department;
import br.ufes.informatica.marvin.admin.domain.DepartmentStudentRepresentation;
import br.ufes.informatica.marvin.core.domain.Academic;

@Local
public interface ManageDepartmentStudentRepresentationService extends CrudService<DepartmentStudentRepresentation> {

	void saveEntity(DepartmentStudentRepresentation entity);
	
	List<DepartmentStudentRepresentation> returnDepartmentHeadshipByAcademic(Academic academic);
	
	List<DepartmentStudentRepresentation> returnDepartmentHeadshipByDepartment(Department department);
	
	PersistentObjectConverterFromId<Academic> getAcademicConverter();
	
	PersistentObjectConverterFromId<Department> getDepartmentConverter();
	
	List<Academic> findAcademicByQuery(String query);
	
	List<Department> findDepartmentByName(String name);
}
