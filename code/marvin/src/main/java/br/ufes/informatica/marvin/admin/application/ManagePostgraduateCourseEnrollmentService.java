package br.ufes.informatica.marvin.admin.application;

import java.util.List;
import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.admin.domain.PostgraduateCourse;
import br.ufes.informatica.marvin.admin.domain.PostgraduateCourseEnrollment;

/**
 * Local EJB interface for the session information component. This bean provides an authentication
 * method for the controller, identifying users of the system.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Local
public interface ManagePostgraduateCourseEnrollmentService extends CrudService<PostgraduateCourseEnrollment> {

	void saveEntity(PostgraduateCourseEnrollment entity);
				
	List<PostgraduateCourseEnrollment> returnPostgraduateCourseEnrollmentByAcademic(Academic academic);
	
	List<PostgraduateCourseEnrollment> returnPostgraduateCourseEnrollmentByPostgraduateCourse(PostgraduateCourse postgraduateCourse);

	PersistentObjectConverterFromId<Academic> getAcademicConverter();
	
	PersistentObjectConverterFromId<PostgraduateCourse> getPostgraduateCourseConverter();
	
	List<Academic> findAcademicByQuery(String query);
	
	List<PostgraduateCourse> findPostgraduateCourseByQuery(String query);
}
