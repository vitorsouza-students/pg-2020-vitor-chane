package br.ufes.informatica.marvin.admin.controller;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.application.filters.LikeFilter;
import br.ufes.inf.nemo.jbutler.ejb.controller.CrudController;
import br.ufes.informatica.marvin.admin.application.ManageFacultyService;
import br.ufes.informatica.marvin.admin.domain.Faculty;

@Named
@SessionScoped
public class ManageFacultyController extends CrudController<Faculty>{

	private static final long serialVersionUID = 1L;
		
	@EJB
	private ManageFacultyService manageFacultiesService;
	
	@Override
	protected CrudService<Faculty> getCrudService() {
		return manageFacultiesService;
	}
	
	@Override
	protected void initFilters() {
		addFilter(new LikeFilter("manageFaculty.filter.byName", "name", "Filter faculty by name"));
	}
}
