package br.ufes.informatica.marvin.admin.persistence;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.PersistentObjectNotFoundException;
import br.ufes.informatica.marvin.admin.domain.PostgraduateCourse_;
import br.ufes.informatica.marvin.admin.domain.PostgraduateCourse;

/**
 * Stateless session bean implementing a DAO for objects of the PostgraduateCourse domain class using JPA2.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation implementations are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any is defined
 * in the implementing DAO interface) have to be implemented in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
public class PostgraduateCourseJPADAO extends BaseJPADAO<PostgraduateCourse> implements PostgraduateCourseDAO {
	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(PostgraduateCourseJPADAO.class.getCanonicalName());

	/** The application's persistent context provided by the application server. */
	@PersistenceContext
	private EntityManager entityManager;

	/** Getter for Entity Manager. */
	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	@Override
	public PostgraduateCourse retrieveByName(String name) throws PersistentObjectNotFoundException, MultiplePersistentObjectsFoundException {
		logger.log(Level.FINE, "Retrieving the undergraduate course whose name is \"{0}\"...", name);
	
		// Constructs the query over the Faculty class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<PostgraduateCourse> cq = cb.createQuery(PostgraduateCourse.class);
	    Root<PostgraduateCourse> root = cq.from(PostgraduateCourse.class);
	    
	    // Filters the query
	    cq.where(cb.equal(root.get(PostgraduateCourse_.name), name));
	    PostgraduateCourse result = executeSingleResultQuery(cq, name);
	    logger.log(Level.INFO, "Retrieve undergraduate course with the name \"{0}\" returned \"{1}\"", new Object[] {name, result});

	    return result;
	}
	
	@Override
	public List<PostgraduateCourse> findByQuery(String query) {
		logger.log(Level.FINE, "Finding undergraduate course whose undergraduate course contain \"{0}\"...", query);
		
		// Constructs the query over the Department class.
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<PostgraduateCourse> cq = cb.createQuery(PostgraduateCourse.class);
		Root<PostgraduateCourse> root = cq.from(PostgraduateCourse.class);
		
		// Filters the query
		query = "%" + query + "%";
		cq.where(cb.like(root.get(PostgraduateCourse_.name), query));
	    List<PostgraduateCourse> result = entityManager.createQuery(cq).getResultList();
	    
	    logger.log(Level.INFO, "Found {0} undergraduate course whose name contains \"{1}\".", new Object[] { result.size(), query });

	    return result;
	}

}