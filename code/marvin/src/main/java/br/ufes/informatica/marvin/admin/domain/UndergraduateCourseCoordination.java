package br.ufes.informatica.marvin.admin.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * The undergraduate course coordination class for the MACS (Marvin Access Control System) module of Marvin.
 *
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
 * @version 1.0
*/
@Entity
public class UndergraduateCourseCoordination extends PersistentObjectSupport implements Comparable<UndergraduateCourseCoordination> {

	/**============================UNDERGRADUATE COURSE COORDINATION INFORMATION=================================================*/

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The timestamp of the moment the academic started to work on the undergraduate course coordination */
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private Date beginDate;
	
	/** Getter for beginDate. */
	public Date getBeginDate() {
	  return beginDate;
	}
	
	/** Setter for beginDate. */
	public void setBeginDate(Date beginDate) {
	  this.beginDate = beginDate;
	}
	
	/** The timestamp of the moment the academic ended the work on the undergraduate course coordination */
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	
	/** Getter for endDate. */
	public Date getEndDate() {
	  return endDate;
	}
	
	/** Setter for endDate. */
	public void setEndDate(Date endDate) {
	  this.endDate = endDate;
	}
	
	/** If the academic is the undergraduate course coordinator(true) or subcoordinator(false). */
	@NotNull
	private boolean isCoordinator;
	
	/** Getter for isCoordinator. */
	public boolean getIsCoordinator() {
		return isCoordinator;
	}
	
	/** Setter for isCoordinator. */
	public void setIsCoordinator(boolean isCoordinator) {
	  this.isCoordinator = isCoordinator;
	}
	
	/** Compare function for undergraduate course coordinator. */
	@Override
	public int compareTo(UndergraduateCourseCoordination undergraduateCourseCoordination) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**========================================================ACADEMIC========================================================*/

	/** Undergraduate course coordinator or subcoordinator (Academic with Professor role) for this undergraduate course coordination. */
	@NotNull
	@ManyToOne
	private Academic academic;
	
	/** Getter for academic. */
	public Academic getAcademic() {
		return academic;
	}
 	
 	/** Setter for academic. */
 	public void setAcademic(Academic academic) {
 		this.academic = academic;
 	}
	
	/**========================================================UNDERGRADUATE COURSE========================================================*/

	/** Related undergraduate course for this undergraduate course coordination.*/
 	@NotNull
 	@ManyToOne
 	private UndergraduateCourse undergraduateCourse;
 	
 	/** Getter for undergraduate course. */
 	public UndergraduateCourse getUndergraduateCourse() {
 		return undergraduateCourse;
 	}
 	
 	/** Setter for undergraduate course. */
 	public void setUndergraduateCourse(UndergraduateCourse undergraduateCourse) {
 		this.undergraduateCourse = undergraduateCourse;
 	}

}
