package br.ufes.informatica.marvin.admin.persistence;

import java.util.List;
import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourse;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourseEnrollment;

/**
 * Interface for a DAO for objects of the Undergraduate Course Enrollment domain class.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation definitions are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any) are
 * specified in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Local
public interface UndergraduateCourseEnrollmentDAO extends BaseDAO<UndergraduateCourseEnrollment> {
	
	/**
	   * TODO: document this method.
	   * 
	   * @param academic
	   * @return List<UndergraduateCourseEnrollment>
	*/
	List<UndergraduateCourseEnrollment> retrieveByAcademic(Academic academic);
	
	/**
	   * TODO: document this method.
	   * 
	   * @param undergraduateCourse
	   * @return List<UndergraduateCourseEnrollment>
	*/
	List<UndergraduateCourseEnrollment> retrieveByUndergraduateCourse(UndergraduateCourse undergraduateCourse);
	
	/**
	   * TODO: document this method.
	   * 
	   * @param academic
	   * @return true or false
	*/
	boolean academicIsActiveUndergraduateCourseEnrollment(Academic academic);
}
