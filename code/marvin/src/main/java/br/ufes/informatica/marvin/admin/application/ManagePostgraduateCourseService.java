package br.ufes.informatica.marvin.admin.application;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.admin.domain.PostgraduateCourse;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgram;

/**
 * Local EJB interface for the session information component. This bean provides an authentication
 * method for the controller, identifying users of the system.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Local
public interface ManagePostgraduateCourseService extends CrudService<PostgraduateCourse> {
	
	/**
	 * TODO: document this method.
	 * 
	 * @return
	*/
	PersistentObjectConverterFromId<PostgraduateProgram> getPostgraduateProgramConverter();
	
	/**
	 * Obtains the PostgraduateProgram by typing the PostgraduateProgram name.
	 * 
	 * @param name.
	 * @return The PostgraduateProgram object that match the current input name.
	*/
	List<PostgraduateProgram> findPostgraduateProgramByName(String name);
}