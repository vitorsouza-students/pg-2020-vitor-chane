package br.ufes.informatica.marvin.admin.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;

/**
 * The PostgraduateCourse class for the MACS (Marvin Access Control System) module of Marvin.
 *
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
 * @version 1.0
*/
@Entity
public class PostgraduateCourse extends PersistentObjectSupport implements Comparable <PostgraduateCourse> {

	/**========================================================PostgraduateCourse============================================================*/

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The PostgraduateCourse name. */
	@NotNull
	@Size(max = 150)
	private String name;
	
	
	/** Getter for PostgraduateCourse name. */
	public String getName() {
		return name;
	}
	
	/** Setter for PostgraduateCourse name. */
	public void setName(String name) {
		this.name = name;
	}
	
	/** The PostgraduateCourse code. */
	@NotNull
	private int code;
	
	/** Getter for PostgraduateCourse code. */
	public int getCode() {
		return code;
	}
	
	/** Setter for PostgraduateCourse code. */
	public void setCode(int code) {
		this.code = code;
	}
	
	/** The PostgraduateCourse level. */
	@NotNull
	private int level;
	
	/** Getter for PostgraduateCourse level. */
	public int getLevel() {
		return level;
	}
	
	/** Setter for PostgraduateCourse level. */
	public void setLevel(int level) {
		this.level = level;
	}
	
	/** The PostgraduateCourse shift. */
	@NotNull
	@Size(max = 100)
	private String shift;
	
	/** Getter for PostgraduateCourse shift. */
	public String getShift() {
		return shift;
	}
	
	/** Setter for PostgraduateCourse shift. */
	public void setShift(String shift) {
		this.shift = shift;
	}
	
	
	/** Compare function for PostgraduateCourse. */
	@Override
	public int compareTo(PostgraduateCourse postgraduateCourse) {
		
		// Compare the PostgraduateCourse's name
		if (name == null) {
			return 1;
		}
		if (postgraduateCourse.name == null) {
			return -1;
		}
		int cmp = name.compareTo(postgraduateCourse.name);
		if (cmp != 0) {
			return cmp;
		}
		
		// If it's the same name, check if it's the same entity.
		return uuid.compareTo(postgraduateCourse.uuid);
	}
	
/**========================================================PostgraduateCourse PostgraduateProgram==================================================*/
	
	/** Related faculty for this PostgraduateCourse.*/
	@NotNull
	@ManyToOne
	private PostgraduateProgram postgraduateCourse;

	/** Getter for the PostgraduateCourse related faculty.*/
	public PostgraduateProgram getPostgraduateProgram() {
		return postgraduateCourse;
	}
	
	/** Setter for the PostgraduateCourse related faculty.*/
 	public void setPostgraduateProgram(PostgraduateProgram postgraduateCourse) {
 		this.postgraduateCourse = postgraduateCourse;
	}
}
