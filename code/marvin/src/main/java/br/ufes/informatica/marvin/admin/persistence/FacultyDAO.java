package br.ufes.informatica.marvin.admin.persistence;

import java.util.List;
import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.PersistentObjectNotFoundException;
import br.ufes.informatica.marvin.admin.domain.Faculty;

/**
 * Interface for a DAO for objects of the Faculty domain class.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation definitions are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any) are
 * specified in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Local
public interface FacultyDAO extends BaseDAO<Faculty> {
	
	/**
	   * TODO: document this method.
	   * 
	   * @param name
	   * @return Faculty
	*/
	Faculty retrieveByName(String name) throws PersistentObjectNotFoundException, MultiplePersistentObjectsFoundException;
	
	/**
	   * TODO: document this method.
	   * 
	   * @param name
	   * @return List<Faculty>
	*/
	List<Faculty> findByName(String name);
}
