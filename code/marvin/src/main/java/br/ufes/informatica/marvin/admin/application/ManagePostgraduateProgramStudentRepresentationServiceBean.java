package br.ufes.informatica.marvin.admin.application;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.core.persistence.AcademicDAO;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgram;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgramStudentRepresentation;
import br.ufes.informatica.marvin.admin.persistence.PostgraduateProgramDAO;
import br.ufes.informatica.marvin.admin.persistence.PostgraduateProgramStudentRepresentationDAO;

/**
 * Stateless session bean implementing the ManagePostgraduateProgramStudentRepresentationService.
 * See the implemented interface documentation for details.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
@RolesAllowed({"SysAdmin", "Professor", "Staff"})
public class ManagePostgraduateProgramStudentRepresentationServiceBean extends CrudServiceBean<PostgraduateProgramStudentRepresentation> implements ManagePostgraduateProgramStudentRepresentationService {

	/**=====================================PostgraduateProgramStudentRepresentation=====================================*/

	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** The PostgraduateProgramStudentRepresentationDAO is used to save the PostgraduateProgramStudentRepresentation that is registered by the SysAdmin. */
	@EJB
	private PostgraduateProgramStudentRepresentationDAO postgraduateProgramStudentRepresentationDAO;
	
	/** Getter for PostgraduateProgramStudentRepresentationDAO. */
	@Override
	public BaseDAO<PostgraduateProgramStudentRepresentation> getDAO() {
		return postgraduateProgramStudentRepresentationDAO;
	}
	
	@Override
	public List<PostgraduateProgramStudentRepresentation> returnPostgraduateProgramStudentRepresentationByAcademic(Academic academic) {
		return postgraduateProgramStudentRepresentationDAO.retrieveByAcademic(academic);
	}
	
	@Override
	public List<PostgraduateProgramStudentRepresentation> returnPostgraduateProgramStudentRepresentationByPostgraduateProgram(PostgraduateProgram postgraduateProgram){
		return postgraduateProgramStudentRepresentationDAO.retrieveByPostgraduateProgram(postgraduateProgram);
	}
	
	public void saveEntity(PostgraduateProgramStudentRepresentation entity) {
		// Validates the entity before persisting.
		entity = validate(entity, null);
		
		// Save the entity.
		postgraduateProgramStudentRepresentationDAO.save(entity);
	}
	
	/**========================================================ACADEMIC========================================================*/

	@EJB
	private AcademicDAO academicDAO;
	
	private PersistentObjectConverterFromId<Academic> academicConverter;
	
	@Override
	public PersistentObjectConverterFromId<Academic> getAcademicConverter() {
		if(academicConverter == null) {
			academicConverter = new PersistentObjectConverterFromId<Academic>(academicDAO);
		}
		return academicConverter;
	}
	
	@Override
	public List<Academic> findAcademicByQuery(String query) {
		return academicDAO.findByNameEmailOrCpf(query);
	}
	
	/**===========================================PostgraduateProgram===========================================*/

	@EJB
	private PostgraduateProgramDAO postgraduateProgramDAO;
	
	private PersistentObjectConverterFromId<PostgraduateProgram> postgraduateProgramConverter;
	
	@Override
	public PersistentObjectConverterFromId<PostgraduateProgram> getPostgraduateProgramConverter() {
		if(postgraduateProgramConverter == null) {
			postgraduateProgramConverter = new PersistentObjectConverterFromId<PostgraduateProgram>(postgraduateProgramDAO);
		}
		return postgraduateProgramConverter;
	}

	@Override
	public List<PostgraduateProgram> findPostgraduateProgramByQuery(String query) {
		return postgraduateProgramDAO.findByQuery(query);
	}
}
