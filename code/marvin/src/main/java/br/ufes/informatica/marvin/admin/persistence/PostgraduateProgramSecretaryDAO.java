package br.ufes.informatica.marvin.admin.persistence;

import java.util.List;
import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgram;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgramSecretary;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * Interface for a DAO for objects of the PostgraduateProgramSecretary domain class.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation definitions are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any) are
 * specified in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Local
public interface PostgraduateProgramSecretaryDAO extends BaseDAO<PostgraduateProgramSecretary> {
	
	/**
	   * TODO: document this method.
	   * 
	   * @param academic
	   * @return List<PostgraduateProgramSecretary>
	*/
	List<PostgraduateProgramSecretary> retrieveByAcademic(Academic academic);
	
	/**
	   * TODO: document this method.
	   * 
	   * @param undergraduateCourse
	   * @return List<PostgraduateProgramSecretary>
	*/
	List<PostgraduateProgramSecretary> retrieveByPostgraduateProgram(PostgraduateProgram postgraduateProgram);

	boolean academicIsActivePostgraduateProgramSecretary(Academic academic);

}