package br.ufes.informatica.marvin.admin.controller;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.application.filters.LikeFilter;
import br.ufes.inf.nemo.jbutler.ejb.controller.CrudController;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.admin.application.ManagePostgraduateCourseService;
import br.ufes.informatica.marvin.admin.domain.PostgraduateCourse;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgram;

@Named
@SessionScoped
public class ManagePostgraduateCourseController extends CrudController<PostgraduateCourse>{

	/**=============================================PostgraduateCourse======================================================*/

	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** The "Manage PostgraduateCourse" service. */
	@EJB
	private ManagePostgraduateCourseService managePostgraduateCourseService;
	
	/** Getter for "Manage PostgraduateCourse" service. */
	@Override
	protected CrudService<PostgraduateCourse> getCrudService() {
		return managePostgraduateCourseService;
	}
	
	@Override
	protected void initFilters() {
		addFilter(new LikeFilter("managePostgraduateCourse.filter.byName", "name", "Filter PostgraduateCourse by name"));
		addFilter(new LikeFilter("managePostgraduateCourse.filter.byCode", "code", "Filter PostgraduateCourse by code"));
		addFilter(new LikeFilter("managePostgraduateCourse.filter.byShift", "shift", "Filter PostgraduateCourse by shift"));
		addFilter(new LikeFilter("managePostgraduateCourse.filter.byLevel", "level", "Filter PostgraduateCourse by level"));
	}
	
	/**=====================================================PostgraduateProgram=====================================================*/
	
	public PersistentObjectConverterFromId<PostgraduateProgram> getPostgraduateProgramConverter() {
		return managePostgraduateCourseService.getPostgraduateProgramConverter();
	}
	
	public PostgraduateProgram getSelectedPostgraduateProgram() {
		PostgraduateProgram postgraduateProgram = selectedEntity.getPostgraduateProgram();
		
		return (postgraduateProgram == null) ? new PostgraduateProgram() : postgraduateProgram;
	}
	
	public void setSelectedPostgraduateProgram(PostgraduateProgram selectedPostgraduateProgram) {
		selectedEntity.setPostgraduateProgram((selectedPostgraduateProgram == null) ? new PostgraduateProgram() : selectedPostgraduateProgram);
	}
	
	public List<PostgraduateProgram> completePostgraduateProgram(String query) {
		return managePostgraduateCourseService.findPostgraduateProgramByName(query);
	}
}