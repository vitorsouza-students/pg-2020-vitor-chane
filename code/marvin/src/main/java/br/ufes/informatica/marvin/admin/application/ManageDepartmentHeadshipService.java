package br.ufes.informatica.marvin.admin.application;

import java.util.List;
import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.admin.domain.Department;
import br.ufes.informatica.marvin.admin.domain.DepartmentHeadship;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * Local EJB interface for the session information component. This bean provides an authentication
 * method for the controller, identifying users of the system.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Local
public interface ManageDepartmentHeadshipService extends CrudService<DepartmentHeadship> {
	
	/**
	 * TODO: document this method.
	 * 
	 * @return
	*/
	void saveEntity(DepartmentHeadship entity);
	
	/**
	 * TODO: document this method.
	 * 
	 * @return
	*/
	boolean checkIfAcademicIsDepartmentBoss(Academic academic);
	
	/**
	 * TODO: document this method.
	 * 
	 * @return
	*/
	List<DepartmentHeadship> returnDepartmentHeadshipByAcademic(Academic academic);
	
	/**
	 * TODO: document this method.
	 * 
	 * @return
	*/
	List<DepartmentHeadship> returnDepartmentHeadshipByDepartment(Department department);
	
	/**
	 * TODO: document this method.
	 * 
	 * @return
	*/
	PersistentObjectConverterFromId<Academic> getAcademicConverter();
	
	/**
	 * TODO: document this method.
	 * 
	 * @return
	*/
	PersistentObjectConverterFromId<Department> getDepartmentConverter();
	
	/**
	 * TODO: document this method.
	 * 
	 * @return
	*/
	List<Academic> findAcademicByQuery(String query);
	
	/**
	 * TODO: document this method.
	 * 
	 * @return
	*/
	List<Department> findDepartmentByName(String name);
}
