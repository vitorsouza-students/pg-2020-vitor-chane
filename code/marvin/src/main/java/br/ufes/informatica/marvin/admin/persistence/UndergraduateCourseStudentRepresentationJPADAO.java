package br.ufes.informatica.marvin.admin.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourseStudentRepresentation_;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourse;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourseStudentRepresentation;

/**
 * Stateless session bean implementing a DAO for objects of the undergraduate course student representation domain class using JPA2.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation implementations are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any is defined
 * in the implementing DAO interface) have to be implemented in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
public class UndergraduateCourseStudentRepresentationJPADAO extends BaseJPADAO<UndergraduateCourseStudentRepresentation> implements UndergraduateCourseStudentRepresentationDAO {
	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(UndergraduateCourseStudentRepresentationJPADAO.class.getCanonicalName());

	/** The application's persistent context provided by the application server. */
	@PersistenceContext
	private EntityManager entityManager;
	
	/** Getter for Entity Manager. */
	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	@Override
	public List<UndergraduateCourseStudentRepresentation> retrieveByAcademic(Academic academic) {
		logger.log(Level.FINE, "Retrieving all Undergraduate Course Student Representation instances whose academic is \"{0}\"...", academic);
		
	    // Constructs the query over the UndergraduateCourseStudentRepresentation class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<UndergraduateCourseStudentRepresentation> cq = cb.createQuery(UndergraduateCourseStudentRepresentation.class);
	    Root<UndergraduateCourseStudentRepresentation> root = cq.from(UndergraduateCourseStudentRepresentation.class);
	
	    // Filters the query with the academic.
	    cq.where(cb.equal(root.get(UndergraduateCourseStudentRepresentation_.academic), academic));
	    List<UndergraduateCourseStudentRepresentation> result = entityManager.createQuery(cq).getResultList();
	    logger.log(Level.INFO, "Retrieving all Undergraduate Course Student Representation instances by the academic \"{0}\" returned {1} results", new Object[] {academic, result.size()});
	    
	    return result;
	}

	@Override
	public List<UndergraduateCourseStudentRepresentation> retrieveByUndergraduateCourse(UndergraduateCourse undergraduateCourse) {
		logger.log(Level.FINE, "Retrieving all Undergraduate Course Student Representation instances whose department is \"{0}\"...", undergraduateCourse);
		
		// Constructs the query over the UndergraduateCourseStudentRepresentation class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<UndergraduateCourseStudentRepresentation> cq = cb.createQuery(UndergraduateCourseStudentRepresentation.class);
	    Root<UndergraduateCourseStudentRepresentation> root = cq.from(UndergraduateCourseStudentRepresentation.class);
	    
	    // Filters the query with the UndergraduateCourse.
	    cq.where(cb.equal(root.get(UndergraduateCourseStudentRepresentation_.undergraduateCourse), undergraduateCourse));
	    List<UndergraduateCourseStudentRepresentation> result = entityManager.createQuery(cq).getResultList();
	    
	    logger.log(Level.INFO, "Retrieving all Undergraduate Course Student Representation instances by the department \"{0}\" returned {1} results", new Object[] {undergraduateCourse, result.size()});
	    
	    return result;
	}

	@Override
	public boolean academicIsActiveUndergraduateCourseStudentRepresentation(Academic academic) {
		logger.log(Level.FINE, "Checking if the Undergraduate Course has active secretary");
		
		// Constructs the query over the UndergraduateCourseStudentRepresentation class.
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<UndergraduateCourseStudentRepresentation> cq = cb.createQuery(UndergraduateCourseStudentRepresentation.class);
		Root<UndergraduateCourseStudentRepresentation> root = cq.from(UndergraduateCourseStudentRepresentation.class);
		
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(cb.equal(root.get(UndergraduateCourseStudentRepresentation_.academic), academic));
		predicates.add(cb.isNull(root.get(UndergraduateCourseStudentRepresentation_.endDate)));
		
		// Filters the query with the academic.
		cq.select(root).where(predicates.toArray(new Predicate[] {}));
		List<UndergraduateCourseStudentRepresentation> result = entityManager.createQuery(cq).getResultList();
		
		if (!result.isEmpty()) {
			return true;
		}
		
		return false;
	}
}
