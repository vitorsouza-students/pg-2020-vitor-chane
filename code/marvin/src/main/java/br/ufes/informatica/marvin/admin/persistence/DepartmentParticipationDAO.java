package br.ufes.informatica.marvin.admin.persistence;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.informatica.marvin.admin.domain.Department;
import br.ufes.informatica.marvin.admin.domain.DepartmentParticipation;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * Interface for a DAO for objects of the DepartmentParticipation domain class.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation definitions are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any) are
 * specified in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Local
public interface DepartmentParticipationDAO extends BaseDAO<DepartmentParticipation> {

	/**
	   * TODO: document this method.
	   * 
	   * @param academic
	   * @return List<DepartmentParticipation>
	*/
	List<DepartmentParticipation> retrieveByAcademic(Academic academic);
	
	/**
	   * TODO: document this method.
	   * 
	   * @param department
	   * @return List<DepartmentParticipation>
	*/
	List<DepartmentParticipation> retrieveByDepartment(Department department);
	
	/**
	   * TODO: document this method.
	   * 
	   * @param academic
	   * @return true or false
	*/
	boolean academicHasActiveDepartmentParticipation(Academic academic);
}
