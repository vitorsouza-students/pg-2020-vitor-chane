package br.ufes.informatica.marvin.admin.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;
import br.ufes.informatica.marvin.admin.domain.DepartmentHeadship_;
import br.ufes.informatica.marvin.admin.domain.Department;
import br.ufes.informatica.marvin.admin.domain.DepartmentHeadship;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * Stateless session bean implementing a DAO for objects of the DepartmentHeadship domain class using JPA2.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation implementations are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any is defined
 * in the implementing DAO interface) have to be implemented in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
public class DepartmentHeadshipJPADAO extends BaseJPADAO<DepartmentHeadship> implements DepartmentHeadshipDAO {
	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(DepartmentHeadshipJPADAO.class.getCanonicalName());
	
	/** The application's persistent context provided by the application server. */
	@PersistenceContext
	private EntityManager entityManager;

	/** Getter for Entity Manager. */
	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	public List<DepartmentHeadship> retrieveByAcademic(Academic academic) {
		logger.log(Level.FINE, "Retrieving all department headship instances whose academic is \"{0}\"...", academic);
		
	    // Constructs the query over the DepartmentHeadship class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<DepartmentHeadship> cq = cb.createQuery(DepartmentHeadship.class);
	    Root<DepartmentHeadship> root = cq.from(DepartmentHeadship.class);
	
	    // Filters the query with the academic.
	    cq.where(cb.equal(root.get(DepartmentHeadship_.academic), academic));
	    List<DepartmentHeadship> result = entityManager.createQuery(cq).getResultList();
	    logger.log(Level.INFO, "Retrieving all department headship instances by the academic \"{0}\" returned {1} results", new Object[] {academic, result.size()});
	    
	    return result;
	}
	
	public List<DepartmentHeadship> retrieveByDepartment(Department department) {
		logger.log(Level.FINE, "Retrieving all department headship instances whose department is \"{0}\"...", department);
		
		// Constructs the query over the DepartmentHeadship class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<DepartmentHeadship> cq = cb.createQuery(DepartmentHeadship.class);
	    Root<DepartmentHeadship> root = cq.from(DepartmentHeadship.class);
	    
	    // Filters the query with the department.
	    cq.where(cb.equal(root.get(DepartmentHeadship_.department), department));
	    List<DepartmentHeadship> result = entityManager.createQuery(cq).getResultList();
	    
	    logger.log(Level.INFO, "Retrieving all department headship instances by the department \"{0}\" returned {1} results", new Object[] {department, result.size()});
	    
	    return result;
	}
	
	public List<DepartmentHeadship> retrieveBySpecificRole(DepartmentHeadship specificRole) {
		logger.log(Level.FINE, "Retrieving all department headship instances whose specific role is \"{0}\"...", specificRole);
		
		// Constructs the query over the DepartmentHeadship class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<DepartmentHeadship> cq = cb.createQuery(DepartmentHeadship.class);
	    Root<DepartmentHeadship> root = cq.from(DepartmentHeadship.class);
	    
	    // Filters the query with the department.
	    cq.where(cb.equal(root.get(DepartmentHeadship_.isBoss), specificRole));
	    List<DepartmentHeadship> result = entityManager.createQuery(cq).getResultList();
	    
	    logger.log(Level.INFO, "Retrieving all department headship instances by the specific role \"{0}\" returned {1} results", new Object[] {specificRole, result.size()});
	    
	    return result;
	}
	
	@Override
	public boolean academicIsActiveDepartmentBoss(Academic academic) {
		logger.log(Level.FINE, "Checking if the department has active boss");
		
		// Constructs the query over the Department Headship class.
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<DepartmentHeadship> cq = cb.createQuery(DepartmentHeadship.class);
		Root<DepartmentHeadship> root = cq.from(DepartmentHeadship.class);
		
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(cb.equal(root.get(DepartmentHeadship_.academic), academic));
		predicates.add(cb.isNull(root.get(DepartmentHeadship_.endDate)));
		
		// Filters the query with the academic.
		cq.select(root).where(predicates.toArray(new Predicate[] {}));
		List<DepartmentHeadship> result = entityManager.createQuery(cq).getResultList();
		
		if (!result.isEmpty()) {
			return true;
		}
		
		return false;
	}
}
