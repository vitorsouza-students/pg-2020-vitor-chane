package br.ufes.informatica.marvin.admin.application;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudException;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.PersistentObjectNotFoundException;
import br.ufes.informatica.marvin.admin.domain.Faculty;
import br.ufes.informatica.marvin.admin.persistence.FacultyDAO;

/**
 * Stateless session bean implementing the faculty service. See the implemented interface
 * documentation for details.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
@RolesAllowed("SysAdmin")
public class ManageFacultyServiceBean extends CrudServiceBean<Faculty> implements ManageFacultyService {
	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(ManageFacultyServiceBean.class.getCanonicalName());
	
	/** The Faculty DAO is used to save the faculty that is registered by the SysAdmin. */
	@EJB
	private FacultyDAO facultyDAO;
	
	/** Getter for faculty DAO. */
	@Override
	public BaseDAO<Faculty> getDAO() {
		return facultyDAO;
	}
	
	/** 
	 * Check if there is already a faculty with the same name. If exists throw a exception.
	 * 
	 * @param faculty
	 * @throws CrudException
	*/
	@Override
	public void validateCreate(Faculty faculty) throws CrudException {
		
		// Checks if the chosen name is already in use.
		try {
			facultyDAO.retrieveByName(faculty.getName());
			logger.log(Level.WARNING, "New faculty violates unique name rule: \"{0}\"", faculty.getName());
			throw new CrudException(null, "New faculty violates unique name rule!", null);
		}
		catch (PersistentObjectNotFoundException e) {
			// This is the expected outcome. Just log that everything is OK.
			logger.log(Level.INFO, "New faculty satisfies unique name rule: \"{0}\"", faculty.getName());
		}
		catch (MultiplePersistentObjectsFoundException e) {
			// This is a severe problem: the unique constraint has already been violated.
			logger.log(Level.SEVERE, "There are already faculties with the name: \"{0}\"!", faculty.getName());
			throw new EJBException(e);
		}
	}
	
	/** 
	 * Check if there is already a faculty with the same name. If exists throw a exception.
	 * 
	 * @param faculty
	 * @throws CrudException
	*/
	@Override
	public void validateUpdate(Faculty faculty) throws CrudException {
		
		// Checks if the chosen name is already in use.
		try {
			facultyDAO.retrieveByName(faculty.getName());
			logger.log(Level.WARNING, "New faculty violates unique name rule: \"{0}\"", faculty.getName());
			throw new CrudException(null, "New faculty violates unique name rule!", null);
		}
		catch (PersistentObjectNotFoundException e) {
			// This is the expected outcome. Just log that everything is OK.
			logger.log(Level.INFO, "New faculty satisfies unique name rule: \"{0}\"", faculty.getName());
		}
		catch (MultiplePersistentObjectsFoundException e) {
			// This is a severe problem: the unique constraint has already been violated.
			logger.log(Level.SEVERE, "There are already faculties with the name: \"{0}\"!", faculty.getName());
			throw new EJBException(e);
		}
	}
}
