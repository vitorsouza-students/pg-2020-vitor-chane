package br.ufes.informatica.marvin.admin.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * The department participation class for the MACS (Marvin Access Control System) module of Marvin.
 *
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
 * @version 1.0
*/

@Entity
public class DepartmentParticipation extends PersistentObjectSupport implements Comparable <DepartmentParticipation> {
	
	/**============================================DEPARTMENT PARTICIPATION INFORMATION===============================================*/
	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The timestamp of the moment the academic started to participate on the department. */
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private Date beginDate;
	
	/** Getter for beginDate. */
	public Date getBeginDate() {
	  return beginDate;
	}
	
	/** Setter for beginDate. */
	public void setBeginDate(Date beginDate) {
	  this.beginDate = beginDate;
	}
	
	/** The timestamp of the moment the academic finished his participation on the department */
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	
	/** Getter for endDate. */
	public Date getEndDate() {
	  return endDate;
	}
	
	/** Setter for endDate. */
	public void setEndDate(Date endDate) {
	  this.endDate = endDate;
	}
	
	/** The specific role of the academic on the department participation. */
	private String specificRole;
	
	/** Getter for specificRole. */
	public String getSpecificRole() {
		return specificRole;
	}

	/** Setter for specificRole. */
	public void setSpecificRole(String specificRole) {
		this.specificRole = specificRole;
	}
	
	/** In case the academic is disconnected from the department*/
	private String disconnectionReason;
	
	/** Getter for disconnection reason. */
	public String getDisconnectionReason() {
		return disconnectionReason;
	}

	/** Setter for disconnection reason. */
	public void setDisconnectionReason(String disconnectionReason) {
		this.disconnectionReason = disconnectionReason;
	}
	
	/** Compare function for department participation. */
	@Override
	public int compareTo(DepartmentParticipation o) {
 		return 0;
 	}
	
	/**========================================================ACADEMIC========================================================*/
	
	/** Academic participating on this department. */
	@NotNull
	@ManyToOne
	private Academic academic;
	
	/** Getter for academic. */
	public Academic getAcademic() {
		return academic;
	}
 	
 	/** Setter for academic. */
 	public void setAcademic(Academic academic) {
 		this.academic = academic;
 	}
	
	/**========================================================DEPARTMENT========================================================*/

	/** Department for this department participation. */
	@NotNull
	@ManyToOne
	private Department department;
	
	/** Getter for department. */
	public Department getDepartment() {
	  return department;
	}
	
	/** Setter for department. */
 	public void setDepartment(Department department) {
	  this.department = department;
	}
 	
}
