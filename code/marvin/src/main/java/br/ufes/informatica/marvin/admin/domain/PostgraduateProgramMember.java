package br.ufes.informatica.marvin.admin.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * The PostgraduateProgramMember class for the MACS (Marvin Access Control System) module of Marvin.
 *
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
 * @version 1.0
*/
@Entity
public class PostgraduateProgramMember extends PersistentObjectSupport implements Comparable<PostgraduateProgramMember> {

	/**============================PostgraduateProgramMember INFORMATION=================================================*/

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The timestamp of the moment the academic started to work on the PostgraduateProgramMember */
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private Date beginDate;
	
	/** Getter for beginDate. */
	public Date getBeginDate() {
	  return beginDate;
	}
	
	/** Setter for beginDate. */
	public void setBeginDate(Date beginDate) {
	  this.beginDate = beginDate;
	}
	
	/** The timestamp of the moment the academic ended the work on the PostgraduateProgramMember */
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	
	/** Getter for endDate. */
	public Date getEndDate() {
	  return endDate;
	}
	
	/** Setter for endDate. */
	public void setEndDate(Date endDate) {
	  this.endDate = endDate;
	}
	
	private String bondType;
	
	public String getBondType() {
		return bondType;
	}
	
	public void setgetBondType(String bondType) {
	  this.bondType = bondType;
	}

	@Override
	public int compareTo(PostgraduateProgramMember postgraduateProgramCollegiateMember) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**========================================================ACADEMIC========================================================*/

	/** Academic with Professor or Staff role for this PostgraduateProgramMember. */
	@NotNull
	@ManyToOne
	private Academic academic;
	
	/** Getter for academic. */
	public Academic getAcademic() {
		return academic;
	}
 	
 	/** Setter for academic. */
 	public void setAcademic(Academic academic) {
 		this.academic = academic;
 	}
	
	/**========================================================PostgraduateProgram========================================================*/

	/** Related PostgraduateProgram for this PostgraduateProgramMember.*/
 	@NotNull
 	@ManyToOne
 	private PostgraduateProgram postgraduateProgram;
 	
 	/** Getter for PostgraduateProgram. */
 	public PostgraduateProgram getPostgraduateProgram() {
 		return postgraduateProgram;
 	}
 	
 	/** Setter for PostgraduateProgram. */
 	public void setPostgraduateProgram(PostgraduateProgram postgraduateProgram) {
 		this.postgraduateProgram = postgraduateProgram;
 	}
}
