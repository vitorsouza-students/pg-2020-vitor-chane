package br.ufes.informatica.marvin.admin.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgramSecretary_;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgram;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgramSecretary;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * Stateless session bean implementing a DAO for objects of the PostgraduateProgramSecretary domain class using JPA2.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation implementations are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any is defined
 * in the implementing DAO interface) have to be implemented in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
public class PostgraduateProgramSecretaryJPADAO extends BaseJPADAO<PostgraduateProgramSecretary> implements PostgraduateProgramSecretaryDAO {
	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(PostgraduateProgramSecretaryJPADAO.class.getCanonicalName());

	/** The application's persistent context provided by the application server. */
	@PersistenceContext
	private EntityManager entityManager;

	/** Getter for Entity Manager. */
	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	@Override
	public List<PostgraduateProgramSecretary> retrieveByAcademic(Academic academic) {
		logger.log(Level.FINE, "Retrieving all PostgraduateProgramSecretary instances whose academic is \"{0}\"...", academic);
		
	    // Constructs the query over the PostgraduateProgramSecretary class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<PostgraduateProgramSecretary> cq = cb.createQuery(PostgraduateProgramSecretary.class);
	    Root<PostgraduateProgramSecretary> root = cq.from(PostgraduateProgramSecretary.class);
	
	    // Filters the query with the academic.
	    cq.where(cb.equal(root.get(PostgraduateProgramSecretary_.academic), academic));
	    List<PostgraduateProgramSecretary> result = entityManager.createQuery(cq).getResultList();
	    logger.log(Level.INFO, "Retrieving all PostgraduateProgramSecretarys instances by the academic \"{0}\" returned {1} results", new Object[] {academic, result.size()});
	    
	    return result;
	}

	@Override
	public List<PostgraduateProgramSecretary> retrieveByPostgraduateProgram(PostgraduateProgram postgraduateProgram) {
		logger.log(Level.FINE, "Retrieving all Undergraduate Course Collegiate Members instances whose department is \"{0}\"...", postgraduateProgram);
		
		// Constructs the query over the PostgraduateProgramSecretary class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<PostgraduateProgramSecretary> cq = cb.createQuery(PostgraduateProgramSecretary.class);
	    Root<PostgraduateProgramSecretary> root = cq.from(PostgraduateProgramSecretary.class);
	    
	    // Filters the query with the PostgraduateProgram.
	    cq.where(cb.equal(root.get(PostgraduateProgramSecretary_.postgraduateProgram), postgraduateProgram));
	    List<PostgraduateProgramSecretary> result = entityManager.createQuery(cq).getResultList();
	    
	    logger.log(Level.INFO, "Retrieving all PostgraduateProgramSecretarys instances by the department \"{0}\" returned {1} results", new Object[] {postgraduateProgram, result.size()});
	    
	    return result;
	}

	@Override
	public boolean academicIsActivePostgraduateProgramSecretary(Academic academic) {
		logger.log(Level.FINE, "Checking if the selected academic is a PostgraduateProgramSecretary");

		// Constructs the query over the Department Headship class.
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<PostgraduateProgramSecretary> cq = cb.createQuery(PostgraduateProgramSecretary.class);
		Root<PostgraduateProgramSecretary> root = cq.from(PostgraduateProgramSecretary.class);
	
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(cb.equal(root.get(PostgraduateProgramSecretary_.academic), academic));
		predicates.add(cb.isNull(root.get(PostgraduateProgramSecretary_.endDate)));
	
		// Filters the query with the academic.
		cq.select(root).where(predicates.toArray(new Predicate[] {}));
		List<PostgraduateProgramSecretary> result = entityManager.createQuery(cq).getResultList();
		
		if (!result.isEmpty()) {
			return true;
		}
		
		return false;
	}

}