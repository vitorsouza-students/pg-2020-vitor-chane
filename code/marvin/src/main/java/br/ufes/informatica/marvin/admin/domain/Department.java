package br.ufes.informatica.marvin.admin.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;

/**
 * The department class for the MACS (Marvin Access Control System) module of Marvin.
 *
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
 * @version 1.0
*/
@Entity
public class Department extends PersistentObjectSupport implements Comparable<Department> {
	
	/**========================================================DEPARTMENT INFORMATION=====================================================*/
	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The department name. */
	@NotNull
	@Size(max = 150)
	private String name;
	
	/** Getter for department name. */
	public String getName() {
		return name;
	}
	
	/** Setter for department name. */
	public void setName(String name) {
		this.name = name;
	}
	
	/** Compare function for department. */
	@Override
	public int compareTo(Department department) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**========================================================DEPARTMENT FACULTY==================================================*/
	
	/** Related faculty for this department.*/
	@NotNull
	@ManyToOne
	private Faculty faculty;

	/** Getter for the department related faculty.*/
	public Faculty getFaculty() {
		return faculty;
	}
	
	/** Setter for the department related faculty.*/
 	public void setFaculty(Faculty faculty) {
 		this.faculty = faculty;
	}
}
