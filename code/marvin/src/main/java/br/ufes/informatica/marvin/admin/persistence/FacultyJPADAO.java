package br.ufes.informatica.marvin.admin.persistence;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.PersistentObjectNotFoundException;
import br.ufes.informatica.marvin.admin.domain.Faculty;
import br.ufes.informatica.marvin.admin.domain.Faculty_;

/**
 * Stateless session bean implementing a DAO for objects of the Faculty domain class using JPA2.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation implementations are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any is defined
 * in the implementing DAO interface) have to be implemented in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
public class FacultyJPADAO extends BaseJPADAO<Faculty> implements FacultyDAO {
	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(FacultyJPADAO.class.getCanonicalName());
	
	/** The application's persistent context provided by the application server. */
	@PersistenceContext
	private EntityManager entityManager;
	
	/** Getter for Entity Manager. */
	@Override
	protected EntityManager getEntityManager() {		
		return entityManager;
	}
	
	@Override
	public Faculty retrieveByName(String name) throws PersistentObjectNotFoundException, MultiplePersistentObjectsFoundException {
		
		logger.log(Level.FINE, "Retrieving the faculty whose name is \"{0}\"...", name);
		
		// Constructs the query over the Faculty class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<Faculty> cq = cb.createQuery(Faculty.class);
	    Root<Faculty> root = cq.from(Faculty.class);

	    // Filters the query
	    cq.where(cb.equal(root.get(Faculty_.name), name));
	    Faculty result = executeSingleResultQuery(cq, name);
	    logger.log(Level.INFO, "Retrieve faculty with the name \"{0}\" returned \"{1}\"", new Object[] {name, result});
	    
	    return result;
	}

	@Override
	public List<Faculty> findByName(String name) {
		
		logger.log(Level.FINE, "Finding faculties whose faculty name contain \"{0}\"...", name);

	    // Constructs the query over the Faculty class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<Faculty> cq = cb.createQuery(Faculty.class);
	    Root<Faculty> root = cq.from(Faculty.class);

	    // Filters the query
	    name = "%" + name + "%";
	    cq.where(cb.like(root.get(Faculty_.name), name));
	    List<Faculty> result = entityManager.createQuery(cq).getResultList();
	    logger.log(Level.INFO, "Found {0} faculty whose name contains \"{1}\".", new Object[] { result.size(), name });
	    
	    return result;
	}
}
