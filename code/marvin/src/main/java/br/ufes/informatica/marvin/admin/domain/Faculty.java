package br.ufes.informatica.marvin.admin.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;

/**
 * The faculty class for the MACS (Marvin Access Control System) module of Marvin.
 *
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
 * @version 1.0
*/

@Entity
public class Faculty extends PersistentObjectSupport implements Comparable <Faculty> {
	
	/**========================================================FACULTY INFORMATION============================================================*/

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The faculty name. */
	@NotNull
	@Size(max = 100)
	private String name;
	
	/** Getter for faculty name. */
	public String getName() {
		return name;
	}
	
	/** Setter for faculty name. */
	public void setName(String name) {
		this.name = name;
	}
	
	/** Compare function for faculty. */
	@Override
	public int compareTo(Faculty faculty) {
		
		// Compare the faculty's name
		if (name == null) {
			return 1;
		}
		if (faculty.name == null) {
			return -1;
		}
		int cmp = name.compareTo(faculty.name);
		if (cmp != 0) {
			return cmp;
		}
		
		// If it's the same name, check if it's the same entity.
		return uuid.compareTo(faculty.uuid);
	}
	
	/**========================================================FACULTY DEPARTMENTS=====================================================*/
	
	/** Departments for this faculty. */
	@OneToMany(mappedBy="faculty", cascade = CascadeType.ALL)
	private Set<Department> departments;
	
	/** Getter for departments. */
	public Set<Department> getDepartments() {
		return departments;
	}

	/** Setter for departments. */
	public void setDepartments(Set<Department> departments) {
		this.departments = departments;
	}
}
