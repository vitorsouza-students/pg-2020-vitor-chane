package br.ufes.informatica.marvin.admin.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourseEnrollment_;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourse;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourseEnrollment;

/**
 * Stateless session bean implementing a DAO for objects of the UndergraduateCourseEnrollment domain class using JPA2.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation implementations are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any is defined
 * in the implementing DAO interface) have to be implemented in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
public class UndergraduateCourseEnrollmentJPADAO extends BaseJPADAO<UndergraduateCourseEnrollment> implements UndergraduateCourseEnrollmentDAO {
	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(UndergraduateCourseEnrollmentJPADAO.class.getCanonicalName());

	/** The application's persistent context provided by the application server. */
	@PersistenceContext
	private EntityManager entityManager;
	
	/** Getter for Entity Manager. */
	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	@Override
	public List<UndergraduateCourseEnrollment> retrieveByAcademic(Academic academic) {
		logger.log(Level.FINE, "Retrieving all Undergraduate Course Secretaries instances whose academic is \"{0}\"...", academic);
		
	    // Constructs the query over the Undergraduate Course Coordination class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<UndergraduateCourseEnrollment> cq = cb.createQuery(UndergraduateCourseEnrollment.class);
	    Root<UndergraduateCourseEnrollment> root = cq.from(UndergraduateCourseEnrollment.class);
	
	    // Filters the query with the academic.
	    cq.where(cb.equal(root.get(UndergraduateCourseEnrollment_.academic), academic));
	    List<UndergraduateCourseEnrollment> result = entityManager.createQuery(cq).getResultList();
	    logger.log(Level.INFO, "Retrieving all Undergraduate Course Secretaries instances by the academic \"{0}\" returned {1} results", new Object[] {academic, result.size()});
	    
	    return result;
	}

	@Override
	public List<UndergraduateCourseEnrollment> retrieveByUndergraduateCourse(UndergraduateCourse undergraduateCourse) {
		logger.log(Level.FINE, "Retrieving all Undergraduate Course Secretaries instances whose department is \"{0}\"...", undergraduateCourse);
		
		// Constructs the query over the Undergraduate Course Coordination class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<UndergraduateCourseEnrollment> cq = cb.createQuery(UndergraduateCourseEnrollment.class);
	    Root<UndergraduateCourseEnrollment> root = cq.from(UndergraduateCourseEnrollment.class);
	    
	    // Filters the query with the Undergraduate Course.
	    cq.where(cb.equal(root.get(UndergraduateCourseEnrollment_.undergraduateCourse), undergraduateCourse));
	    List<UndergraduateCourseEnrollment> result = entityManager.createQuery(cq).getResultList();
	    
	    logger.log(Level.INFO, "Retrieving all Undergraduate Course Secretaries instances by the department \"{0}\" returned {1} results", new Object[] {undergraduateCourse, result.size()});
	    
	    return result;
	}

	@Override
	public boolean academicIsActiveUndergraduateCourseEnrollment(Academic academic) {
		logger.log(Level.FINE, "Checking if the Undergraduate Course Enrollment has active academics");
		
		// Constructs the query over the Undergraduate Course Enrollment class.
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<UndergraduateCourseEnrollment> cq = cb.createQuery(UndergraduateCourseEnrollment.class);
		Root<UndergraduateCourseEnrollment> root = cq.from(UndergraduateCourseEnrollment.class);
		
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(cb.equal(root.get(UndergraduateCourseEnrollment_.academic), academic));
		predicates.add(cb.isNull(root.get(UndergraduateCourseEnrollment_.endDate)));
		
		// Filters the query with the academic.
		cq.select(root).where(predicates.toArray(new Predicate[] {}));
		List<UndergraduateCourseEnrollment> result = entityManager.createQuery(cq).getResultList();
		
		if (!result.isEmpty()) {
			return true;
		}
		
		return false;
	}
}
