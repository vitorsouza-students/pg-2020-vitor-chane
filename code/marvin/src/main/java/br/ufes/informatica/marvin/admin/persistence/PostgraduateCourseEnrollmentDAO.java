package br.ufes.informatica.marvin.admin.persistence;

import java.util.List;
import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.informatica.marvin.admin.domain.PostgraduateCourse;
import br.ufes.informatica.marvin.admin.domain.PostgraduateCourseEnrollment;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * Interface for a DAO for objects of the PostgraduateCourseEnrollment domain class.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation definitions are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any) are
 * specified in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Local
public interface PostgraduateCourseEnrollmentDAO extends BaseDAO<PostgraduateCourseEnrollment> {
	
	/**
	   * TODO: document this method.
	   * 
	   * @param academic
	   * @return List<PostgraduateCourseEnrollment>
	*/
	List<PostgraduateCourseEnrollment> retrieveByAcademic(Academic academic);
	
	/**
	   * TODO: document this method.
	   * 
	   * @param undergraduateCourse
	   * @return List<PostgraduateCourseEnrollment>
	*/
	List<PostgraduateCourseEnrollment> retrieveByPostgraduateCourse(PostgraduateCourse postgraduateCourse);

}