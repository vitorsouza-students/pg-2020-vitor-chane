package br.ufes.informatica.marvin.admin.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudException;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudValidationError;
import br.ufes.inf.nemo.jbutler.ejb.application.filters.LikeFilter;
import br.ufes.inf.nemo.jbutler.ejb.controller.CrudController;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.admin.application.ManageDepartmentHeadshipService;
import br.ufes.informatica.marvin.admin.application.ManageDepartmentParticipationService;
import br.ufes.informatica.marvin.admin.application.ManageDepartmentSecretaryService;
import br.ufes.informatica.marvin.admin.application.ManageDepartmentStudentRepresentationService;
import br.ufes.informatica.marvin.admin.application.ManageDepartmentStudentRepresentationServiceBean;
import br.ufes.informatica.marvin.admin.domain.Department;
import br.ufes.informatica.marvin.admin.domain.DepartmentHeadship;
import br.ufes.informatica.marvin.admin.domain.DepartmentParticipation;
import br.ufes.informatica.marvin.admin.domain.DepartmentSecretary;
import br.ufes.informatica.marvin.admin.domain.DepartmentStudentRepresentation;
import br.ufes.informatica.marvin.core.controller.SessionController;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.core.domain.Role;

@Named
@SessionScoped
public class ManageDepartmentStudentRepresentationController extends CrudController<DepartmentStudentRepresentation> {

	/**===============================================DEPARTMENT PARTICIPATION========================================================*/
	
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(ManageDepartmentStudentRepresentationServiceBean.class.getCanonicalName());
	
	@EJB
	private ManageDepartmentStudentRepresentationService manageDepartmentStudentRepresentationService;
	
	@Override
	protected CrudService<DepartmentStudentRepresentation> getCrudService() {
		return manageDepartmentStudentRepresentationService;
	}
	
	@Override
	protected void initFilters() {
		addFilter(new LikeFilter("manageDepartmentStudentRepresentation.filter.byAcademicName", "academic.name", "Filter academic by name"));
		addFilter(new LikeFilter("manageDepartmentStudentRepresentation.filter.byDepartment", "department.name", "Filter academic by department"));
	    addFilter(new LikeFilter("manageDepartmentStudentRepresentation.filter.byBeginDate", "beginDate", "Filter academic by begin date"));
	    addFilter(new LikeFilter("manageDepartmentStudentRepresentation.filter.byEndDate", "endDate", "Filter academic by end date"));
	}
	
	/**====================================APPOINT DEPARTMENT STUDENT REPRESENTATION============================================*/

	/** Path to the folder where the view files (web pages) for Appoint Department Student Representation is. */
	private static final String VIEW_PATH = "/admin/appointDepartmentStudentRepresentation/";
	
	/** The session controller, with information of the authenticated user. */
	@Inject
	private SessionController sessionController;
	
	@EJB
	private ManageDepartmentParticipationService manageDepartmentParticipationService;
	
	@EJB
	private ManageDepartmentHeadshipService manageDepartmentHeadshipService;
	
	@EJB
	private ManageDepartmentSecretaryService manageDepartmentSecretaryService;
	
	/** The entity that will have its participation finalized. */
	protected DepartmentStudentRepresentation finishCan;
	
	/** Getter for finishCan. */
	public DepartmentStudentRepresentation getFinishCan() {
		return finishCan;
	}
	
	/** Return if the current logged user is a valid Department Boss/ Underboss/ Secretary */
	public boolean isCurrentUserValidDepartmentAdmin() {
		Academic currentUser = sessionController.getCurrentUser();
		List<DepartmentHeadship> departmentHeadship;
		List<DepartmentSecretary> departmentSecretary;
		List<DepartmentParticipation> departmentParticipation;
		
		if(sessionController.isLoggedIn()) {
			if(sessionController.isProfessor()) {
				if(manageDepartmentParticipationService.checkIfAcademicHasDepartmentParticipation(currentUser)) {
					if(manageDepartmentHeadshipService.checkIfAcademicIsDepartmentBoss(currentUser)) {
						departmentHeadship = manageDepartmentHeadshipService.returnDepartmentHeadshipByAcademic(currentUser);
						departmentParticipation = manageDepartmentParticipationService.returnDepartmentParticipationByAcademic(currentUser);
						
						// Considering that an specific academic could be Department Boss/Underboss for more than one department
						for(DepartmentHeadship entityHeadship: departmentHeadship) {
							for(DepartmentParticipation entityParticipation: departmentParticipation) {
								if(entityHeadship.getDepartment().equals(entityParticipation.getDepartment())) {
									return true;
								}
							}
						}
					}
				}
			}
			if(sessionController.isStaff()) {
				if(manageDepartmentParticipationService.checkIfAcademicHasDepartmentParticipation(currentUser)) {
					if(manageDepartmentSecretaryService.checkIfAcademicIsDepartmentSecretary(currentUser)) {
						departmentSecretary = manageDepartmentSecretaryService.returnDepartmentSecretaryByAcademic(currentUser);
						departmentParticipation = manageDepartmentParticipationService.returnDepartmentParticipationByAcademic(currentUser);
						
						// Considering that an specific academic could be Department Secretary for more than one department
						for(DepartmentSecretary entitySecretary: departmentSecretary) {
							for(DepartmentParticipation entityParticipation: departmentParticipation) {
								if(entitySecretary.getDepartment().equals(entityParticipation.getDepartment())) {
									return true;
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	/** Return the logged user department if he is a valid department boss/underboss/secretary */
	public Department getAdminDepartment() {
		
		Academic currentUser = sessionController.getCurrentUser();
		
		List<DepartmentHeadship> departmentHeadship = manageDepartmentHeadshipService.returnDepartmentHeadshipByAcademic(currentUser);
		List<DepartmentParticipation> departmentParticipation = manageDepartmentParticipationService.returnDepartmentParticipationByAcademic(currentUser);
		List<DepartmentSecretary> departmentSecretary = manageDepartmentSecretaryService.returnDepartmentSecretaryByAcademic(currentUser);
		
		if(isCurrentUserValidDepartmentAdmin()) {
			if(sessionController.isProfessor()) {
				for(DepartmentHeadship entityHeadship: departmentHeadship) {
					for(DepartmentParticipation entityParticipation: departmentParticipation) {
						if(entityHeadship.getDepartment().equals(entityParticipation.getDepartment())) {
							return entityParticipation.getDepartment();
						}
					}
				}
			}
			if(sessionController.isStaff()) {
				for(DepartmentSecretary entitySecretary: departmentSecretary) {
					for(DepartmentParticipation entityParticipation: departmentParticipation) {
						if(entitySecretary.getDepartment().equals(entityParticipation.getDepartment())) {
							return entityParticipation.getDepartment();
						}
					}
				}
			}
		}
		
		return null;
	}
	
	/** Render the Appoint new academic form */
	public String appointNew() {

		// Sets the data as read-write.
		readOnly = false;
		
		// Resets the entity so we can create a new one.
		selectedEntity = createNewEntity();
		
	    return VIEW_PATH + "form.xhtml?faces-redirect=true";
	}
	
	
	public String saveAppointment() {
		logger.log(Level.INFO, "Saving new appoint entity...");

		// Prepare the entity for saving.
		prepEntity();
		
		Long dateNow = System.currentTimeMillis();
        Timestamp timestamp = new Timestamp(dateNow);

		// Validates the operation first and stops in case of errors.
		try {
			if (selectedEntity.getId() == null) {
				
				selectedEntity.setBeginDate(timestamp);
				selectedEntity.setDepartment(getAdminDepartment());
				
				getCrudService().validateCreate(selectedEntity);
				getCrudService().create(selectedEntity);
				addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_INFO, getBundlePrefix() + ".text.createSucceeded", summarizeSelectedEntity());
			}
		}
		catch (CrudException crudException) {
			// Adds an error message to each validation error included in the exception.
			for (CrudValidationError error : crudException) {
				logger.log(Level.WARNING, "Exception while saving " + selectedEntity, crudException.getMessage());

				// Checks if the field name was specified. If it was, attach the message to the form field.
				if (error.getFieldName() != null) {
					addFieldI18nMessage(getFieldName(error.getFieldName()), getBundleName(), FacesMessage.SEVERITY_ERROR, error.getMessageKey(), error.getMessageParams());
				}
				else {
					addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_ERROR, error.getMessageKey(), error.getMessageParams());
				}
			}

			// Goes back to the same page, i.e., the form.
			return null;
		}

		// Goes back to the listing.
		return listEntities();
	}
	
	public String listEntities() {
		logger.log(Level.INFO, "Listing entities...");

		// Clears the selection.
		selectedEntity = null;

		// Gets the entity count.
		count();

		// Checks if the index of the listing should be changed and reload the page.
		if (firstEntityIndex < 0) goFirst();
		else if (lastEntityIndex > entityCount) goLast();
		else retrieveEntities();

		// Goes to the listing.
		return VIEW_PATH + "index.xhtml?faces-redirect=true";
	}
	
	public String cancelAppointment() {
		
		// Removes the entity from the finish can and cancel his finalization.
		logger.log(Level.INFO, "Appointment has been cancelled. Clearing selected entity");
		selectedEntity = null;
		
		// Goes back to the listing.
		return VIEW_PATH + "index.xhtml?faces-redirect=true";
	}
	
	/** Moves the selected entity to the finish can for possible future end of allocation. */
	public void finishParticipation() {
		
		// Proceed only if there is a selected entity or selected entity doesn't have a setted end date.
		if ((selectedEntity == null) || (selectedEntity.getEndDate() != null)) {
			logger.log(Level.WARNING, "Method finishParticipation() called, but selectedEntity is null or have been finalized already!");
			return;
		}		

		// Adds the selected entity to the finish can so the user can confirm the finalization.
		logger.log(Level.INFO, "Adding {0} (id {1}) to the finish can for future end of allocation.", new Object[] { selectedEntity, selectedEntity.getId() });
		finishCan = selectedEntity;
	}
	
	/** Set the end Date for the entity in the finish can. The date will be equal to the current date. */
	public void setEndDate() {
		
		logger.log(Level.INFO, "Setting end date for the selected entity...");
		Long dateNow = System.currentTimeMillis();
        Timestamp timestamp = new Timestamp(dateNow);
		
		finishCan.setEndDate(timestamp);
		manageDepartmentStudentRepresentationService.saveEntity(finishCan);
		
		// Clears the finishCan
		finishCan = null;
				
		// Clears the selection.
		selectedEntity = null;
	}
	
	/** Cancel finishing selected entity allocation and cleans the finish can. */
	public void cancelFinishParticipation() {
		
		// Removes the entity from the finish can and cancel his finalization.
		logger.log(Level.INFO, "Finalization has been cancelled. Clearing finish can");
		finishCan = null;

		// Clears the selection.
		selectedEntity = null;
	}
	
	/**========================================================ACADEMIC========================================================*/

	public PersistentObjectConverterFromId<Academic> getAcademicConverter() {
		return manageDepartmentStudentRepresentationService.getAcademicConverter();
	}
	
	public Academic getSelectedAcademic() {		
		Academic academic = selectedEntity.getAcademic();
		
		return (academic == null) ? new Academic() : academic;
	}
	
	public void setSelectedAcademic(Academic selectedAcademic) throws CrudException {
		Set<Role> setRole = selectedAcademic.getRoles();
		List<Role> listRole = convertSetToList(setRole);
		boolean haveStudentRole = false;
		
		for(int i = 0; i< listRole.size(); i++) {
			if(listRole.get(i).getName().compareTo("Student") == 0) {
				selectedEntity.setAcademic((selectedAcademic == null) ? new Academic() : selectedAcademic);
				haveStudentRole = true;
			}
		}
		
		if(haveStudentRole == false) {
			logger.log(Level.WARNING, "New academic violates role condition");
			throw new CrudException("New academic must have the role: Student", null, null);
		}
	}
	
	public static <T> List<T> convertSetToList(Set<T> set) {
        
		// create an empty list
        List<T> list = new ArrayList<>();
  
        // push each element in the set into the list
        for (T t : set)
            list.add(t);
  
        // return the list
        return list;
    }
	
	public List<Academic> completeAcademic(String query) {
		return manageDepartmentStudentRepresentationService.findAcademicByQuery(query);
	}
	
/**========================================================DEPARTMENT========================================================*/
	
	public PersistentObjectConverterFromId<Department> getDepartmentConverter() {
		return manageDepartmentStudentRepresentationService.getDepartmentConverter();
	}
	
	public Department getSelectedDepartment() {
		Department department = selectedEntity.getDepartment();
		return (department == null) ? new Department() : department;
	}
	
	public void setSelectedDepartment(Department selectedDepartment) {
		selectedEntity.setDepartment((selectedDepartment == null) ? new Department() : selectedDepartment);
	}
	
	public List<Department> completeDepartment(String query) {
		return manageDepartmentStudentRepresentationService.findDepartmentByName(query);
	}
}
