package br.ufes.informatica.marvin.admin.application;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.core.persistence.AcademicDAO;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourse;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourseSecretary;
import br.ufes.informatica.marvin.admin.persistence.UndergraduateCourseSecretaryDAO;
import br.ufes.informatica.marvin.admin.persistence.UndergraduateCourseDAO;

/**
 * Stateless session bean implementing the ManageUndergraduateCourseSecretaryService.
 * See the implemented interface documentation for details.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
@RolesAllowed({"SysAdmin", "Professor", "Staff"})
public class ManageUndergraduateCourseSecretaryServiceBean extends CrudServiceBean<UndergraduateCourseSecretary> implements ManageUndergraduateCourseSecretaryService {

	/**=====================================UndergraduateCourseSecretary=====================================*/

	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** The UndergraduateCourseSecretaryDAO is used to save the UndergraduateCourseSecretary that is registered by the SysAdmin. */
	@EJB
	private UndergraduateCourseSecretaryDAO undergraduateCourseSecretaryDAO;
	
	/** Getter for UndergraduateCourseSecretaryDAO. */
	@Override
	public BaseDAO<UndergraduateCourseSecretary> getDAO() {
		return undergraduateCourseSecretaryDAO;
	}
	
	@Override
	public boolean checkIfAcademicIsUndergraduateCourseSecretary(Academic academic) {
		return undergraduateCourseSecretaryDAO.academicIsActiveUndergraduateCourseSecretary(academic);
	}
	
	@Override
	public List<UndergraduateCourseSecretary> returnUndergraduateCourseSecretaryByAcademic(Academic academic) {
		return undergraduateCourseSecretaryDAO.retrieveByAcademic(academic);
	}
	
	@Override
	public List<UndergraduateCourseSecretary> returnUndergraduateCourseSecretaryByDepartment(UndergraduateCourse undergraduateCourse){
		return undergraduateCourseSecretaryDAO.retrieveByUndergraduateCourse(undergraduateCourse);
	}
	
	public void saveEntity(UndergraduateCourseSecretary entity) {
		// Validates the entity before persisting.
		entity = validate(entity, null);
		
		// Save the entity.
		undergraduateCourseSecretaryDAO.save(entity);
	}
	
	/**========================================================ACADEMIC========================================================*/

	@EJB
	private AcademicDAO academicDAO;
	
	private PersistentObjectConverterFromId<Academic> academicConverter;
	
	@Override
	public PersistentObjectConverterFromId<Academic> getAcademicConverter() {
		if(academicConverter == null) {
			academicConverter = new PersistentObjectConverterFromId<Academic>(academicDAO);
		}
		return academicConverter;
	}
	
	@Override
	public List<Academic> findAcademicByQuery(String query) {
		return academicDAO.findByNameEmailOrCpf(query);
	}
	
	/**===========================================UndergraduateCourse===========================================*/

	@EJB
	private UndergraduateCourseDAO undergraduateCourseDAO;
	
	private PersistentObjectConverterFromId<UndergraduateCourse> undergraduateCourseConverter;
	
	@Override
	public PersistentObjectConverterFromId<UndergraduateCourse> getUndergraduateCourseConverter() {
		if(undergraduateCourseConverter == null) {
			undergraduateCourseConverter = new PersistentObjectConverterFromId<UndergraduateCourse>(undergraduateCourseDAO);
		}
		return undergraduateCourseConverter;
	}

	@Override
	public List<UndergraduateCourse> findUndergraduateCourseByQuery(String query) {
		return undergraduateCourseDAO.findByQuery(query);
	}
}