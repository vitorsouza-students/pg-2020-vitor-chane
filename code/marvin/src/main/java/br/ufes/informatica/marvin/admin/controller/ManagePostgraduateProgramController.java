package br.ufes.informatica.marvin.admin.controller;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.application.filters.LikeFilter;
import br.ufes.inf.nemo.jbutler.ejb.controller.CrudController;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.admin.application.ManagePostgraduateProgramService;
import br.ufes.informatica.marvin.admin.domain.Faculty;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgram;

@Named
@SessionScoped
public class ManagePostgraduateProgramController extends CrudController<PostgraduateProgram>{

	/**=============================================PostgraduateProgram======================================================*/

	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** The "Manage PostgraduateProgram" service. */
	@EJB
	private ManagePostgraduateProgramService managePostgraduateProgramService;
	
	/** Getter for "Manage PostgraduateProgram" service. */
	@Override
	protected CrudService<PostgraduateProgram> getCrudService() {
		return managePostgraduateProgramService;
	}
	
	@Override
	protected void initFilters() {
		addFilter(new LikeFilter("managePostgraduateProgram.filter.byName", "name", "Filter PostgraduateProgram by name"));
	}
	
	/**=====================================================FACULTY=====================================================*/
	
	public PersistentObjectConverterFromId<Faculty> getFacultyConverter() {
		return managePostgraduateProgramService.getFacultyConverter();
	}
	
	public Faculty getSelectedFaculty() {
		Faculty faculty = selectedEntity.getFaculty();
		
		return (faculty == null) ? new Faculty() : faculty;
	}
	
	public void setSelectedFaculty(Faculty selectedFaculty) {
		selectedEntity.setFaculty((selectedFaculty == null) ? new Faculty() : selectedFaculty);
	}
	
	public List<Faculty> completeFaculty(String query) {
		return managePostgraduateProgramService.findFacultyByName(query);
	}
}