package br.ufes.informatica.marvin.admin.application;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.admin.domain.Department;
import br.ufes.informatica.marvin.admin.domain.DepartmentSecretary;
import br.ufes.informatica.marvin.core.domain.Academic;

@Local
public interface ManageDepartmentSecretaryService extends CrudService<DepartmentSecretary> {

	void saveEntity(DepartmentSecretary entity);
	
	boolean checkIfAcademicHasDepartmentParticipation(Academic academic);
	
	boolean checkIfAcademicIsDepartmentSecretary(Academic academic);
	
	List<DepartmentSecretary> returnDepartmentSecretaryByAcademic(Academic academic);
	
	List<DepartmentSecretary> returnDepartmentSecretaryByDepartment(Department department);
	
	PersistentObjectConverterFromId<Academic> getAcademicConverter();
	
	PersistentObjectConverterFromId<Department> getDepartmentConverter();
	
	List<Academic> findAcademicByQuery(String string);
	
	List<Department> findDepartmentByName(String name);
}
