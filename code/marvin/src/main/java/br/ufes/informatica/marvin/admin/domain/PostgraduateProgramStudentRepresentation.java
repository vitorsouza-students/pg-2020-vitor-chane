package br.ufes.informatica.marvin.admin.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * The PostgraduateProgramStudentRepresentation class for the MACS (Marvin Access Control System) module of Marvin.
 *
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
 * @version 1.0
*/
@Entity
public class PostgraduateProgramStudentRepresentation extends PersistentObjectSupport implements Comparable<PostgraduateProgramStudentRepresentation> {

	/**============================PostgraduateProgramStudentRepresentation INFORMATION=================================================*/

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The timestamp of the moment the academic started to work on the PostgraduateProgramStudentRepresentation */
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private Date beginDate;
	
	/** Getter for beginDate. */
	public Date getBeginDate() {
	  return beginDate;
	}
	
	/** Setter for beginDate. */
	public void setBeginDate(Date beginDate) {
	  this.beginDate = beginDate;
	}
	
	/** The timestamp of the moment the academic ended the work on the PostgraduateProgramStudentRepresentation */
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	
	/** Getter for endDate. */
	public Date getEndDate() {
	  return endDate;
	}
	
	/** Setter for endDate. */
	public void setEndDate(Date endDate) {
	  this.endDate = endDate;
	}

	@Override
	public int compareTo(PostgraduateProgramStudentRepresentation postgraduateProgramStudentRepresentation) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**========================================================ACADEMIC========================================================*/

	/** Academic with Student role for this PostgraduateProgramStudentRepresentation. */
	@NotNull
	@ManyToOne
	private Academic academic;
	
	/** Getter for academic. */
	public Academic getAcademic() {
		return academic;
	}
 	
 	/** Setter for academic. */
 	public void setAcademic(Academic academic) {
 		this.academic = academic;
 	}
	
	/**========================================================PostgraduateProgram========================================================*/

	/** Related PostgraduateProgram for this PostgraduateProgramStudentRepresentation.*/
 	@NotNull
 	@ManyToOne
 	private PostgraduateProgram postgraduateProgram;
 	
 	/** Getter for PostgraduateProgram. */
 	public PostgraduateProgram getPostgraduateProgram() {
 		return postgraduateProgram;
 	}
 	
 	/** Setter for PostgraduateProgram. */
 	public void setPostgraduateProgram(PostgraduateProgram postgraduateProgram) {
 		this.postgraduateProgram = postgraduateProgram;
 	}
}
