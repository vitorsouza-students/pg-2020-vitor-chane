package br.ufes.informatica.marvin.admin.application;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.informatica.marvin.admin.domain.Department;
import br.ufes.informatica.marvin.admin.domain.DepartmentHeadship;
import br.ufes.informatica.marvin.admin.persistence.DepartmentDAO;
import br.ufes.informatica.marvin.admin.persistence.DepartmentHeadshipDAO;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.core.persistence.AcademicDAO;

/**
 * Stateless session bean implementing the department headship service.
 * See the implemented interface documentation for details.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
@RolesAllowed({"SysAdmin", "Professor", "Staff"})
public class ManageDepartmentHeadshipServiceBean extends CrudServiceBean<DepartmentHeadship> implements ManageDepartmentHeadshipService {
	
	/**===============================================DEPARTMENT HEADSHIP========================================================*/

	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** The DepartmentHeadship DAO is used to save the department headship that is registered by the SysAdmin. */
	@EJB
	private DepartmentHeadshipDAO departmentHeadshipDAO;

	/** Getter for department headship DAO. */
	@Override
	public BaseDAO<DepartmentHeadship> getDAO() {
		return departmentHeadshipDAO;
	}
	
	/** Check if academic is active department boss or underboss*/
	@Override
	public boolean checkIfAcademicIsDepartmentBoss(Academic academic) {
		return departmentHeadshipDAO.academicIsActiveDepartmentBoss(academic);
	}
	
	@Override
	public List<DepartmentHeadship> returnDepartmentHeadshipByAcademic(Academic academic) {
		return departmentHeadshipDAO.retrieveByAcademic(academic);
	}
	
	@Override
	public List<DepartmentHeadship> returnDepartmentHeadshipByDepartment(Department department){
		return departmentHeadshipDAO.retrieveByDepartment(department);
	}
	
	public void saveEntity(DepartmentHeadship entity) {
		// Validates the entity before persisting.
		entity = validate(entity, null);
		
		// Save the entity.
		departmentHeadshipDAO.save(entity);
	}
		
	/**========================================================ACADEMIC========================================================*/

	@EJB
	private AcademicDAO academicDAO;
	
	private PersistentObjectConverterFromId<Academic> academicConverter;
	
	@Override
	public PersistentObjectConverterFromId<Academic> getAcademicConverter() {
		if(academicConverter == null) {
			academicConverter = new PersistentObjectConverterFromId<Academic>(academicDAO);
		}
		return academicConverter;
	}

	@Override
	public List<Academic> findAcademicByQuery(String query) {
		return academicDAO.findByNameEmailOrCpf(query);
	}
	
	/**========================================================DEPARTMENT========================================================*/
	
	@EJB
	private DepartmentDAO departmentDAO;
	
	private PersistentObjectConverterFromId<Department> departmentConverter;

	@Override
	public PersistentObjectConverterFromId<Department> getDepartmentConverter() {
		if(departmentConverter == null) {
			departmentConverter = new PersistentObjectConverterFromId<Department>(departmentDAO);
		}
		return departmentConverter;
	}

	@Override
	public List<Department> findDepartmentByName(String name) {
		return departmentDAO.findByName(name);
	}
}
