package br.ufes.informatica.marvin.admin.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;

/**
 * The PostgraduateProgram class for the MACS (Marvin Access Control System) module of Marvin.
 *
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
 * @version 1.0
*/
@Entity
public class PostgraduateProgram extends PersistentObjectSupport implements Comparable <PostgraduateProgram> {

	/**========================================================PostgraduateProgram============================================================*/

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The PostgraduateProgram name. */
	@NotNull
	@Size(max = 150)
	private String name;
	
	
	/** Getter for PostgraduateProgram name. */
	public String getName() {
		return name;
	}
	
	/** Setter for PostgraduateProgram name. */
	public void setName(String name) {
		this.name = name;
	}
	
	/** Compare function for PostgraduateProgram. */
	@Override
	public int compareTo(PostgraduateProgram postgraduateProgram) {
		
		// Compare the PostgraduateProgram's name
		if (name == null) {
			return 1;
		}
		if (postgraduateProgram.name == null) {
			return -1;
		}
		int cmp = name.compareTo(postgraduateProgram.name);
		if (cmp != 0) {
			return cmp;
		}
		
		// If it's the same name, check if it's the same entity.
		return uuid.compareTo(postgraduateProgram.uuid);
	}
	
/**========================================================PostgraduateProgram FACULTY==================================================*/
	
	/** Related faculty for this PostgraduateProgram.*/
	@NotNull
	@ManyToOne
	private Faculty faculty;

	/** Getter for the PostgraduateProgram related faculty.*/
	public Faculty getFaculty() {
		return faculty;
	}
	
	/** Setter for the PostgraduateProgram related faculty.*/
 	public void setFaculty(Faculty faculty) {
 		this.faculty = faculty;
	}
}
