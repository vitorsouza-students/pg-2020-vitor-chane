package br.ufes.informatica.marvin.admin.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * The PostgraduateCourseEnrollment class for the MACS (Marvin Access Control System) module of Marvin.
 *
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
 * @version 1.0
*/
@Entity
public class PostgraduateCourseEnrollment extends PersistentObjectSupport implements Comparable<PostgraduateCourseEnrollment> {

	/**============================PostgraduateCourseEnrollment INFORMATION=================================================*/

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The timestamp of the moment the academic started to work on the PostgraduateCourseEnrollment */
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private Date beginDate;
	
	/** Getter for beginDate. */
	public Date getBeginDate() {
	  return beginDate;
	}
	
	/** Setter for beginDate. */
	public void setBeginDate(Date beginDate) {
	  this.beginDate = beginDate;
	}
	
	/** The timestamp of the moment the academic ended the work on the PostgraduateCourseEnrollment */
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	
	/** Getter for endDate. */
	public Date getEndDate() {
	  return endDate;
	}
	
	/** Setter for endDate. */
	public void setEndDate(Date endDate) {
	  this.endDate = endDate;
	}
	
	/** Course enrollment */
	@NotNull
	private int enrollment;
	
	/** Getter for enrollment. */
	public int getEnrollment() {
		return enrollment;
	}
	
	/** Setter for enrollment. */
	public void setEnrollment(int enrollment) {
		this.enrollment = enrollment;
	}
	
	/** If the academic have concluded the course or not. */
	private boolean isConcluded;
	
	/** Getter for isConcluded. */
	public boolean getIsConcluded() {
		return isConcluded;
	}
	
	/** Setter for isConcluded. */
	public void setIsConcluded(boolean isConcluded) {
	  this.isConcluded = isConcluded;
	}
	
	/** If the academic have been disconnected from the course or not. */
	private boolean wasDisconnected;
	
	/** Getter for wasDisconnected. */
	public boolean getWasDisconnected() {
		return wasDisconnected;
	}
	
	/** Setter for wasDisconnected. */
	public void setWasDisconnected(boolean wasDisconnected) {
	  this.wasDisconnected = wasDisconnected;
	}
	
	/** If it was disconnected from the course, the reason */
	private String disconnectionReason;
	
	/** Getter for disconnectionReason. */
	public String getDisconnectionReason() {
		return disconnectionReason;
	}
	
	/** Setter for disconnectionReason. */
	public void setDisconnectionReason(String disconnectionReason) {
	  this.disconnectionReason = disconnectionReason;
	}

	@Override
	public int compareTo(PostgraduateCourseEnrollment postgraduateProgramCollegiateMember) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**========================================================ACADEMIC========================================================*/

	/** Academic with Student role for this PostgraduateCourseEnrollment. */
	@NotNull
	@ManyToOne
	private Academic academic;
	
	/** Getter for academic. */
	public Academic getAcademic() {
		return academic;
	}
 	
 	/** Setter for academic. */
 	public void setAcademic(Academic academic) {
 		this.academic = academic;
 	}
	
	/**========================================================PostgraduateCourse========================================================*/

	/** Related PostgraduateCourse for this PostgraduateCourseEnrollment.*/
 	@NotNull
 	@ManyToOne
 	private PostgraduateCourse postgraduateCourse;
 	
 	/** Getter for PostgraduateCourse. */
 	public PostgraduateCourse getPostgraduateCourse() {
 		return postgraduateCourse;
 	}
 	
 	/** Setter for PostgraduateCourse. */
 	public void setPostgraduateCourse(PostgraduateCourse postgraduateCourse) {
 		this.postgraduateCourse = postgraduateCourse;
 	}
}
