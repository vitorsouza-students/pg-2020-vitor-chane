package br.ufes.informatica.marvin.admin.application;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourse;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourseCollegiateMembers;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * Local EJB interface for the session information component. This bean provides an authentication
 * method for the controller, identifying users of the system.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Local
public interface ManageUndergraduateCourseCollegiateMembersService extends CrudService<UndergraduateCourseCollegiateMembers> {
	
	void saveEntity(UndergraduateCourseCollegiateMembers entity);
	
	List<UndergraduateCourseCollegiateMembers> returnUndergraduateCourseCollegiateMembersByAcademic(Academic academic);
	
	List<UndergraduateCourseCollegiateMembers> returnUndergraduateCourseCollegiateMembersByDepartment(UndergraduateCourse undergraduateCourse);

	PersistentObjectConverterFromId<Academic> getAcademicConverter();
	
	PersistentObjectConverterFromId<UndergraduateCourse> getUndergraduateCourseConverter();
	
	List<Academic> findAcademicByQuery(String query);
	
	List<UndergraduateCourse> findUndergraduateCourseByQuery(String query);
}
