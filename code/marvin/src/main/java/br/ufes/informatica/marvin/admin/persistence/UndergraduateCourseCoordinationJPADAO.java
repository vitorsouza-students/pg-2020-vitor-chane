package br.ufes.informatica.marvin.admin.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourseCoordination_;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourse;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourseCoordination;

/**
 * Stateless session bean implementing a DAO for objects of the undergraduate course secretary domain class using JPA2.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation implementations are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any is defined
 * in the implementing DAO interface) have to be implemented in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
public class UndergraduateCourseCoordinationJPADAO extends BaseJPADAO<UndergraduateCourseCoordination> implements UndergraduateCourseCoordinationDAO {
	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(UndergraduateCourseCoordinationJPADAO.class.getCanonicalName());

	/** The application's persistent context provided by the application server. */
	@PersistenceContext
	private EntityManager entityManager;
	
	/** Getter for Entity Manager. */
	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	@Override
	public List<UndergraduateCourseCoordination> retrieveByAcademic(Academic academic) {
		logger.log(Level.FINE, "Retrieving all Undergraduate Course Secretaries instances whose academic is \"{0}\"...", academic);
		
	    // Constructs the query over the Undergraduate Course Coordination class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<UndergraduateCourseCoordination> cq = cb.createQuery(UndergraduateCourseCoordination.class);
	    Root<UndergraduateCourseCoordination> root = cq.from(UndergraduateCourseCoordination.class);
	
	    // Filters the query with the academic.
	    cq.where(cb.equal(root.get(UndergraduateCourseCoordination_.academic), academic));
	    List<UndergraduateCourseCoordination> result = entityManager.createQuery(cq).getResultList();
	    logger.log(Level.INFO, "Retrieving all Undergraduate Course Secretaries instances by the academic \"{0}\" returned {1} results", new Object[] {academic, result.size()});
	    
	    return result;
	}

	@Override
	public List<UndergraduateCourseCoordination> retrieveByUndergraduateCourse(UndergraduateCourse undergraduateCourse) {
		logger.log(Level.FINE, "Retrieving all Undergraduate Course Secretaries instances whose department is \"{0}\"...", undergraduateCourse);
		
		// Constructs the query over the Undergraduate Course Coordination class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<UndergraduateCourseCoordination> cq = cb.createQuery(UndergraduateCourseCoordination.class);
	    Root<UndergraduateCourseCoordination> root = cq.from(UndergraduateCourseCoordination.class);
	    
	    // Filters the query with the Undergraduate Course.
	    cq.where(cb.equal(root.get(UndergraduateCourseCoordination_.undergraduateCourse), undergraduateCourse));
	    List<UndergraduateCourseCoordination> result = entityManager.createQuery(cq).getResultList();
	    
	    logger.log(Level.INFO, "Retrieving all Undergraduate Course Secretaries instances by the department \"{0}\" returned {1} results", new Object[] {undergraduateCourse, result.size()});
	    
	    return result;
	}
	
	@Override
	public List<UndergraduateCourseCoordination> retrieveBySpecificRole(UndergraduateCourse specificRole) {
		logger.log(Level.FINE, "Retrieving all Undergraduate Course Coordination instances whose specific role is \"{0}\"...", specificRole);
		
		// Constructs the query over the Undergraduate Course Coordination class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<UndergraduateCourseCoordination> cq = cb.createQuery(UndergraduateCourseCoordination.class);
	    Root<UndergraduateCourseCoordination> root = cq.from(UndergraduateCourseCoordination.class);
	    
	    // Filters the query with the specific role.
	    cq.where(cb.equal(root.get(UndergraduateCourseCoordination_.isCoordinator), specificRole));
	    List<UndergraduateCourseCoordination> result = entityManager.createQuery(cq).getResultList();
	    
	    logger.log(Level.INFO, "Retrieving all Undergraduate Course Coordination instances by the specific role \"{0}\" returned {1} results", new Object[] {specificRole, result.size()});
	    
	    return result;
	}

	@Override
	public boolean academicIsActiveUndergraduateCourseCoordination(Academic academic) {
		logger.log(Level.FINE, "Checking if the Undergraduate Course has active secretary");
		
		// Constructs the query over the Undergraduate Course Coordination class.
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<UndergraduateCourseCoordination> cq = cb.createQuery(UndergraduateCourseCoordination.class);
		Root<UndergraduateCourseCoordination> root = cq.from(UndergraduateCourseCoordination.class);
		
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(cb.equal(root.get(UndergraduateCourseCoordination_.academic), academic));
		predicates.add(cb.isNull(root.get(UndergraduateCourseCoordination_.endDate)));
		
		// Filters the query with the academic.
		cq.select(root).where(predicates.toArray(new Predicate[] {}));
		List<UndergraduateCourseCoordination> result = entityManager.createQuery(cq).getResultList();
		
		if (!result.isEmpty()) {
			return true;
		}
		
		return false;
	}
}
