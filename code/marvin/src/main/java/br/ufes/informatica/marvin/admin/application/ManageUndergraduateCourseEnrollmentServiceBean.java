package br.ufes.informatica.marvin.admin.application;

import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.core.persistence.AcademicDAO;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourse;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourseEnrollment;
import br.ufes.informatica.marvin.admin.persistence.UndergraduateCourseEnrollmentDAO;
import br.ufes.informatica.marvin.admin.persistence.UndergraduateCourseDAO;

/**
 * Stateless session bean implementing the ManageUndergraduateCourseEnrollmentService.
 * See the implemented interface documentation for details.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
@RolesAllowed({"SysAdmin", "Professor", "Staff"})
public class ManageUndergraduateCourseEnrollmentServiceBean extends CrudServiceBean<UndergraduateCourseEnrollment> implements ManageUndergraduateCourseEnrollmentService {

	/**=====================================UndergraduateCourseEnrollment=====================================*/

	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** The UndergraduateCourseEnrollmentDAO is used to save the UndergraduateCourseEnrollment that is registered by the SysAdmin. */
	@EJB
	private UndergraduateCourseEnrollmentDAO undergraduateCourseEnrollmentDAO;
	
	/** Getter for UndergraduateCourseEnrollmentDAO. */
	@Override
	public BaseDAO<UndergraduateCourseEnrollment> getDAO() {
		return undergraduateCourseEnrollmentDAO;
	}
	
	@Override
	public List<UndergraduateCourseEnrollment> returnUndergraduateCourseEnrollmentByAcademic(Academic academic) {
		return undergraduateCourseEnrollmentDAO.retrieveByAcademic(academic);
	}
	
	@Override
	public List<UndergraduateCourseEnrollment> returnUndergraduateCourseEnrollmentByDepartment(UndergraduateCourse undergraduateCourse){
		return undergraduateCourseEnrollmentDAO.retrieveByUndergraduateCourse(undergraduateCourse);
	}
	
	public void saveEntity(UndergraduateCourseEnrollment entity) {
		// Validates the entity before persisting.
		entity = validate(entity, null);
		
		// Save the entity.
		undergraduateCourseEnrollmentDAO.save(entity);
	}
	
	/**========================================================ACADEMIC========================================================*/

	@EJB
	private AcademicDAO academicDAO;
	
	private PersistentObjectConverterFromId<Academic> academicConverter;
	
	@Override
	public PersistentObjectConverterFromId<Academic> getAcademicConverter() {
		if(academicConverter == null) {
			academicConverter = new PersistentObjectConverterFromId<Academic>(academicDAO);
		}
		return academicConverter;
	}
	
	@Override
	public List<Academic> findAcademicByQuery(String query) {
		return academicDAO.findByNameEmailOrCpf(query);
	}
	
	/**===========================================UndergraduateCourse===========================================*/

	@EJB
	private UndergraduateCourseDAO undergraduateCourseDAO;
	
	private PersistentObjectConverterFromId<UndergraduateCourse> undergraduateCourseConverter;
	
	@Override
	public PersistentObjectConverterFromId<UndergraduateCourse> getUndergraduateCourseConverter() {
		if(undergraduateCourseConverter == null) {
			undergraduateCourseConverter = new PersistentObjectConverterFromId<UndergraduateCourse>(undergraduateCourseDAO);
		}
		return undergraduateCourseConverter;
	}

	@Override
	public List<UndergraduateCourse> findUndergraduateCourseByQuery(String query) {
		return undergraduateCourseDAO.findByQuery(query);
	}
}