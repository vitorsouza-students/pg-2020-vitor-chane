package br.ufes.informatica.marvin.admin.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;

/**
 * The undergraduate course class for the MACS (Marvin Access Control System) module of Marvin.
 *
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
 * @version 1.0
*/
@Entity
public class UndergraduateCourse extends PersistentObjectSupport implements Comparable<UndergraduateCourse> {

	/**=============================================UNDERGRADUATE COURSE INFORMATION=====================================================*/

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The undergraduate course name. */
	@NotNull
	@Size(max = 150)
	private String name;
	
	/** Getter for undergraduate course name. */
	public String getName() {
		return name;
	}
	
	/** Setter for undergraduate course name. */
	public void setName(String name) {
		this.name = name;
	}
	
	/** Compare function for undergraduate course. */
	@Override
	public int compareTo(UndergraduateCourse undergraduateCourse) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/** The undergraduate course code. */
	@NotNull
	private int code;
	
	/** Getter for undergraduate course code. */
	public int getCode() {
		return code;
	}
	
	/** Setter for undergraduate course code. */
	public void setCode(int code) {
		this.code = code;
	}
	
	/** The undergraduate course shift. */
	@NotNull
	@Size(max = 100)
	private String shift;
	
	/** Getter for undergraduate course shift. */
	public String getShift() {
		return shift;
	}
	
	/** Setter for undergraduate course shift. */
	public void setShift(String shift) {
		this.shift = shift;
	}
	
	/**========================================== UNDERGRADUATE COURSE FACULTY==================================================*/

	/** Related faculty for this undergraduate course.*/
	@NotNull
	@ManyToOne
	private Faculty faculty;

	/** Getter for the undergraduate course related faculty.*/
	public Faculty getFaculty() {
		return faculty;
	}
	
	/** Setter for the undergraduate course related faculty.*/
 	public void setFaculty(Faculty faculty) {
 		this.faculty = faculty;
	}
}
