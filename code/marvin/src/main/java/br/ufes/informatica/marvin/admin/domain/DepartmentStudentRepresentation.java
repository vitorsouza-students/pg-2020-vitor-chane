package br.ufes.informatica.marvin.admin.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * The department student representation class for the MACS (Marvin Access Control System) module of Marvin.
 *
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
 * @version 1.0
*/

@Entity
public class DepartmentStudentRepresentation extends PersistentObjectSupport implements Comparable <DepartmentStudentRepresentation> {
	
	/**==================================DEPARTMENT STUDENT REPRESENTATION INFORMATION========================================================*/
	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The timestamp of the moment the academic started his student representation on the department. */
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private Date beginDate;
	
	/** Getter for beginDate. */
	public Date getBeginDate() {
	  return beginDate;
	}
	
	/** Setter for beginDate. */
	public void setBeginDate(Date beginDate) {
	  this.beginDate = beginDate;
	}
	
	/** The timestamp of the moment the academic finished his student representation on the department */
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	
	/** Getter for endDate. */
	public Date getEndDate() {
	  return endDate;
	}
	
	/** Setter for endDate. */
	public void setEndDate(Date endDate) {
	  this.endDate = endDate;
	}
	
	/** Compare function for department student representation. */
	@Override
	public int compareTo(DepartmentStudentRepresentation o) {
 		return 0;
 	}
	
	/**========================================================ACADEMIC========================================================*/
	
	/** Student representative (Academic with Student role) for this department. */
	@NotNull
	@ManyToOne
	private Academic academic;
	
	/** Getter for student representative. */
	public Academic getAcademic() {
		return academic;
	}
 	
 	/** Setter for student representative. */
 	public void setAcademic(Academic academic) {
 		this.academic = academic;
 	}
	
	/**========================================================DEPARTMENT========================================================*/
	
	/** Department for this department student representation. */
	@NotNull
	@ManyToOne
	private Department department;
	
	/** Getter for department. */
	public Department getDepartment() {
	  return department;
	}
	
	/** Setter for department. */
 	public void setDepartment(Department department) {
	  this.department = department;
	}
}

