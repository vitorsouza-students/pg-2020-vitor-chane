package br.ufes.informatica.marvin.admin.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;
import br.ufes.informatica.marvin.admin.domain.DepartmentSecretary_;
import br.ufes.informatica.marvin.admin.domain.Department;
import br.ufes.informatica.marvin.admin.domain.DepartmentParticipation;
import br.ufes.informatica.marvin.admin.domain.DepartmentSecretary;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.admin.domain.DepartmentParticipation_;

/**
 * Stateless session bean implementing a DAO for objects of the DepartmentSecretary domain class using JPA2.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation implementations are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any is defined
 * in the implementing DAO interface) have to be implemented in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
public class DepartmentSecretaryJPADAO extends BaseJPADAO<DepartmentSecretary> implements DepartmentSecretaryDAO {
	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(DepartmentSecretaryJPADAO.class.getCanonicalName());
	
	/** The application's persistent context provided by the application server. */
	@PersistenceContext
	private EntityManager entityManager;
	
	/** Getter for Entity Manager. */
	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	public List<DepartmentSecretary> retrieveByAcademic(Academic academic) {
		logger.log(Level.FINE, "Retrieving all department secretary instances whose academic is \"{0}\"...", academic);
		
	    // Constructs the query over the DepartmentSecretary class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<DepartmentSecretary> cq = cb.createQuery(DepartmentSecretary.class);
	    Root<DepartmentSecretary> root = cq.from(DepartmentSecretary.class);
	
	    // Filters the query with the academic.
	    cq.where(cb.equal(root.get(DepartmentSecretary_.academic), academic));
	    List<DepartmentSecretary> result = entityManager.createQuery(cq).getResultList();
	    logger.log(Level.INFO, "Retrieving all department secretary instances by the academic \"{0}\" returned {1} results", new Object[] {academic, result.size()});
	    
	    return result;
	}
	
	public List<DepartmentSecretary> retrieveByDepartment(Department department) {
		logger.log(Level.FINE, "Retrieving all department secretary instances whose department is \"{0}\"...", department);
		
		// Constructs the query over the DepartmentSecretary class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<DepartmentSecretary> cq = cb.createQuery(DepartmentSecretary.class);
	    Root<DepartmentSecretary> root = cq.from(DepartmentSecretary.class);
	    
	    // Filters the query with the department.
	    cq.where(cb.equal(root.get(DepartmentSecretary_.department), department));
	    List<DepartmentSecretary> result = entityManager.createQuery(cq).getResultList();
	    
	    logger.log(Level.INFO, "Retrieving all department secretary instances by the department \"{0}\" returned {1} results", new Object[] {department, result.size()});
	    
	    return result;
	}
	
	@Override
	public boolean academicHasActiveDepartmentParticipation(Academic academic) {
		logger.log(Level.FINE, "Checking if the selected academic has a department participation");
		
		// Constructs the query over the Department Headship class.
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<DepartmentParticipation> cq = cb.createQuery(DepartmentParticipation.class);
		Root<DepartmentParticipation> root = cq.from(DepartmentParticipation.class);
		
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(cb.equal(root.get(DepartmentParticipation_.academic), academic));
		predicates.add(cb.isNull(root.get(DepartmentParticipation_.endDate)));
		
		// Filters the query with the academic.
		cq.select(root).where(predicates.toArray(new Predicate[] {}));
		List<DepartmentParticipation> result = entityManager.createQuery(cq).getResultList();
		
		if (!result.isEmpty()) {
			return true;
		}
		
		return false;
	}

	@Override
	public boolean academicIsActiveDepartmentSecretary(Academic academic) {
		logger.log(Level.FINE, "Checking if the department has active secretary");
		
		// Constructs the query over the Department Headship class.
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<DepartmentSecretary> cq = cb.createQuery(DepartmentSecretary.class);
		Root<DepartmentSecretary> root = cq.from(DepartmentSecretary.class);
		
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(cb.equal(root.get(DepartmentSecretary_.academic), academic));
		predicates.add(cb.isNull(root.get(DepartmentSecretary_.endDate)));
		
		// Filters the query with the academic.
		cq.select(root).where(predicates.toArray(new Predicate[] {}));
		List<DepartmentSecretary> result = entityManager.createQuery(cq).getResultList();
		
		if (!result.isEmpty()) {
			return true;
		}
		
		return false;
	}
}
