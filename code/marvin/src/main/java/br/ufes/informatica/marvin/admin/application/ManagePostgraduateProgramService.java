package br.ufes.informatica.marvin.admin.application;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.admin.domain.Faculty;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgram;

/**
 * Local EJB interface for the session information component. This bean provides an authentication
 * method for the controller, identifying users of the system.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Local
public interface ManagePostgraduateProgramService extends CrudService<PostgraduateProgram> {
	
	/**
	 * TODO: document this method.
	 * 
	 * @return
	*/
	PersistentObjectConverterFromId<Faculty> getFacultyConverter();
	
	/**
	 * Obtains the faculty by typing the faculty name.
	 * 
	 * @param name.
	 * @return The Faculty object that match the current input name.
	*/
	List<Faculty> findFacultyByName(String name);
}