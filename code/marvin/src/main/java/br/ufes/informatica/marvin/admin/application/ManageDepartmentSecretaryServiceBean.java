package br.ufes.informatica.marvin.admin.application;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.informatica.marvin.admin.domain.Department;
import br.ufes.informatica.marvin.admin.domain.DepartmentSecretary;
import br.ufes.informatica.marvin.admin.persistence.DepartmentDAO;
import br.ufes.informatica.marvin.admin.persistence.DepartmentSecretaryDAO;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.core.persistence.AcademicDAO;

@Stateless
@RolesAllowed({"SysAdmin", "Professor", "Staff"})
public class ManageDepartmentSecretaryServiceBean extends CrudServiceBean<DepartmentSecretary> implements ManageDepartmentSecretaryService {
	
	/**===============================================DEPARTMENT SECRETARY========================================================*/
	
	private static final long serialVersionUID = 1L;

	@EJB
	private DepartmentSecretaryDAO departmentSecretaryDAO;
	
	@Override
	public BaseDAO<DepartmentSecretary> getDAO() {
		return departmentSecretaryDAO;
	}
	
	/** Check if an academic has active participation in a Department*/
	@Override
	public boolean checkIfAcademicHasDepartmentParticipation(Academic academic) {
		return departmentSecretaryDAO.academicHasActiveDepartmentParticipation(academic);
	}
	
	/** Check if academic is active department boss or underboss*/
	@Override
	public boolean checkIfAcademicIsDepartmentSecretary(Academic academic) {
		return departmentSecretaryDAO.academicIsActiveDepartmentSecretary(academic);
	}
	
	@Override
	public List<DepartmentSecretary> returnDepartmentSecretaryByAcademic(Academic academic) {
		return departmentSecretaryDAO.retrieveByAcademic(academic);
	}
	
	@Override
	public List<DepartmentSecretary> returnDepartmentSecretaryByDepartment(Department department){
		return departmentSecretaryDAO.retrieveByDepartment(department);
	}
	
	public void saveEntity(DepartmentSecretary entity) {
		// Validates the entity before persisting.
		entity = validate(entity, null);
		
		// Save the entity.
		departmentSecretaryDAO.save(entity);
	}
	
	/**========================================================ACADEMIC========================================================*/

	@EJB
	private AcademicDAO academicDAO;
	
	private PersistentObjectConverterFromId<Academic> academicConverter;
	
	@Override
	public PersistentObjectConverterFromId<Academic> getAcademicConverter() {
		if(academicConverter == null) {
			academicConverter = new PersistentObjectConverterFromId<Academic>(academicDAO);
		}
		return academicConverter;
	}
	
	@Override
	public List<Academic> findAcademicByQuery(String query) {
		return academicDAO.findByNameEmailOrCpf(query);
	}
	
	/**=======================================================DEPARTMENT========================================================*/

	@EJB
	private DepartmentDAO departmentDAO;
	
	private PersistentObjectConverterFromId<Department> departmentConverter;
	
	@Override
	public PersistentObjectConverterFromId<Department> getDepartmentConverter() {
		if(departmentConverter == null) {
			departmentConverter = new PersistentObjectConverterFromId<Department>(departmentDAO);
		}
		return departmentConverter;
	}
	
	@Override
	public List<Department> findDepartmentByName(String name) {
		return departmentDAO.findByName(name);
	}
}
