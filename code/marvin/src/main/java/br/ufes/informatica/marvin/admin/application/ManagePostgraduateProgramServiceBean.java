package br.ufes.informatica.marvin.admin.application;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudException;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.PersistentObjectNotFoundException;
import br.ufes.informatica.marvin.admin.domain.Faculty;
import br.ufes.informatica.marvin.admin.persistence.FacultyDAO;
import br.ufes.informatica.marvin.admin.persistence.PostgraduateProgramDAO;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgram;

/**
 * Stateless session bean implementing the PostgraduateProgram service.
 * See the implemented interface documentation for details.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
@RolesAllowed("SysAdmin")
public class ManagePostgraduateProgramServiceBean extends CrudServiceBean<PostgraduateProgram> implements ManagePostgraduateProgramService {

	/**=========================================UNDERGRADUATE COURSE========================================================*/

	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(ManagePostgraduateProgramServiceBean.class.getCanonicalName());

	/** The PostgraduateProgramDAO is used to save the Undergraduate Course that is registered by the SysAdmin. */
	@EJB
	private PostgraduateProgramDAO postgraduateProgramDAO;
	
	/** Getter for PostgraduateProgramDAO. */
	@Override
	public BaseDAO<PostgraduateProgram> getDAO() {
		return postgraduateProgramDAO;
	}
	
	/** 
	 * Check if there is already a PostgraduateProgram with the same name. If exists throw a exception.
	 * 
	 * @param postgraduateProgram
	 * @throws CrudException
	*/
	@Override
	public void validateCreate(PostgraduateProgram postgraduateProgram) throws CrudException {
		
		// Checks if the chosen name is already in use.
		try {
			postgraduateProgramDAO.retrieveByName(postgraduateProgram.getName());
			logger.log(Level.WARNING, "New PostgraduateProgram violates unique name rule: \"{0}\"", postgraduateProgram.getName());
			throw new CrudException(null, "New PostgraduateProgram violates unique name rule!", null);
		}
		catch (PersistentObjectNotFoundException e) {
			// This is the expected outcome. Just log that everything is OK.
			logger.log(Level.INFO, "New PostgraduateProgram satisfies unique name rule: \"{0}\"", postgraduateProgram.getName());
		}
		catch (MultiplePersistentObjectsFoundException e) {
			// This is a severe problem: the unique constraint has already been violated.
			logger.log(Level.SEVERE, "There are already PostgraduateProgram with the name: \"{0}\"!", postgraduateProgram.getName());
			throw new EJBException(e);
		}
	}
	
	/**===============================================FACULTY========================================================*/

	/** The Faculty DAO is used to save the faculty that is registered by the SysAdmin. */
	@EJB
	private FacultyDAO facultyDAO;
	
	private PersistentObjectConverterFromId<Faculty> facultyConverter;
	
	@Override
	public PersistentObjectConverterFromId<Faculty> getFacultyConverter() {
		
		if(facultyConverter == null) {
			facultyConverter = new PersistentObjectConverterFromId<Faculty>(facultyDAO);
		}
		return facultyConverter;
	}
	
	@Override
	public List<Faculty> findFacultyByName(String name) {
		return facultyDAO.findByName(name);
	}
}