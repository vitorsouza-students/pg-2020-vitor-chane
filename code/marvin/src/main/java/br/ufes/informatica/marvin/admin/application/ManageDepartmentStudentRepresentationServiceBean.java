package br.ufes.informatica.marvin.admin.application;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.informatica.marvin.admin.domain.Department;
import br.ufes.informatica.marvin.admin.domain.DepartmentStudentRepresentation;
import br.ufes.informatica.marvin.admin.persistence.DepartmentDAO;
import br.ufes.informatica.marvin.admin.persistence.DepartmentStudentRepresentationDAO;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.core.persistence.AcademicDAO;

@Stateless
@RolesAllowed({"SysAdmin", "Professor", "Staff"})
public class ManageDepartmentStudentRepresentationServiceBean extends CrudServiceBean<DepartmentStudentRepresentation> implements ManageDepartmentStudentRepresentationService {

	/**======================================DEPARTMENT STUDENT REPRESENTATION========================================================*/
	
	private static final long serialVersionUID = 1L;
	
	@EJB
	private DepartmentStudentRepresentationDAO departmentStudentRepresentationDAO;
	
	@Override
	public BaseDAO<DepartmentStudentRepresentation> getDAO() {
		return departmentStudentRepresentationDAO;
	}
	
	@Override
	public List<DepartmentStudentRepresentation> returnDepartmentHeadshipByAcademic(Academic academic) {
		return departmentStudentRepresentationDAO.retrieveByAcademic(academic);
	}
	
	@Override
	public List<DepartmentStudentRepresentation> returnDepartmentHeadshipByDepartment(Department department){
		return departmentStudentRepresentationDAO.retrieveByDepartment(department);
	}
	
	public void saveEntity(DepartmentStudentRepresentation entity) {
		// Validates the entity before persisting.
		entity = validate(entity, null);
		
		// Save the entity.
		departmentStudentRepresentationDAO.save(entity);
	}
	
	/**========================================================ACADEMIC========================================================*/

	@EJB
	private AcademicDAO academicDAO;
	
	private PersistentObjectConverterFromId<Academic> academicConverter;
	
	@Override
	public PersistentObjectConverterFromId<Academic> getAcademicConverter() {
		if(academicConverter == null) {
			academicConverter = new PersistentObjectConverterFromId<Academic>(academicDAO);
		}
		return academicConverter;
	}
	
	@Override
	public List<Academic> findAcademicByQuery(String query) {
		return academicDAO.findByNameEmailOrCpf(query);
	}
	
	/**========================================================DEPARTMENT========================================================*/

	@EJB
	private DepartmentDAO departmentDAO;
	
	private PersistentObjectConverterFromId<Department> departmentConverter;

	@Override
	public PersistentObjectConverterFromId<Department> getDepartmentConverter() {
		if(departmentConverter == null) {
			departmentConverter = new PersistentObjectConverterFromId<Department>(departmentDAO);
		}
		return departmentConverter;
	}
	
	@Override
	public List<Department> findDepartmentByName(String name) {
		return departmentDAO.findByName(name);
	}
}
