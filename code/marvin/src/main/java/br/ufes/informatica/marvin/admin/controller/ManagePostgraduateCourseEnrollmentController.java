package br.ufes.informatica.marvin.admin.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudException;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudValidationError;
import br.ufes.inf.nemo.jbutler.ejb.application.filters.LikeFilter;
import br.ufes.inf.nemo.jbutler.ejb.controller.CrudController;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.PersistentObjectNotFoundException;
import br.ufes.informatica.marvin.core.controller.SessionController;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.core.domain.Role;
import br.ufes.informatica.marvin.admin.application.ManagePostgraduateCourseEnrollmentService;
import br.ufes.informatica.marvin.admin.application.ManagePostgraduateCourseEnrollmentServiceBean;
import br.ufes.informatica.marvin.admin.application.ManagePostgraduateProgramCoordinationService;
import br.ufes.informatica.marvin.admin.application.ManagePostgraduateProgramMemberService;
import br.ufes.informatica.marvin.admin.application.ManagePostgraduateProgramSecretaryService;
import br.ufes.informatica.marvin.admin.domain.PostgraduateCourse;
import br.ufes.informatica.marvin.admin.domain.PostgraduateCourseEnrollment;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgramCoordination;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgramMember;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgramSecretary;

@Named
@SessionScoped
public class ManagePostgraduateCourseEnrollmentController extends CrudController<PostgraduateCourseEnrollment>{

	/**======================================================PostgraduateCourseEnrollment======================================================*/

	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(ManagePostgraduateCourseEnrollmentServiceBean.class.getCanonicalName());

	/** The ManagePostgraduateCourseEnrollmentService. */
	@EJB
	private ManagePostgraduateCourseEnrollmentService managePostgraduateCourseEnrollmentService;
	
	/** Getter for ManagePostgraduateCourseEnrollmentService. */
	@Override
	protected CrudService<PostgraduateCourseEnrollment> getCrudService() {
		return managePostgraduateCourseEnrollmentService;
	}
	
	@Override
	protected void initFilters() {
		addFilter(new LikeFilter("managePostgraduateCourseEnrollment.filter.byPostgraduateCourse", "postgraduateCourse", "Filter PostgraduateCourseEnrollment by PostgraduateCourse"));
		addFilter(new LikeFilter("managePostgraduateCourseEnrollment.filter.byAcademic", "academic", "Filter PostgraduateCourseEnrollment by academic"));
		addFilter(new LikeFilter("managePostgraduateCourseEnrollment.filter.byEnrollment", "enrollment", "Filter PostgraduateCourseEnrollment byEnrollment"));
		addFilter(new LikeFilter("managePostgraduateCourseEnrollment.filter.byBeginDate", "beginDate", "Filter PostgraduateCourseEnrollment by beginDate"));
		addFilter(new LikeFilter("managePostgraduateCourseEnrollment.filter.byEndDate", "endDate", "Filter PostgraduateCourseEnrollment by endDate"));
	}
	
	/**=======================AppointPostgraduateCourseEnrollment=======================*/

	/** Path to the folder where the view files (web pages) for AppointPostgraduateCourseEnrollment is. */
	private static final String VIEW_PATH = "/admin/appointPostgraduateCourseEnrollment/";

	/** The session controller, with information of the authenticated user. */
	@Inject
	private SessionController sessionController;
	
	@EJB
	private ManagePostgraduateProgramSecretaryService managePostgraduateProgramSecretaryService;
	
	@EJB
	private ManagePostgraduateProgramMemberService managePostgraduateProgramMemberService;
	
	/** The ManagePostgraduateProgramCoordinationService. */
	@EJB
	private ManagePostgraduateProgramCoordinationService managePostgraduateProgramCoordinationService;
	
	/** The entity that will have its participation finalized. */
	protected PostgraduateCourseEnrollment finishCan;
	
	/** Getter for finishCan. */
	public PostgraduateCourseEnrollment getFinishCan() {
		return finishCan;
	}
	
	/** Return if the current logged user is a valid PP Coordinator/ Subcoordinator/ Secretary */
	public boolean isCurrentUserValidPostgraduateProgramAdmin() {
		Academic currentUser = sessionController.getCurrentUser();
		List<PostgraduateProgramCoordination> postgraduateProgramCoordination;
		List<PostgraduateProgramSecretary> postgraduateProgramSecretary;
		List<PostgraduateProgramMember> postgraduateProgramMember;
		
		if(sessionController.isLoggedIn()) {
			if(sessionController.isProfessor()) {
				if(managePostgraduateProgramMemberService.checkIfAcademicIsPostgraduateProgramMember(currentUser)) {
					if(managePostgraduateProgramCoordinationService.checkIfAcademicIsPostgraduateProgramCoordination(currentUser)) {
						postgraduateProgramCoordination = managePostgraduateProgramCoordinationService.returnPostgraduateProgramCoordinationByAcademic(currentUser);
						postgraduateProgramMember = managePostgraduateProgramMemberService.returnPostgraduateProgramMemberByAcademic(currentUser);
						
						// Considering that an specific academic could be PostgraduateProgram Boss/Underboss for more than one postgraduateProgram
						for(PostgraduateProgramCoordination entityCoordination: postgraduateProgramCoordination) {
							for(PostgraduateProgramMember entityMember: postgraduateProgramMember) {
								if(entityCoordination.getPostgraduateProgram().equals(entityMember.getPostgraduateProgram())) {
									return true;
								}
							}
						}
					}
				}
			}
			if(sessionController.isStaff()) {
				if(managePostgraduateProgramMemberService.checkIfAcademicIsPostgraduateProgramMember(currentUser)) {
					if(managePostgraduateProgramSecretaryService.checkIfAcademicIsPostgraduateProgramSecretary(currentUser)) {
						postgraduateProgramSecretary = managePostgraduateProgramSecretaryService.returnPostgraduateProgramSecretaryByAcademic(currentUser);
						postgraduateProgramMember = managePostgraduateProgramMemberService.returnPostgraduateProgramMemberByAcademic(currentUser);
						
						// Considering that an specific academic could be PostgraduateProgram Secretary for more than one postgraduateProgram
						for(PostgraduateProgramSecretary entitySecretary: postgraduateProgramSecretary) {
							for(PostgraduateProgramMember entityMember: postgraduateProgramMember) {
								if(entitySecretary.getPostgraduateProgram().equals(entityMember.getPostgraduateProgram())) {
									return true;
								}
							}
						}
					}
				}
			}
		}

		return false;
	}
	
	/** Render the Appoint new academic form */
	public String appointNew() {

		// Sets the data as read-write.
		readOnly = false;
		
		// Resets the entity so we can create a new one.
		selectedEntity = createNewEntity();
		
	    return VIEW_PATH + "form.xhtml?faces-redirect=true";
	}
	
	public String saveAppointment() {
		logger.log(Level.INFO, "Saving new appoint entity...");

		// Prepare the entity for saving.
		prepEntity();
		
		Long dateNow = System.currentTimeMillis();
        Timestamp timestamp = new Timestamp(dateNow);

		// Validates the operation first and stops in case of errors.
		try {
			if (selectedEntity.getId() == null) {
				selectedEntity.setBeginDate(timestamp);
				
				getCrudService().validateCreate(selectedEntity);
				getCrudService().create(selectedEntity);
				addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_INFO, getBundlePrefix() + ".text.createSucceeded", summarizeSelectedEntity());
			}
		}
		catch (CrudException crudException) {
			// Adds an error message to each validation error included in the exception.
			for (CrudValidationError error : crudException) {
				logger.log(Level.WARNING, "Exception while saving " + selectedEntity, crudException.getMessage());

				// Checks if the field name was specified. If it was, attach the message to the form field.
				if (error.getFieldName() != null) {
					addFieldI18nMessage(getFieldName(error.getFieldName()), getBundleName(), FacesMessage.SEVERITY_ERROR, error.getMessageKey(), error.getMessageParams());
				}
				else {
					addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_ERROR, error.getMessageKey(), error.getMessageParams());
				}
			}

			// Goes back to the same page, i.e., the form.
			return null;
		}

		// Goes back to the listing.
		return listEntities();
	}
	
	public String listEntities() {
		logger.log(Level.INFO, "Listing entities...");

		// Clears the selection.
		selectedEntity = null;

		// Gets the entity count.
		count();

		// Checks if the index of the listing should be changed and reload the page.
		if (firstEntityIndex < 0) goFirst();
		else if (lastEntityIndex > entityCount) goLast();
		else retrieveEntities();

		// Goes to the listing.
		return VIEW_PATH + "index.xhtml?faces-redirect=true";
	}
	
	public String cancelAppointment() {
		
		// Removes the entity from the finish can and cancel his finalization.
		logger.log(Level.INFO, "Appointment has been cancelled. Clearing selected entity");
		selectedEntity = null;
		
		// Goes back to the listing.
		return VIEW_PATH + "index.xhtml?faces-redirect=true";
	}
	
	/** Moves the selected entity to the finish can for possible future end of allocation. */
	public void finishParticipation() {
		
		// Proceed only if there is a selected entity or selected entity doesn't have a setted end date.
		if ((selectedEntity == null) || (selectedEntity.getEndDate() != null)) {
			logger.log(Level.WARNING, "Method finishParticipation() called, but selectedEntity is null or have been finalized already!");
			return;
		}		

		// Adds the selected entity to the finish can so the user can confirm the finalization.
		logger.log(Level.INFO, "Adding {0} (id {1}) to the finish can for future end of allocation.", new Object[] { selectedEntity, selectedEntity.getId() });
		finishCan = selectedEntity;
	}
	
	/** Set the end Date for the entity in the finish can. The date will be equal to the current date. */
	public void setEndDate() {
		
		logger.log(Level.INFO, "Setting end date for the selected entity...");
		Long dateNow = System.currentTimeMillis();
        Timestamp timestamp = new Timestamp(dateNow);
		
		finishCan.setEndDate(timestamp);
		managePostgraduateCourseEnrollmentService.saveEntity(finishCan);
		
		// Clears the finishCan
		finishCan = null;
				
		// Clears the selection.
		selectedEntity = null;
	}
	
	/** Cancel finishing selected entity allocation and cleans the finish can. */
	public void cancelFinishParticipation() {
		
		// Removes the entity from the finish can and cancel his finalization.
		logger.log(Level.INFO, "Finalization has been cancelled. Clearing finish can");
		finishCan = null;

		// Clears the selection.
		selectedEntity = null;
	}
	
	/**======================================================ACADEMIC======================================================*/
	
	public PersistentObjectConverterFromId<Academic> getAcademicConverter() {
		return managePostgraduateCourseEnrollmentService.getAcademicConverter();
	}
	
	public Academic getSelectedAcademic() {		
		Academic academic = selectedEntity.getAcademic();
		
		return (academic == null) ? new Academic() : academic;
	}
	
	public void setSelectedAcademic(Academic selectedAcademic) throws CrudException {
		Set<Role> setRole = selectedAcademic.getRoles();
		List<Role> listRole = convertSetToList(setRole);
		boolean haveStudentRole = false;
		
		for(int i = 0; i< listRole.size(); i++) {
			if(listRole.get(i).getName().compareTo("Student") == 0) {
				selectedEntity.setAcademic((selectedAcademic == null) ? new Academic() : selectedAcademic);
				haveStudentRole = true;
			}
		}
		
		if(haveStudentRole == false) {
			logger.log(Level.WARNING, "New academic violates role condition");
			throw new CrudException("New academic must have the role: Student", null, null);
		}
	}
	
	public static <T> List<T> convertSetToList(Set<T> set) {
        
		// create an empty list
        List<T> list = new ArrayList<>();
  
        // push each element in the set into the list
        for (T t : set)
            list.add(t);
  
        // return the list
        return list;
    }
	
	public List<Academic> completeAcademic(String query) throws PersistentObjectNotFoundException, MultiplePersistentObjectsFoundException {
		return managePostgraduateCourseEnrollmentService.findAcademicByQuery(query);
	}
	
	/**======================================================PostgraduateCourse======================================================*/

	public PersistentObjectConverterFromId<PostgraduateCourse> getPostgraduateCourseConverter() {
		return managePostgraduateCourseEnrollmentService.getPostgraduateCourseConverter();
	}
	
	public PostgraduateCourse getSelectedPostgraduateCourse() {
		PostgraduateCourse postgraduateCourse = selectedEntity.getPostgraduateCourse();
		return (postgraduateCourse == null) ? new PostgraduateCourse() : postgraduateCourse;
	}
	
	public void setSelectedPostgraduateCourse(PostgraduateCourse selectedPostgraduateCourse) {
		selectedEntity.setPostgraduateCourse((selectedPostgraduateCourse == null) ? new PostgraduateCourse() : selectedPostgraduateCourse);
	}
	
	public List<PostgraduateCourse> completePostgraduateCourse(String query) {
		return managePostgraduateCourseEnrollmentService.findPostgraduateCourseByQuery(query);
	}
}