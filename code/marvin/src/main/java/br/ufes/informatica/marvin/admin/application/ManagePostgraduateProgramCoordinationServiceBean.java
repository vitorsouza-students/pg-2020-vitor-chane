package br.ufes.informatica.marvin.admin.application;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.core.persistence.AcademicDAO;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgram;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgramCoordination;
import br.ufes.informatica.marvin.admin.persistence.PostgraduateProgramCoordinationDAO;
import br.ufes.informatica.marvin.admin.persistence.PostgraduateProgramDAO;

/**
 * Stateless session bean implementing the ManagePostgraduateProgramCoordinationService.
 * See the implemented interface documentation for details.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
@RolesAllowed({"SysAdmin", "Professor", "Staff"})
public class ManagePostgraduateProgramCoordinationServiceBean extends CrudServiceBean<PostgraduateProgramCoordination> implements ManagePostgraduateProgramCoordinationService {

	/**=====================================PostgraduateProgramCoordination=====================================*/

	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** The PostgraduateProgramCoordinationDAO is used to save the PostgraduateProgramCoordination that is registered by the SysAdmin. */
	@EJB
	private PostgraduateProgramCoordinationDAO postgraduateProgramCoordinationDAO;
	
	/** Getter for PostgraduateProgramCoordinationDAO. */
	@Override
	public BaseDAO<PostgraduateProgramCoordination> getDAO() {
		return postgraduateProgramCoordinationDAO;
	}
	
	@Override
	public boolean checkIfAcademicIsPostgraduateProgramCoordination(Academic academic) {
		return postgraduateProgramCoordinationDAO.academicIsActivePostgraduateProgramCoordination(academic);
	}
	
	@Override
	public List<PostgraduateProgramCoordination> returnPostgraduateProgramCoordinationByAcademic(Academic academic) {
		return postgraduateProgramCoordinationDAO.retrieveByAcademic(academic);
	}
	
	@Override
	public List<PostgraduateProgramCoordination> returnPostgraduateProgramCoordinationByPostgraduateProgram(PostgraduateProgram postgraduateProgram){
		return postgraduateProgramCoordinationDAO.retrieveByPostgraduateProgram(postgraduateProgram);
	}
	
	public void saveEntity(PostgraduateProgramCoordination entity) {
		// Validates the entity before persisting.
		entity = validate(entity, null);
		
		// Save the entity.
		postgraduateProgramCoordinationDAO.save(entity);
	}
	
	/**========================================================ACADEMIC========================================================*/

	@EJB
	private AcademicDAO academicDAO;
	
	private PersistentObjectConverterFromId<Academic> academicConverter;
	
	@Override
	public PersistentObjectConverterFromId<Academic> getAcademicConverter() {
		if(academicConverter == null) {
			academicConverter = new PersistentObjectConverterFromId<Academic>(academicDAO);
		}
		return academicConverter;
	}
	
	@Override
	public List<Academic> findAcademicByQuery(String query) {
		return academicDAO.findByNameEmailOrCpf(query);
	}
	
	/**===========================================PostgraduateProgram===========================================*/

	@EJB
	private PostgraduateProgramDAO postgraduateProgramDAO;
	
	private PersistentObjectConverterFromId<PostgraduateProgram> postgraduateProgramConverter;
	
	@Override
	public PersistentObjectConverterFromId<PostgraduateProgram> getPostgraduateProgramConverter() {
		if(postgraduateProgramConverter == null) {
			postgraduateProgramConverter = new PersistentObjectConverterFromId<PostgraduateProgram>(postgraduateProgramDAO);
		}
		return postgraduateProgramConverter;
	}

	@Override
	public List<PostgraduateProgram> findPostgraduateProgramByQuery(String query) {
		return postgraduateProgramDAO.findByQuery(query);
	}
}
