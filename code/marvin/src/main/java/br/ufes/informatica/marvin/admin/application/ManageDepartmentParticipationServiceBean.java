package br.ufes.informatica.marvin.admin.application;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.informatica.marvin.admin.domain.Department;
import br.ufes.informatica.marvin.admin.domain.DepartmentParticipation;
import br.ufes.informatica.marvin.admin.persistence.DepartmentDAO;
import br.ufes.informatica.marvin.admin.persistence.DepartmentParticipationDAO;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.core.persistence.AcademicDAO;

@Stateless
@RolesAllowed({"SysAdmin", "Professor", "Staff"})
public class ManageDepartmentParticipationServiceBean extends CrudServiceBean<DepartmentParticipation> implements ManageDepartmentParticipationService {

	/**===============================================DEPARTMENT PARTICIPATION========================================================*/
	
	private static final long serialVersionUID = 1L;

	@EJB
	private DepartmentParticipationDAO departmentParticipationDAO;
	
	@Override
	public BaseDAO<DepartmentParticipation> getDAO() {
		return departmentParticipationDAO;
	}
	
	@Override
	public List<DepartmentParticipation> returnDepartmentParticipationByAcademic(Academic academic) {
		return departmentParticipationDAO.retrieveByAcademic(academic);
	}
	
	@Override
	public List<DepartmentParticipation> returnDepartmentParticipationByDepartment(Department department){
		return departmentParticipationDAO.retrieveByDepartment(department);
	}
	
	/** Check if an academic has active participation in a Department*/
	@Override
	public boolean checkIfAcademicHasDepartmentParticipation(Academic academic) {
		return departmentParticipationDAO.academicHasActiveDepartmentParticipation(academic);
	}
	
	public void saveEntity(DepartmentParticipation entity) {
		// Validates the entity before persisting.
		entity = validate(entity, null);
		
		// Save the entity.
		departmentParticipationDAO.save(entity);
	}
	
	/**========================================================ACADEMIC========================================================*/

	@EJB
	private AcademicDAO academicDAO;
	
	private PersistentObjectConverterFromId<Academic> academicConverter;
	
	@Override
	public PersistentObjectConverterFromId<Academic> getAcademicConverter() {
		if(academicConverter == null) {
			academicConverter = new PersistentObjectConverterFromId<Academic>(academicDAO);
		}
		return academicConverter;
	}
	
	@Override
	public List<Academic> findAcademicByQuery(String query) {
		return academicDAO.findByNameEmailOrCpf(query);
	}
	
	/**========================================================DEPARTMENT========================================================*/

	@EJB
	private DepartmentDAO departmentDAO;
	
	private PersistentObjectConverterFromId<Department> departmentConverter;

	@Override
	public PersistentObjectConverterFromId<Department> getDepartmentConverter() {
		if(departmentConverter == null) {
			departmentConverter = new PersistentObjectConverterFromId<Department>(departmentDAO);
		}
		return departmentConverter;
	}
	
	@Override
	public List<Department> findDepartmentByName(String name) {
		return departmentDAO.findByName(name);
	}
}
