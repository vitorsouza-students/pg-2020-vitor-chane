package br.ufes.informatica.marvin.admin.controller;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.application.filters.LikeFilter;
import br.ufes.inf.nemo.jbutler.ejb.controller.CrudController;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.admin.application.ManageDepartmentService;
import br.ufes.informatica.marvin.admin.domain.Department;
import br.ufes.informatica.marvin.admin.domain.Faculty;

/**
 * Controller for the "Manage Department Controller" use case.
 * 
 * This use case allows any user to create a new account in Marvin, becoming an Academic. The user
 * supplies basic information (name, e-mail, etc.) and Marvin sends an e-mail with a code so the new
 * Academic sets her password (through the "Change Password" use case), thus activating the account.
 * 
 * This controller is conversation scoped, beginning the conversation in method begin() and ending
 * in method end(), which should be called explicitly by the web pages.
 *
 * @author  (https://github.com/vitorsouza/)
 */

@Named
@SessionScoped
public class ManageDepartmentController extends CrudController<Department> {
	
	/**===============================================DEPARTMENT========================================================*/
	
	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** The "Manage Department" service. */
	@EJB
	private ManageDepartmentService manageDepartmentService;
	
	/** Getter for "Manage Department Controller" service. */
	@Override
	protected CrudService<Department> getCrudService() {
		return manageDepartmentService;
	}
	
	@Override
	protected void initFilters() {
		addFilter(new LikeFilter("manageDepartment.filter.byName", "name", "Filter department by name"));
	}
	
	/**=====================================================FACULTY=====================================================*/
	
	public PersistentObjectConverterFromId<Faculty> getFacultyConverter() {
		return manageDepartmentService.getFacultyConverter();
	}
	
	public Faculty getSelectedFaculty() {
		Faculty faculty = selectedEntity.getFaculty();
		
		return (faculty == null) ? new Faculty() : faculty;
	}
	
	public void setSelectedFaculty(Faculty selectedFaculty) {
		selectedEntity.setFaculty((selectedFaculty == null) ? new Faculty() : selectedFaculty);
	}
	
	public List<Faculty> completeFaculty(String query) {
		return manageDepartmentService.findFacultyByName(query);
	}
}