package br.ufes.informatica.marvin.admin.application;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.core.persistence.AcademicDAO;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourse;
import br.ufes.informatica.marvin.admin.domain.UndergraduateCourseInternshipCoordination;
import br.ufes.informatica.marvin.admin.persistence.UndergraduateCourseInternshipCoordinationDAO;
import br.ufes.informatica.marvin.admin.persistence.UndergraduateCourseDAO;

/**
 * Stateless session bean implementing the ManageUndergraduateCourseInternshipCoordinationService.
 * See the implemented interface documentation for details.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
@RolesAllowed({"SysAdmin", "Professor", "Staff"})
public class ManageUndergraduateCourseInternshipCoordinationServiceBean extends CrudServiceBean<UndergraduateCourseInternshipCoordination> implements ManageUndergraduateCourseInternshipCoordinationService {

	/**=====================================UndergraduateCourseInternshipCoordination=====================================*/

	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** The UndergraduateCourseInternshipCoordinationDAO is used to save the UndergraduateCourseInternshipCoordination that is registered by the SysAdmin. */
	@EJB
	private UndergraduateCourseInternshipCoordinationDAO undergraduateCourseInternshipCoordinationDAO;
	
	/** Getter for UndergraduateCourseInternshipCoordinationDAO. */
	@Override
	public BaseDAO<UndergraduateCourseInternshipCoordination> getDAO() {
		return undergraduateCourseInternshipCoordinationDAO;
	}
	
	@Override
	public List<UndergraduateCourseInternshipCoordination> returnUndergraduateCourseInternshipCoordinationByAcademic(Academic academic) {
		return undergraduateCourseInternshipCoordinationDAO.retrieveByAcademic(academic);
	}
	
	@Override
	public List<UndergraduateCourseInternshipCoordination> returnUndergraduateCourseInternshipCoordinationByDepartment(UndergraduateCourse undergraduateCourse){
		return undergraduateCourseInternshipCoordinationDAO.retrieveByUndergraduateCourse(undergraduateCourse);
	}
	
	public void saveEntity(UndergraduateCourseInternshipCoordination entity) {
		// Validates the entity before persisting.
		entity = validate(entity, null);
		
		// Save the entity.
		undergraduateCourseInternshipCoordinationDAO.save(entity);
	}
	
	/**========================================================ACADEMIC========================================================*/

	@EJB
	private AcademicDAO academicDAO;
	
	private PersistentObjectConverterFromId<Academic> academicConverter;
	
	@Override
	public PersistentObjectConverterFromId<Academic> getAcademicConverter() {
		if(academicConverter == null) {
			academicConverter = new PersistentObjectConverterFromId<Academic>(academicDAO);
		}
		return academicConverter;
	}
	
	@Override
	public List<Academic> findAcademicByQuery(String query) {
		return academicDAO.findByNameEmailOrCpf(query);
	}
	
	/**===========================================UndergraduateCourse===========================================*/

	@EJB
	private UndergraduateCourseDAO undergraduateCourseDAO;
	
	private PersistentObjectConverterFromId<UndergraduateCourse> undergraduateCourseConverter;
	
	@Override
	public PersistentObjectConverterFromId<UndergraduateCourse> getUndergraduateCourseConverter() {
		if(undergraduateCourseConverter == null) {
			undergraduateCourseConverter = new PersistentObjectConverterFromId<UndergraduateCourse>(undergraduateCourseDAO);
		}
		return undergraduateCourseConverter;
	}

	@Override
	public List<UndergraduateCourse> findUndergraduateCourseByQuery(String query) {
		return undergraduateCourseDAO.findByQuery(query);
	}
}