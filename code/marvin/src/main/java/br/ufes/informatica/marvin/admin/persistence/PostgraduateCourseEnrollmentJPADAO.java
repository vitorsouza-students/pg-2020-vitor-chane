package br.ufes.informatica.marvin.admin.persistence;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;
import br.ufes.informatica.marvin.admin.domain.PostgraduateCourseEnrollment_;
import br.ufes.informatica.marvin.admin.domain.PostgraduateCourse;
import br.ufes.informatica.marvin.admin.domain.PostgraduateCourseEnrollment;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * Stateless session bean implementing a DAO for objects of the PostgraduateCourseEnrollment domain class using JPA2.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation implementations are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any is defined
 * in the implementing DAO interface) have to be implemented in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Stateless
public class PostgraduateCourseEnrollmentJPADAO extends BaseJPADAO<PostgraduateCourseEnrollment> implements PostgraduateCourseEnrollmentDAO {
	/** The unique identifier for a serializable class. */
	private static final long serialVersionUID = 1L;
	
	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(PostgraduateCourseEnrollmentJPADAO.class.getCanonicalName());

	/** The application's persistent context provided by the application server. */
	@PersistenceContext
	private EntityManager entityManager;

	/** Getter for Entity Manager. */
	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	@Override
	public List<PostgraduateCourseEnrollment> retrieveByAcademic(Academic academic) {
		logger.log(Level.FINE, "Retrieving all PostgraduateCourseEnrollment instances whose academic is \"{0}\"...", academic);
		
	    // Constructs the query over the PostgraduateCourseEnrollment class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<PostgraduateCourseEnrollment> cq = cb.createQuery(PostgraduateCourseEnrollment.class);
	    Root<PostgraduateCourseEnrollment> root = cq.from(PostgraduateCourseEnrollment.class);
	
	    // Filters the query with the academic.
	    cq.where(cb.equal(root.get(PostgraduateCourseEnrollment_.academic), academic));
	    List<PostgraduateCourseEnrollment> result = entityManager.createQuery(cq).getResultList();
	    logger.log(Level.INFO, "Retrieving all PostgraduateCourseEnrollments instances by the academic \"{0}\" returned {1} results", new Object[] {academic, result.size()});
	    
	    return result;
	}

	@Override
	public List<PostgraduateCourseEnrollment> retrieveByPostgraduateCourse(PostgraduateCourse postgraduateCourse) {
		logger.log(Level.FINE, "Retrieving all Undergraduate Course Collegiate Members instances whose department is \"{0}\"...", postgraduateCourse);
		
		// Constructs the query over the PostgraduateCourseEnrollment class.
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<PostgraduateCourseEnrollment> cq = cb.createQuery(PostgraduateCourseEnrollment.class);
	    Root<PostgraduateCourseEnrollment> root = cq.from(PostgraduateCourseEnrollment.class);
	    
	    // Filters the query with the PostgraduateCourse.
	    cq.where(cb.equal(root.get(PostgraduateCourseEnrollment_.postgraduateCourse), postgraduateCourse));
	    List<PostgraduateCourseEnrollment> result = entityManager.createQuery(cq).getResultList();
	    
	    logger.log(Level.INFO, "Retrieving all PostgraduateCourseEnrollments instances by the department \"{0}\" returned {1} results", new Object[] {postgraduateCourse, result.size()});
	    
	    return result;
	}

}