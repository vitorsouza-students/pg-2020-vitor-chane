package br.ufes.informatica.marvin.admin.domain;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * The department secretary class for the MACS (Marvin Access Control System) module of Marvin.
 *
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
 * @version 1.0
*/

@Entity
public class DepartmentSecretary extends PersistentObjectSupport implements Comparable <DepartmentSecretary> {
	
	/**===================================================DEPARTMENT SECRETARY INFORMATION===================================================*/
	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The timestamp of the moment the secretary started to work on the department secretary */
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private Date beginDate;
	
	/** Getter for beginDate. */
	public Date getBeginDate() {
	  return beginDate;
	}
	
	/** Setter for beginDate. */
	public void setBeginDate(Date beginDate) {
	  this.beginDate = beginDate;
	}
	
	/** The timestamp of the moment the academic finished the work on the department secretary */
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	
	/** Getter for endDate. */
	public Date getEndDate() {
	  return endDate;
	}
	
	/** Setter for endDate. */
	public void setEndDate(Date endDate) {
	  this.endDate = endDate;
	}
	
	/** Compare function for department secretary. */
	@Override
	public int compareTo(DepartmentSecretary o) {
 		return 0;
 	}
	
	/**==============================================================ACADEMIC=============================================================*/
	
	/** Secretary (Academic with Staff role) for this department secretary. */
	@NotNull
	@ManyToOne
	private Academic academic;
	
	/** Getter for secretary. */
	public Academic getAcademic() {
		return academic;
	}
 	
 	/** Setter for secretary. */
 	public void setAcademic(Academic academic) {
 		this.academic = academic;
 	}
	
	/**=========================================================DEPARTMENT========================================================*/
	
	/** Department for this department secretary. */
	@NotNull
	@ManyToOne
	private Department department;
	
	/** Getter for department. */
	public Department getDepartment() {
	  return department;
	}
	
	/** Setter for department. */
 	public void setDepartment(Department department) {
	  this.department = department;
	}
}
