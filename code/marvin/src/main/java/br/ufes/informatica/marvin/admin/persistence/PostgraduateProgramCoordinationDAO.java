package br.ufes.informatica.marvin.admin.persistence;

import java.util.List;
import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgram;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgramCoordination;
import br.ufes.informatica.marvin.core.domain.Academic;

/**
 * Interface for a DAO for objects of the PostgraduateProgramCoordination domain class.
 * 
 * Using a mini CRUD framework for EJB3, basic DAO operation definitions are inherited from the
 * superclass, whereas operations that are specific to the managed domain class (if any) are
 * specified in this class.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Local
public interface PostgraduateProgramCoordinationDAO extends BaseDAO<PostgraduateProgramCoordination> {
	
	/**
	   * TODO: document this method.
	   * 
	   * @param academic
	   * @return List<PostgraduateProgramCoordination>
	*/
	List<PostgraduateProgramCoordination> retrieveByAcademic(Academic academic);
	
	/**
	   * TODO: document this method.
	   * 
	   * @param undergraduateCourse
	   * @return List<PostgraduateProgramCoordination>
	*/
	List<PostgraduateProgramCoordination> retrieveByPostgraduateProgram(PostgraduateProgram postgraduateProgram);

	boolean academicIsActivePostgraduateProgramCoordination(Academic academic);

}