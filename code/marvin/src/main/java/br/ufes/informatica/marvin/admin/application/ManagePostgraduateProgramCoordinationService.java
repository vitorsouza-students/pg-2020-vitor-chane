package br.ufes.informatica.marvin.admin.application;

import java.util.List;
import javax.ejb.Local;

import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;
import br.ufes.informatica.marvin.core.domain.Academic;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgram;
import br.ufes.informatica.marvin.admin.domain.PostgraduateProgramCoordination;

/**
 * Local EJB interface for the session information component. This bean provides an authentication
 * method for the controller, identifying users of the system.
 * 
 * @author Vitor Araujo Chane (vitor.chane@gmail.com)
*/
@Local
public interface ManagePostgraduateProgramCoordinationService extends CrudService<PostgraduateProgramCoordination> {

	void saveEntity(PostgraduateProgramCoordination entity);
	
	boolean checkIfAcademicIsPostgraduateProgramCoordination(Academic academic);
			
	List<PostgraduateProgramCoordination> returnPostgraduateProgramCoordinationByAcademic(Academic academic);
	
	List<PostgraduateProgramCoordination> returnPostgraduateProgramCoordinationByPostgraduateProgram(PostgraduateProgram postgraduateProgram);

	PersistentObjectConverterFromId<Academic> getAcademicConverter();
	
	PersistentObjectConverterFromId<PostgraduateProgram> getPostgraduateProgramConverter();
	
	List<Academic> findAcademicByQuery(String query);
	
	List<PostgraduateProgram> findPostgraduateProgramByQuery(String query);
}
